using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemCollect : MonoBehaviour
{
    private string fish;
    // Start is called before the first frame update

    private void OnTriggerEnter2D(Collider2D collision)
    {

        switch (collision.tag)
        {

            case "fish":
                Destroy(collision.gameObject);
                GameObject fish = collision.gameObject;
                Debug.Log("You collected a fish");
                break;

            case "coral":
                Destroy(collision.gameObject);
                GameObject coral = collision.gameObject;
                Debug.Log("You collected a coral");
                break;

            case "scrap":
                Destroy(collision.gameObject);
                GameObject scrap = collision.gameObject;
                Debug.Log("You collected some scrap");
                break;

        }

    }

}
