using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{

    public float speed = 5f;
    public float force = 2f;
    private float movementX;
    private float movementY;

    void playerMovement()
    {

        movementX = Input.GetAxisRaw("Horizontal");
        transform.position += new Vector3(movementX, 0, 0) * Time.deltaTime * force;

        movementY = Input.GetAxisRaw("Vertical");
        transform.position += new Vector3(0, movementY, 0) * Time.deltaTime * force;

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        playerMovement();

    }
}
