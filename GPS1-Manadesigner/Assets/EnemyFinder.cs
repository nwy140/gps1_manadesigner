using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFinder : MonoBehaviour
{
    public EnemyHealth enemyHealth;

    HomingBullet homingBullet;

    private void Start()
    {
        homingBullet = GetComponentInParent<HomingBullet>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<EnemyHealth>())
        {
            enemyHealth = collision.gameObject.GetComponent<EnemyHealth>();
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, GetComponent<CircleCollider2D>().radius);
    }
}
