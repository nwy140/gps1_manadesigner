using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionByTagEvent : MonoBehaviour
{
    public List<string> allowedTags;
    public bool filterByAllowedTags = true;
    public bool isCallOnCollisionEnter2D;

    public bool isCallOnTriggerEnter2D;
    public List<string> disallowedTags;

    public List<string> ignorePhysicsForTargetTags;

    public List<Collider2D> ignoreTargetCols = new List<Collider2D>();
    Collider2D col;
    private void Awake()
    {
        TryGetComponent(out col);
        foreach (var tag in ignorePhysicsForTargetTags)
        {
            var tmpObj = GameObject.FindGameObjectWithTag(tag);
            if (tmpObj == null) break;
            Collider2D tempCol;
            tmpObj.TryGetComponent(out tempCol);
            if (tempCol == null) break;
            Physics2D.IgnoreCollision(col, tempCol);
            ignoreTargetCols.Add(tempCol);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ignorePhysicsForTargetTags.Contains(collision.gameObject.tag))
        {
            Physics2D.IgnoreCollision(collision.collider, col);
        }

        if (isCallOnCollisionEnter2D)
        {
            OnHitbox(collision.gameObject);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isCallOnTriggerEnter2D)
        {
            OnHitbox(collision.gameObject);
        }
    }
    public UnityEvent OnHitBox_Event;
    //public UnityEvent OnDisallowedTag_HitBox_Event;
    //public List<GameObject> s;
    public void OnHitbox(GameObject other)
    {
        //Debug.Log(other.tag);

        if (filterByAllowedTags == true)
        {
            if (allowedTags.Contains(other.tag) == true && disallowedTags.Contains(other.tag) == false && other.GetComponent<CompositeCollider2D>() == null)
            {
                OnHitBox_Event.Invoke();
                //s.Add(other);
            }
        }
        else
        {
            if (disallowedTags.Contains(other.tag) == false && other.GetComponent<CompositeCollider2D>() == null)
            {
                OnHitBox_Event.Invoke();
                //s.Add(other);

            }
        }


        //if(disallowedTags.Contains(other.tag) == true)
        //{
        //    OnDisallowedTag_HitBox_Event.Invoke();
        //}
    }


}
