using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AutoFlipSpriteByDir : MonoBehaviour
{
    NavMeshAgent agent;
    public SpriteRenderer sprite;
    public Rigidbody2D rb;
    public bool hasRb;
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        hasRb = TryGetComponent(out rb);

    }
    private void Update()
    {
        if (hasRb == false)
        {
            UpdateFlipSpriteByDir(agent.destination);
        }
        else
        {
            UpdateFlipSpriteByDir(sprite.transform.position + (Vector3)rb.velocity, true);
        }
    }
    // Move to 2d Common Transform
    void UpdateFlipSpriteByDir(Vector3 targetPos, bool isFlipX = false)
    {
        Vector3 newRot = transform.eulerAngles;

        //print(CheckIfObjectDirectionBetween2Vectors(transform.position, targetPos));
        if (GetObjectDirectionBetween2Vectors(transform.position, targetPos) < 0
            && GetObjectDirectionBetween2Vectors(transform.position, targetPos) != 0
            )// if right dir
             //newRot.y = 180;
        {
            //newRot.y = 0;
            if (isFlipX == true)
            {
                sprite.flipX = false;
            }
            if (isFlipX == false)
            {
                sprite.flipY = false;
            }
        }
        else if (GetObjectDirectionBetween2Vectors(transform.position, targetPos) > 0
      && GetObjectDirectionBetween2Vectors(transform.position, targetPos) != 0
      )
        {
            //newRot.y = 0;
            if (isFlipX == true)
            {
                sprite.flipX = true;
            }
            if (isFlipX == false)
            {
                sprite.flipY = true;
            }
        }
        //t.eulerAngles = newRot;
    }

    int GetObjectDirectionBetween2Vectors(Vector3 point1, Vector3 point2)
    {
        int dirNum;
        Vector3 heading = point1 - point2;
        dirNum = AngleDir(Vector3.forward, heading, Vector3.up);
        return dirNum;
    }
    int AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            return 1;
        }
        else if (dir < 0f)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
    // Ref: https://stackoverflow.com/questions/43492004/unity-clamp-rotation-between-arbitrary-angles
    public float ClampRotation2(float angle, float min, float max)
    {
        if (angle < 0) angle += 360;

        if (max < 0) max += 360;

        if (min < 0) min += 360;
        if (min > max) min -= 360;

        return Mathf.Clamp(angle, min, max);
    }
    public bool IsWithinArbitaryRot(float angle, float min, float max)
    {
        bool result = true;

        var tmp = ClampRotation2(angle, min, max);
        if (tmp == min || tmp == -max)
        {
            result = false;
        }
        return result;
    }
}
