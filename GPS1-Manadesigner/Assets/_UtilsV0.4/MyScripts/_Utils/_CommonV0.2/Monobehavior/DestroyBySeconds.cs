using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBySeconds : MonoBehaviour
{
    public float destroyTimer = 3f;
    public bool isAllowDestroy = true;
    private void Start()
    {
        if(isAllowDestroy)
            Destroy(gameObject, destroyTimer);
    }
}
