using UnityEngine;
using UnityEngine.UI;

// from https://stackoverflow.com/questions/30766020/how-to-scroll-to-a-specific-element-in-scrollrect-with-unity-ui
public static class ScrollRectCommon
{
    public static void ScrollRectSnapAnchorPosTo(ScrollRect scrollRect, RectTransform contentPanel, RectTransform target)
    {
        Canvas.ForceUpdateCanvases();

        contentPanel.anchoredPosition =
            (Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position)
            - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }
    public static void ScrollRectSnapAnchorPosTo(ScrollRect scrollRect, RectTransform contentPanel, RectTransform target, Vector2 offsetPos)
    {
        Canvas.ForceUpdateCanvases();

        contentPanel.anchoredPosition =
            (Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position)
            - (Vector2)scrollRect.transform.InverseTransformPoint(target.position)
            + offsetPos;

    }
}



