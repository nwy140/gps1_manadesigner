
using UnityEngine;
using UnityEngine.UI;
using TMPro;
[ExecuteAlways]
public class SyncTextWithTextMeshPro : MonoBehaviour
{
    public Text text;
    public TextMeshProUGUI text_TMPro;

    public string removeString;

    void OnGUI()
    {
        if(text!=null && text_TMPro != null)
        {
            text_TMPro.text = text.text;
        }

        if(text.text!="" && removeString != "")
        {
        if (text.text.Contains(removeString))
        {
                text.text= text.text.Replace(removeString, "");
        }
        }
    }
}
