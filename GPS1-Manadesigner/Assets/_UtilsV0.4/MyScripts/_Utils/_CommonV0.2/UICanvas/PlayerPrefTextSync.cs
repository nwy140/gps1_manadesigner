using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PlayerPrefTextSync : MonoBehaviour
{
    public TextMeshProUGUI text;
    bool hasText;
    public string playerPrefsFloatName = "Score";
    public bool isInt;

    public float offset;
    // Start is called before the first frame update
    void Start()
    {
        hasText = TryGetComponent(out text);

    }

    // Update is called once per frame
    void Update()
    {
        if (hasText)
        {
            if(isInt == false)
            { 
                text.text = (PlayerPrefs.GetFloat(playerPrefsFloatName) + offset).ToString();
            }
            else
            {
                text.text = (PlayerPrefs.GetInt(playerPrefsFloatName) + (int)offset).ToString();
            }
        }
    }
}
