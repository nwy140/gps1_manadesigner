using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transform2DAutoLookAtObj2D : MonoBehaviour
{
    Transform targetObj;
    bool targetIsPlayer = false;

    public float aimRotOffset = -90;

    private void Awake()
    {
    }
    void Update()
    {
        if (targetIsPlayer)
        {
            if (GameObject.FindGameObjectWithTag("Player"))
            {

                Transform obj = GameObject.FindGameObjectWithTag("Player").transform;
                targetObj = obj;
            }
        }
        var rot = Quaternion.Euler((Vector2Common.GetRotBetween2Pos(targetObj.position, transform.position) + aimRotOffset) * Vector3.forward);
        transform.rotation = rot;
    }

}
