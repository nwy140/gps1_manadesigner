using System.Collections;
using System.Collections.Generic;
//using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

public class NavAgentTestAutoSetRandomDestination : MonoBehaviour
{
    public NavMeshAgent agent;
    public float radius = 25;
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    public void SetNewDestination()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit hit;

        //    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
        //    {
        //        agent.destination = hit.point;
        //    }
        //}
        if (agent != null)
            agent.SetDestination(AINavAgentCommon.RandomPosition(transform.position, radius));
        //Wanderer.DrawGizmos();
    }

}