using System.Collections;
using System.Collections.Generic;
//using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

public class NavAgentTestAutoSetWASDMovement : MonoBehaviour
{
    NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit hit;

        //    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
        //    {
        //        agent.destination = hit.point;
        //    }
        //}
        float horInput = Input.GetAxis("Horizontal");
        float verInput = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(horInput , verInput, 0);
        Vector3 moveDestination = transform.position + movement;
        agent.destination = moveDestination;
        //Wanderer.DrawGizmos();
    }

}