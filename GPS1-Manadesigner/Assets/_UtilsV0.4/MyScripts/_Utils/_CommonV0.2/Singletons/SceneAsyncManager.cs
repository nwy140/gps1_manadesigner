﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
	
	// Example Doxygen Documentation Convention
	/**
	 * @file
	 * @author  John Doe <jdoe@example.com>
	 * @version 1.0
	 * @section DESCRIPTION
	 *
	 * The time class represents a moment of time.
	 */
public class SceneAsyncManager : MonoBehaviour
{

	public static SceneAsyncManager instance;
	
	// Force mouse to be visible at all times
	public bool isForceMouseAlwaysVisible = false;
	public bool isNextSceneFullyLoaded;
	AsyncOperation asyncLoad;

	public bool isWaitForAsyncFinishLoading = true;
	public void Awake()
	{
		MakeInstance();

	}
	void MakeInstance()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	// Ref: https://docs.unity3d.com/ScriptReference/SceneManagement.SceneManager.LoadSceneAsync.html
	IEnumerator LoadAsyncScene(string sceneName)
	{
		isNextSceneFullyLoaded = false;
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.
        if (isWaitForAsyncFinishLoading)
        {
            asyncLoad = SceneManager.LoadSceneAsync(sceneName);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                isNextSceneFullyLoaded = false;
                yield return null;
            }
        }
        else
        {
			SceneManager.LoadScene(sceneName);
        }
    }

    // Custom made Wrapper for SceneManager.LoadScene
    public void LoadScene(string sceneName)
    {
		StartCoroutine(LoadAsyncScene(sceneName));
    }

	void Update()
	{

		// For quick level switching in development only
		/// Load first level by index or second level by index
		//if (Input.GetKeyDown(KeyCode.Alpha1))    
		//{
		//	LoadLevelByIndex(0);
		//}
		//else if (Input.GetKeyDown(KeyCode.Alpha2)) {
		//	LoadLevelByIndex(1);

		//}
		//Always show mouse on Menu

	    
	}

	public void LoadLevel(string Name)
	{
		gameObject.name = "Level Manager is trying to: " + "Load level name by " + Name ;

		Debug.Log("Level Load requeted for : " + Name);
		LoadScene(Name);
	}

	// Ref: https://answers.unity.com/questions/1262342/how-to-get-scene-name-at-certain-buildindex.html
	private string GetSceneNameFromIndex(int BuildIndex)
	{
		string path = SceneUtility.GetScenePathByBuildIndex(BuildIndex);
		int slash = path.LastIndexOf('/');
		string name = path.Substring(slash + 1);
		int dot = name.LastIndexOf('.');
		return name.Substring(0, dot);
	}
	public void RestartLevel()
	{
		gameObject.name = "Level Manager is trying to: " + "Restart the level" ;

		Debug.Log("Level Load requeted for : " + "Restart Level");
		LoadScene(SceneManager.GetActiveScene().name);
	}


	public void RequestQuit()
	{
		Debug.Log("I want to quit");
		Application.Quit();
	}
	public void LoadNextLevel()
	{
		gameObject.name = "Level Manager is trying to: " + "Load next Level " ;
		LoadScene(GetSceneNameFromIndex(SceneManager.GetActiveScene().buildIndex + 1));
	}
	
	public int GetCurrentLevelIndex(){
		return SceneManager.GetActiveScene().buildIndex;
	}

	public void LoadLevelByIndex(int index)
	{
		gameObject.name = "Level Manager is trying to: " + "Load level index by " + index  ;

		Debug.Log("Level Load requeted for index : " + index);
		LoadScene(GetSceneNameFromIndex(index));
		// show mouse on menu
	}

	public void SetMouseVisibility(bool isVisible)
	{

		if(isForceMouseAlwaysVisible == false)
			Cursor.visible = isVisible;
		else
			Cursor.visible = true;
	}
 
	public void PauseLevel()
	{
		gameObject.name = "Level Manager is trying to: " + "Pause the level" ;
		if(GameplayManager.instance){
			// You can only pause the game when Gameplay State is in running state
			if(GameplayManager.instance.CurrGameplayState == GameplayManager.GameplayState.GAME_RUNNING){
				GameplayManager.instance.CurrGameplayState = GameplayManager.GameplayState.GAME_PAUSE;
			}
		}
	}

	public void unPauseLevel()
	{
		gameObject.name = "Level Manager is trying to: " + "UnPause the level" ;

		if(GameplayManager.instance){
			GameplayManager.instance.CurrGameplayState = GameplayManager.GameplayState.GAME_RUNNING;
		}

	}
	


}