using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class Stat
{
    [SerializeField] int _curValue;
    public virtual int curValue { get => _curValue; set => _curValue = value; }
    public int maxValue;
    public int minValue = 0;
    public UnityEvent OnStatValueModifiedEvent;

    [Header("UI Components")]
    public Slider slider;
    public TextMeshProUGUI text;


    [Header("Runtime view Only")]
    [SerializeField] float lerpHP;
    public float hpSliderLerpSpeed = 0.95f;


    public int IncrementValue(int incrementVal) {
        _curValue += incrementVal;
        _curValue = Mathf.Clamp(_curValue, minValue, maxValue);
        OnStatValueModified();
        return _curValue;
    }
    public int DecrementValue(int decrementVal)
    {
        _curValue += decrementVal;
        _curValue = Mathf.Clamp(_curValue,minValue,maxValue);
        OnStatValueModified();
        return _curValue;
    }

    public void OnStatValueModified()
    {
        if(slider!=null)
            slider.value = _curValue;
        OnStatValueModifiedEvent.Invoke();
    }

    public void OnExternalUpdate() // Call from Monobehavior Update if neccessary
    {
        if (slider != null)
            slider.value = _curValue;
        _curValue = Mathf.Clamp(_curValue, minValue, maxValue);
        HandleSliderLerpValue();
    }

    public bool hasSliderFinishedLerping;
    private void HandleSliderLerpValue()
    {
        //HP Lerp Implementation Reference: https://www.youtube.com/watch?v=Wx9TgWl4LAU
        if (slider != null)
        {

            if (maxValue != 0) // should not divide by zero
            {
                lerpHP = MathCommon.CalculateSliderValuePercentage(slider, curValue, maxValue, hpSliderLerpSpeed * Time.deltaTime) * maxValue;
                // 2decimal place ToString("F2") Reference: https://answers.unity.com/questions/50391/how-to-round-a-float-to-2-dp.html
                if (slider != null)
                {
                    text.SetText( Mathf.Round(lerpHP) + " / " + (int)maxValue);
                }
            }
            // If HP bar value == 0 i.e by 0 decimal place, then ResetToOriginal Shader Material
            if (Mathf.Round(lerpHP) == 0)
            {
                hasSliderFinishedLerping = true;
            }
            else
            {
                hasSliderFinishedLerping = false;
            }
        }
    }
}
