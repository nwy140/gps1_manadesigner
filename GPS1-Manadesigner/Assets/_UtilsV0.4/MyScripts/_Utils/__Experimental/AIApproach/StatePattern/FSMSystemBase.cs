using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMSystemBase : MonoBehaviour
{
    // Treat the FSM System as if it's a seperate gamepad entity that evaluates the environments, and calls its own commands from my custom system when necessary

    [Header("FSM State")]
    public FSMBaseState _curState;
    public FSMBaseState _prevState;

    public FSMBaseState _startingState;

    public bool isDebugLog = false;
    

    public virtual void Awake()
    {
        if (_startingState != null)
        {
            _curState = _startingState;
        }
        else
        {
            _curState = new DefaultState(this);
        }
        _curState.OnEnterState();
    }


    public virtual void Update()
    {
        if (_curState != null)
        {
            _curState.OnUpdate();
        }
    }
    public virtual void TryEnterState(FSMBaseState nextState)
    {
        if (_curState != null)
        {
            _curState.OnExitState();
        }
        _curState = nextState;
        _curState.OnEnterState();
        //Debug.Log(_curState.aName);

    }

    //private void OnCollisionEnter(Collision collision)
    //{ 
    //    _curState.OnCollisionEnter(collision);
    //}


    //private void OnEnable()
    //{
    //    //SetState(new TestState1(this));
    //}
}
