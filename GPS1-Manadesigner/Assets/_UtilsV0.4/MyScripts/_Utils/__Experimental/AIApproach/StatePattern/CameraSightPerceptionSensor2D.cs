using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;


// Ref: https://stackoverflow.com/questions/45020915/how-to-detect-all-gameobjects-within-the-camera-fov-unity3d
public class CameraSightPerceptionSensor2D : MonoBehaviour
{

    // Sprite Renderer
    [Header("All Renderers In Scene that has a NavMeshAgent component in a parent obj")]
    public List<SpriteRenderer> renderers;
    public List<Transform> visibleRenderers;
    public List<Camera> cameras;
    public GameObject ownerRootObj;

    List<SpriteRenderer> renderersUnderCurrentOwnerObj;

    void Awake()
    {
        //GameObject walls = GameObject.FindGameObjectWithTag("Wall");
        //renderers = walls.GetComponentsInChildren<Renderer>();
        RefreshRenderersList();

        renderersUnderCurrentOwnerObj = new List<SpriteRenderer>(ownerRootObj.GetComponentsInChildren<SpriteRenderer>());
        if (GetComponent<SpriteRenderer>())
        {
            renderersUnderCurrentOwnerObj.Add(GetComponent<SpriteRenderer>());
        }
    }

    public void RefreshRenderersList()
    {
        var allNavAgents = GameObject.FindObjectsOfType<NavMeshAgent>();

        foreach (var a in allNavAgents)
        {
            var renderer = a.GetComponent<SpriteRenderer>();
            if (renderer == null)
            {
                renderer = a.GetComponentInChildren<SpriteRenderer>();
            }
            if (renderer != null)
            {
                if (renderers.Contains(renderer) == false)
                {
                    renderers.Add(renderer);
                }
            }
        }
    }

    void Update()
    {
        RefreshRenderersList();
        OutputVisibleRenderers(renderers);
    }

    void OutputVisibleRenderers(List<SpriteRenderer> renderers)
    {
        foreach (var renderer in renderers)
        {
            // output only the visible renderers' name
            if (IsVisible(renderer))
            {
                //Debug.Log(renderer.name + " is detected!");
                if (visibleRenderers.Contains(renderer.transform) == false)
                    visibleRenderers.Add(renderer.transform);
            }
            else
            {
                visibleRenderers.Remove(renderer.transform);
            }
        }

        //Debug.Log("--------------------------------------------------");
    }

    private bool IsVisible(Renderer renderer)
    {
        bool isVisibleFound = false;
        foreach (var cam in cameras)
        {
            if (renderersUnderCurrentOwnerObj.Contains(renderer)) // if does not belong to self
            {
                isVisibleFound = false;
                break;
            }
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);

            if (GeometryUtility.TestPlanesAABB(planes, renderer.bounds))
                isVisibleFound = true;
            else
                isVisibleFound = false;
            if (isVisibleFound == true)
                break;
        }
        return isVisibleFound;
    }
}