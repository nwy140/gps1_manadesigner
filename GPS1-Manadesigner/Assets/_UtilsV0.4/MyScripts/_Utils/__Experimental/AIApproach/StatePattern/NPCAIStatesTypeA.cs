using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Contains All States for NPCAITypeA

// see NPCAISystemTypeA


public class DefaultState : FSMBaseState
{
    public DefaultState(FSMSystemBase system) : base(system)
    {
    }
    public override bool OnEnterState()
    {

        return base.OnEnterState();
    }
    public override bool OnExitState()
    {
        return base.OnExitState();
    }
    public override void OnUpdate()
    {
    }
}


public class Alert : FSMBaseState
{
    public Alert(FSMSystemBase system) : base(system)
    {
    }
    public override bool OnEnterState()
    {
        bool result = base.OnEnterState();

        if (result)
            _system.StartCoroutine(OnEnterState_Couroutine());
        return result;
    }
    IEnumerator OnEnterState_Couroutine()
    {
        yield return new WaitForSeconds(0.5f);
        ((NPCAISystemTypeA)_system).TryEnterStateByName(nameof(Chase));
    }
    public override bool OnExitState()
    {
        return base.OnExitState();
    }

}

public class Chase : FSMBaseState
{
    NavMeshAgent agent;
    public bool hasSetDestination;
    public Transform target;

    float originalSpeed;
    float originalAngularSpeed;
    public Chase(FSMSystemBase system) : base(system)
    {
        agent = ((NPCAISystemTypeA)_system).agent;
        originalSpeed = agent.speed;
        originalAngularSpeed = agent.angularSpeed;
    }
    public override bool OnEnterState()
    {

        return base.OnEnterState();
    }
    public override bool OnExitState()
    {
        agent.speed = originalSpeed;
        agent.angularSpeed = originalAngularSpeed;
        return base.OnExitState();
    }
    float fleeRadius = 5f;
    public float newSpeed = 30f;
    public float newAgularSpeed = 500f;

    public override void OnStayState()
    {
        base.OnStayState();
        if (Physics2D.Linecast(agent.transform.position, target.position))
        {
            var tempDestinationPos = target.position;
            //hasSetDestination = agent.SetDestination(tempDestinationPos);
            agent.destination = tempDestinationPos;
        }
    }
}

public class Flee : FSMBaseState
{
    NavMeshAgent agent;
    public bool hasSetDestination;
    public Transform target;

    float originalSpeed;
    float originalAngularSpeed;
    public Flee(FSMSystemBase system) : base(system)
    {
        agent = ((NPCAISystemTypeA)_system).agent;
        originalSpeed = agent.speed;
        originalAngularSpeed = agent.angularSpeed;
    }
    public override bool OnEnterState()
    {

        return base.OnEnterState();
    }
    public override bool OnExitState()
    {
        agent.speed = originalSpeed;
        agent.angularSpeed = originalAngularSpeed;
        return base.OnExitState();
    }
    float fleeRadius = 5f;
    public float newSpeed = 30f;
    public float newAgularSpeed = 500f;

    public override void OnStayState()
    {
        base.OnStayState();
        //if (Physics2D.Linecast(agent.transform.position, target.position))
        //{
        //    var tempDestinationPos = target.position;
        //    hasSetDestination = agent.SetDestination(tempDestinationPos);
        //}
        if (Physics2D.Linecast(agent.transform.position, target.position))
        {
            var fleeDirection = (agent.transform.position - target.position).normalized;
            var newGoal = agent.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);
            if (path.status != NavMeshPathStatus.PathInvalid)
            {
                var tempDestinationPos = path.corners[path.corners.Length - 1];
                //hasSetDestination = agent.SetDestination(tempDestinationPos);
                agent.destination = tempDestinationPos;
                agent.speed = newSpeed;
                agent.angularSpeed = newAgularSpeed;
            }
            else
            {
                //Debug.Log("Fail");
                agent.destination = newGoal;
                agent.speed = newSpeed;
                agent.angularSpeed = newAgularSpeed;
            }
        }
    }
}

public class PatrolByRandomPos : FSMBaseState
{
    NavMeshAgent agent;
    public int waypointIndex = 0;
    public WaypointPath waypointComp;

    [Header("Patrol Agent Details And Time")]
    public bool hasSetDestination;
    public float etaNextWaypoint;
    public float timeBetweenWaypointsMin = 5f;
    public float timeBetweenWaypointsMax = 10f;

    public PatrolByRandomPos(FSMSystemBase system) : base(system)
    {
        agent = ((NPCAISystemTypeA)_system).agent;
        waypointComp = ((NPCAISystemTypeA)_system).waypointComp;
    }
    public override bool OnEnterState()
    {
        bool result = base.OnEnterState();
        return result;
    }


    public override bool OnExitState()
    {
        return base.OnExitState();
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
    }

    public override void OnStayState()
    {
        base.OnStayState();

        if ((agent != null && Time.time > etaNextWaypoint) || ( AINavAgentCommon.HasAgentReachedDestination(agent) == true ) )
        {
            PerformStateBehaviour();
        }
    }
    public void PerformStateBehaviour()
    {
        etaNextWaypoint += Random.Range(timeBetweenWaypointsMin, timeBetweenWaypointsMax);
        var tempDestinationPos = AINavAgentCommon.RandomPosition(_system.transform.position, 7);
        hasSetDestination = agent.SetDestination(tempDestinationPos);
        //agent.destination = tempDestinationPos;
    }
}
public class PatrolByWaypoints : FSMBaseState
{
    NavMeshAgent agent;
    public int waypointIndex = 0;
    public WaypointPath waypointComp;

    [Header("Patrol Agent Details And Time")]
    public bool hasSetDestination;
    public float etaNextWaypoint;
    public float timeBetweenWaypointsMin = 5f;
    public float timeBetweenWaypointsMax = 10f;
    public bool isInvertWaypointsIteration;
    public PatrolByWaypoints(FSMSystemBase system) : base(system)
    {
        agent = ((NPCAISystemTypeA)_system).agent;
        waypointComp = ((NPCAISystemTypeA)_system).waypointComp;
    }
    public override bool OnEnterState()
    {
        bool result = base.OnEnterState();

        return result;
    }

    public override bool OnExitState()
    {
        return base.OnExitState();
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
    }

    public override void OnStayState()
    {
        base.OnStayState();

        if ((agent != null && Time.time > etaNextWaypoint) || AINavAgentCommon.HasAgentReachedDestination(agent) == true )
        {
            PerformStateBehaviour();
        }
    }
    public void PerformStateBehaviour()
    {
        var tempDestinationPos = waypointComp.waypoints[waypointIndex].position;
        if(_system.isDebugLog)
        Debug.Log(waypointIndex);
        if (waypointIndex >= waypointComp.waypoints.Length - 1)
        {
            //waypointIndex = 0;
            //waypointIndex += 1;
            isInvertWaypointsIteration = true;
        }
        else if (waypointIndex <= 0)
        {
            //waypointIndex = waypointComp.waypoints.Length - 1;
            //waypointIndex -= 1;
            isInvertWaypointsIteration = !isInvertWaypointsIteration;
            isInvertWaypointsIteration = false;
        }
        waypointIndex += isInvertWaypointsIteration ? -1 : 1;
        waypointIndex = Mathf.Clamp(waypointIndex, 0, waypointComp.waypoints.Length - 1);

        etaNextWaypoint += Random.Range(timeBetweenWaypointsMin, timeBetweenWaypointsMax);
        hasSetDestination = agent.SetDestination(tempDestinationPos);
        //agent.destination = tempDestinationPos;

    }
}