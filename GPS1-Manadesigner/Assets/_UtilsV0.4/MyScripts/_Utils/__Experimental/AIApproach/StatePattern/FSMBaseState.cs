using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//[System.Serializable]

// Ref: https://youtu.be/21yDDUKCQOI?t=326 
public abstract class FSMBaseState : ScriptableObject
{
    //Ref: https://www.youtube.com/watch?v=Vt8aZDPzRjI
    protected readonly FSMSystemBase _system;
    [SerializeField] string _aName;
    public virtual string aName { get => GetType().Name; set => _aName = value; }

    public FSMExecutionState _ExecutionState;
    public virtual FSMExecutionState ExecutionState { get => _ExecutionState; set => _ExecutionState = value; }

    public float OnEnterTimeStamp;
    [Header("Set In Runtime")]
    public float ElapsedTime;
    public FSMBaseState(FSMSystemBase system)
    {
        _system = system;
        _aName = aName;
    }

    // Start is called before the first frame update
    public virtual bool OnEnterState()
    {
        if (_system.isDebugLog)
            Debug.Log("Enter State: " + aName);

        if (_ExecutionState != FSMExecutionState.ACTIVE)
        {
            _ExecutionState = FSMExecutionState.ACTIVE;
            OnEnterTimeStamp = Time.time;
            ElapsedTime = 0;
            return true;
        }
        return false;
    }
    public virtual void OnUpdate()
    {
        if (_system.isDebugLog)
            Debug.Log("Updating State: " + aName);
        ElapsedTime += Time.deltaTime;
        if (_ExecutionState == FSMExecutionState.ACTIVE)
        {
            OnStayState();
        }
    }
    public virtual void OnStayState()
    {
        if (_system.isDebugLog)
            Debug.Log("Stay State: " + aName);

    }
    public virtual bool OnExitState()
    {
        if (_system.isDebugLog)
            Debug.Log("Exit State: " + aName);

        if (_ExecutionState != FSMExecutionState.COMPLETED)
        {
            _ExecutionState = FSMExecutionState.COMPLETED;
            return true;
        }
        return false;
    }

    public void OnEnable()
    {
        _ExecutionState = FSMExecutionState.NONE;
    }

    public void OnDisable()
    {

    }


}


public enum FSMExecutionState
{
    NONE,
    ACTIVE,
    COMPLETED,
    TERMINATED,
}
