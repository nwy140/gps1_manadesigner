//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.AI;

////public class NPCAIStates : FSMState
////{
////    public NPCAIStates(FSMSystemBase system) : base(system)
////    {
////    }

////}


//public class Patrol : FSMBaseState
//{
//    NavMeshAgent agent;
//    public float timeBetweenPatrol = 10f;
//    public int waypointIndex = 0;
//    public WaypointPath waypointComp;


//    public Patrol(FSMSystemBase system) : base(system)
//    {
//        agent = ((NPCAISystemTypeA)_system).agent;
//        waypointComp = ((NPCAISystemTypeA)_system).waypointComp;
//    }
//    public override IEnumerator OnEnterState()
//    {
//        if (((NPCAISystemTypeA)_system).isUseWaypoint == false)
//        {
//            while (_system._curState.aName == this.aName)
//            {

//                if (agent != null)
//                {
//                    agent.SetDestination(AINavAgentCommon.RandomPosition(_system.transform.position, 25));
//                }
//                yield return new WaitForSeconds(timeBetweenPatrol);
//            }
//        }
//        else
//        {
//            //SetDestination_Waypoint(0);
//            while (_system._curState.aName == this.aName)
//            {
//                if (agent != null)
//                {
//                    var tempPos = waypointComp.waypoints[waypointIndex].transform.position;
//                    agent.SetDestination(new Vector2(tempPos.x, tempPos.y)); // cast to vector2
//                }
//                if (agent.hasPath)
//                {
//                    yield return new WaitUntil(() => AINavAgentCommon.HasAgentReachedDestination(agent) == true || agent.isPathStale);
//                    waypointIndex++;
//                    if (waypointIndex >= waypointComp.waypoints.Length)
//                    {
//                        waypointIndex = 0;
//                    }
//                    else if (waypointIndex < 0)
//                    {
//                        waypointIndex = waypointComp.waypoints.Length - 1;
//                    }
//                }
//                yield return null;

//            }
//        }
//        yield break;
//    }
//    //public bool SetDestination_Waypoint(int i)
//    //{
//    //    bool isSet = false;
//    //    if (waypointComp != null)
//    //    {
//    //        var p = waypointComp.waypoints[i].position;
//    //        isSet = agent.SetDestination(p);
//    //    }
//    //    return isSet;
//    //}

//}

//public class Alert : FSMBaseState
//{
//    NavMeshAgent agent;
//    float originalAgentSpeed;
//    public Alert(FSMSystemBase system) : base(system)
//    {
//        agent = ((NPCAISystemTypeA)_system).agent;
//        originalAgentSpeed = agent.speed;

//    }
//    public override IEnumerator OnEnterState()
//    {
//        //agent.speed = 0.3f;
//        //yield return new WaitForSeconds(0.5f);
//        agent.speed = originalAgentSpeed;
//        if (((NPCAISystemTypeA)_system).sensor.visibleRenderers.Count > 0)
//        {
//            ((NPCAISystemTypeA)_system).target = ((NPCAISystemTypeA)_system).sensor.visibleRenderers[0];
//            _system.SetState(((NPCAISystemTypeA)_system).GetStateSOInstanceByName(nameof(Chase)));
//        }
//        yield break;
//    }

//}
//public class Chase : FSMBaseState
//{
//    NavMeshAgent agent;
//    public Transform target;
//    public Chase(FSMSystemBase system) : base(system)
//    {
//        agent = ((NPCAISystemTypeA)_system).agent;
//    }

//    public override void OnEnterState()
//    {
//        while (_system._curState.aName == this.aName)
//        {
//            if (target != null)
//            {
//                Debug.DrawLine(agent.transform.position, (target.position), Color.red);
//                agent.SetDestination(target.transform.position);
//            }
//            else
//            {
//                _system.SetState(((NPCAISystemTypeA)_system).GetStateSOInstanceByName(nameof(GoToLastSeenLocation)));
//            }
//            yield return null;
//        }
//        yield break;
//    }
//}

//public class GoToLastSeenLocation : FSMBaseState
//{
//    NavMeshAgent agent;
//    public GoToLastSeenLocation(FSMSystemBase system) : base(system)
//    {
//        agent = ((NPCAISystemTypeA)_system).agent;
//    }
//    public override IEnumerator OnEnterState()
//    {
//        Debug.DrawLine(agent.transform.position, agent.destination, Color.black);
//        yield return new WaitUntil(() => AINavAgentCommon.HasAgentReachedDestination(agent) == true || agent.isPathStale);
//        _system.SetState(((NPCAISystemTypeA)_system).GetStateSOInstanceByName(nameof(Patrol)));

//        yield break;
//    }
//}

///*
// * 
// *
//// Ref: https://www.reddit.com/r/HiTMAN/comments/4msljs/can_someone_please_explain_the_states_of_47_for_me/
//Trespassing: every person in an area will recognize you as a threat, however if you are discovered, guards will simply escort you to a safe area

//Hostile Area: much like a trespassing zone though you get shot rather than escorted

//suspicious: you've done something pretty dubious and a few guards are going to want to investigate you, run away before they consider you a threat

//compromised : someone now knows your disguise, and may alert others about it if given the chance. // i.e  if you knock someone out and he saw your face, you'll be compromised until you hide their body in a box or closet. That's because after you've hid them they can no longer compromise your outfit. Because nobody is able to wake them.

//Searching: you've done a bad thing, possibly knocked out/killed someone and the body has been found, 
//or you were in combat and escaped, enemies will be alerted and highlighted orange in instinct, these guards will shoot if they see you

//Arresting: a hostile guard has found you and is closing in on you, this gives 47 a short chance to fake a surrender before attacking the guard trying to arrest you... this is very likely to end badly

//combat: basically you're about to be turned into jam

// */