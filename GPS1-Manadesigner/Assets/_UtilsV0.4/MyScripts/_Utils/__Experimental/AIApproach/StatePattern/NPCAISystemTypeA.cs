using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
using System.Linq;
public enum NPCAITypeAReactinStyle
{
    Chase,
    Flee,
    Flock,
    Patrol
}
public class NPCAISystemTypeA : FSMSystemBase
{
    public NPCAITypeAReactinStyle reactionStyle = NPCAITypeAReactinStyle.Chase;
    public List<FSMBaseState> possibleStatesSOInstance;
    public List<string> possibleStatesNames;
    public CommandSlotsHolderManager commandSlotsHolderManager;
    public NavMeshAgent agent;
    public CameraSightPerceptionSensor2D sensor;

    public WaypointPath waypointComp;
    public bool isUseWaypoint = true;

    public RotationConstraint rotationConstraint;

    public override void Awake()
    { 
        base.Awake();

        
        rotationConstraint = GetComponentInChildren<RotationConstraint>();
        if(rotationConstraint.sourceCount > 0)
        {
            if(rotationConstraint.GetSource(0).sourceTransform == null)
            {
                var rotationVector = rotationConstraint.transform.rotation.eulerAngles;
                rotationVector.z = -90;
                rotationConstraint.transform.rotation = Quaternion.Euler(rotationVector);

                ConstraintSource cSrc  = new ConstraintSource() { sourceTransform = Camera.main.transform, weight = 1 };
                rotationConstraint.SetSource(0, cSrc);
                rotationConstraint.locked = true;
            }
        }


        if (commandSlotsHolderManager == null)
        {
            commandSlotsHolderManager = GetComponentInChildren<CommandSlotsHolderManager>();
        }
        if (agent == null)
        {
            agent = GetComponentInChildren<NavMeshAgent>();
            agent.gameObject.AddComponent<NavMeshObstacle>();
        }
        if (_curState != null)
        {
            possibleStatesSOInstance.Add(_curState);
        }

        possibleStatesSOInstance.Add(new PatrolByWaypoints(this));
        possibleStatesSOInstance.Add(new PatrolByRandomPos(this));

        //possibleStatesSOInstance.Add(new Alert(this));
        possibleStatesSOInstance.Add(new Chase(this));
        possibleStatesSOInstance.Add(new Flee(this));

        //possibleStatesSOInstance.Add(new GoToLastSeenLocation(this));

        possibleStatesSOInstance.ForEach(x => possibleStatesNames.Add(x.aName));
        if (waypointComp == null)
        {
            isUseWaypoint = false;
        }
        HandleEnterPatrolState();
    }

    public override void Update()
    {
        base.Update();
        if (sensor.visibleRenderers.Count > 0)
        {
            Transform evaluatedTargetWithinSight = sensor.visibleRenderers.Find(x => x.GetComponent<Player>() != null || x.GetComponentInParent<Player>() != null
            /*&& Physics2D.Linecast(agent.transform.position,x.transform.position, )== false*/);
            if (evaluatedTargetWithinSight != null)
            {
                // Evaluate Target Faction .. i.e Neutral, Friendly, Hostile
                // Pass in the target Transform
                if (reactionStyle == NPCAITypeAReactinStyle.Chase)
                {
                    var tempState = (Chase)GetStateSOInstanceByName(nameof(Chase));
                    tempState.target = evaluatedTargetWithinSight;
                    TryEnterState(tempState);
                }
                else if (reactionStyle == NPCAITypeAReactinStyle.Flee)
                {
                    var tempState = (Flee)GetStateSOInstanceByName(nameof(Flee));
                    tempState.target = evaluatedTargetWithinSight;
                    TryEnterState(tempState);
                }

                //if (_curState.aName != nameof(Chase))
                //{
                //    TryEnterStateByName(nameof(Alert));
                //} 
            }
            else
            {
                HandleEnterPatrolState();
            }
        }
        else
        {
            HandleEnterPatrolState();
        }

        // Refresh Agent destination by itself on Update
        //if (agent.SetDestination(new Vector3(agent.destination.x, agent.destination.y, transform.position.z)) == false)
        //{
        //    //TryEnterStateByName(nameof(PatrolByRandomPos));
        //}
    }

    public void HandleEnterPatrolState()
    {
        if (isUseWaypoint == true)
        {
            var tempState = (PatrolByWaypoints)GetStateSOInstanceByName(nameof(PatrolByWaypoints));
            TryEnterState(tempState);
        }
        else if (isUseWaypoint == false)
        {
            var tempState = (PatrolByRandomPos)GetStateSOInstanceByName(nameof(PatrolByRandomPos));
            TryEnterState(tempState);
        }
    }

    public void TryEnterStateByName(string nextStateName)
    {
        TryEnterState(GetStateSOInstanceByName(nextStateName));
    }

    private void OnEnable()
    {
        //SwitchState(GetStateSOInstanceByName(nameof(Patrol)));
    }

    public FSMBaseState GetStateSOInstanceByName(string stateName)
    {
        return possibleStatesSOInstance[possibleStatesNames.IndexOf(stateName)];
    }

    public Transform target;
    //public bool hasFoundHostile = false;

    //public float chaseRequiredProximityDistance = 80f;
    //public float alertRequiredProximityDistance = 200f;



    private void OnDrawGizmos()
    {
        Gizmos.DrawCube(agent.destination, Vector3.one);
        Gizmos.DrawLine(agent.transform.position, agent.destination);
    }

}
