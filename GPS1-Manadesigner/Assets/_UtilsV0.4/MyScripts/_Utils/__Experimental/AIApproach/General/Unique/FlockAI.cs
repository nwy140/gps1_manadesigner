using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
// Ref: https://software.intel.com/content/www/us/en/develop/articles/fish-flocking-with-unitysimulating-the-behavior-of-object-moving-with-ai.html
// Ref: https://youtu.be/eMpI1eCsIyM?list=PLuLJclBWmeWUWFcmTUzlbAK_pdemtT9TN
public class FlockAI : MonoBehaviour
{
    public SpawnFlockManager spawnFlockManager;
    float speed;

    void Start()
    {
        speed = Random.Range(spawnFlockManager.minSpeed,
                                spawnFlockManager.maxSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnFlockManager.isUseNavAgentInstead == false)
        {
            transform.Translate(0, 0, Time.deltaTime * speed);
        }
        else
        {
            GetComponent<NavMeshAgent>().SetDestination(transform.position+ Vector3.forward * Time.deltaTime * speed);
        }
        ApplyRules();

    }
    void ApplyRules()
    {

        Vector3 vcentre = Vector3.zero;
        Vector3 vavoid = Vector3.zero;
        float gSpeed = 0.01f;
        float nDistance;
        int groupSize = 0;

        foreach (GameObject go in spawnFlockManager.allObjsInFlock)
        {
            if (go != this.gameObject)
            {
                nDistance = Vector3.Distance(go.transform.position, this.transform.position);
                if (nDistance <= spawnFlockManager.neighbourDistance)
                {
                    vcentre += go.transform.position;

                    groupSize++;

                    if (nDistance < 1.0f)
                    {
                        //vavoid = vavoid + (this.transform.position - go.transform.position);
                        vavoid = vavoid + (spawnFlockManager.transform.position - go.transform.position);

                    }

                    FlockAI anotherFlock = go.GetComponent<FlockAI>();
                    gSpeed = gSpeed + anotherFlock.speed;
                }
            }
        }

        if (groupSize > 0)
        {
            vcentre = vcentre / groupSize;
            speed = gSpeed / groupSize;

            Vector3 direction = (vcentre + vavoid) - transform.position;
            if (direction != Vector3.zero && spawnFlockManager.isUseNavAgentInstead == false)
                transform.rotation = Quaternion.Slerp(transform.rotation,
                                                      Quaternion.LookRotation(direction),
                                                      spawnFlockManager.rotationSpeed * Time.deltaTime);
        }
    }
}