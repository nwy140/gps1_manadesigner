using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Ref: https://software.intel.com/content/www/us/en/develop/articles/fish-flocking-with-unitysimulating-the-behavior-of-object-moving-with-ai.html
// Ref: https://youtu.be/eMpI1eCsIyM?list=PLuLJclBWmeWUWFcmTUzlbAK_pdemtT9TN
public class SpawnFlockManager : MonoBehaviour
{
    public GameObject[] objPrefabs;
    public int flockCount;
    public List<GameObject> allObjsInFlock;
    public Vector3 boundLimits = new Vector3(5, 5, 5); 
    public Vector3 goalPos;

    [Header("Use Transform Translate or NavAgent")]
    public bool isUseNavAgentInstead;

    [Header("If Not NavAgent")]
    [Range(0.0f, 5.0f)]
    public float minSpeed;
    [Range(0.0f, 5.0f)]
    public float maxSpeed;
    public float rotationSpeed = 4.0f;

    [Header("Setting")]
    public float neighbourDistance = 3f;

    private void Start()
    {
        for (int i = 0; i < flockCount; i++)
        {
            Vector3 pos = this.transform.position + new Vector3(Random.Range(-boundLimits.x, boundLimits.x),
                                                                  Random.Range(-boundLimits.y, boundLimits.y),
                                                                  Random.Range(-boundLimits.z, boundLimits.z));
            var tempObj = Instantiate(objPrefabs[Random.Range(0, objPrefabs.Length - 1)], pos, Quaternion.identity);

            tempObj.transform.SetParent(transform);

            //var agent = tempObj.GetComponent<NavMeshAgent>();
            //agent.enabled = false;
            var NPCAI = tempObj.GetComponent<NPCAISystemTypeA>();
            NPCAI.enabled = false;
            var flockComp = tempObj.AddComponent<FlockAI>();
            flockComp.spawnFlockManager = this;

            allObjsInFlock.Add(tempObj);
        }

    }

    //private void Update()
    //{
    //    //if (Random.Range(0, 10000) < 50)
    //    //{
    //    //    goalPos = transform.position + new Vector3(Random.Range(-boundLimits, boundLimits),
    //    //        Random.Range(-boundLimits, boundLimits),
    //    //        Random.Range(-boundLimits, boundLimits)
    //    //        );
    //    //}
    //}
}