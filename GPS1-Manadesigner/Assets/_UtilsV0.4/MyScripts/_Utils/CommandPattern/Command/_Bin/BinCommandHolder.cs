//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;
//using UnityEngine.UI;

//public class BinCommandHolder : MonoBehaviour
//{
//    [SerializeField] CommandManager _commandInvoker;
//    public virtual CommandManager commandInvoker { get => _commandInvoker; set => _commandInvoker = value; }

//    [Header("Basic")]
//    public CommandBase CommandFire1;
//    [Header("Negative Edge")]
//    public CommandBase CommandFire2;
//    public GameObject commandFire2ObjRef;


//    #region Commands List
//    [Header("Chain Commands")]
//    public CommandBase Fire2Fire2Fire1Fire2ChainCommand;
//    List<string> Fire2Fire2Fire1Fire2 = new List<string> { "CommandFire2", "CommandFire2", "CommandFire1", "CommandFire2" };
//    public GameObject Fire2Fire2Fire1Fire2ChainCommandObjRef;

//    public CommandBase Fire1Fire1Fire1ChainCommand;
//    List<string> Fire1Fire1Fire1 = new List<string> { "CommandFire1", "CommandFire1", "CommandFire1" };
//    public GameObject Fire1Fire1Fire1ChainCommandObjRef;

//    public CommandBase Fire2Fire2Fire2ChainCommand;
//    List<string> Fire2Fire2Fire2 = new List<string> { "CommandFire2", "CommandFire2", "CommandFire2" };
//    public GameObject Fire2Fire2Fire2ChainCommandObjRef;
//    #endregion Commands List
//    private void OnEnable()
//    {


//    }
//    private void Awake()
//    {
//        CommandFire2.InitGameObj(commandFire2ObjRef);

//        //Fire2Fire2Fire1ChainCommand = ScriptableObject.CreateInstance<DestroyGameObjectCommand>();
//        Fire2Fire2Fire1Fire2ChainCommand.InitGameObj(Fire2Fire2Fire1Fire2ChainCommandObjRef);
//        Fire1Fire1Fire1ChainCommand.InitGameObj(Fire1Fire1Fire1ChainCommandObjRef);
//        Fire2Fire2Fire2ChainCommand.InitGameObj(Fire2Fire2Fire2ChainCommandObjRef);



//    }
//    private void Update()
//    {
//        _commandInvoker.commandQueue_Basic.OnUpdate();
//        _commandInvoker.commandQueue_NegativeEdge.OnUpdate();
//        _commandInvoker.commandQueue_Chain.OnUpdate();


//        if (_commandInvoker.commandQueue_Basic.LogText != null)
//        {
//            if (_commandInvoker.commandQueue_Basic.LogText.isActiveAndEnabled)
//            {
//                int tempSize = 12;
//                var tempArr = _commandInvoker.commandQueue_Basic._commandNamesHistory.queueList.Skip(_commandInvoker.commandQueue_Basic._commandNamesHistory.Count - 1 - tempSize).Take(tempSize);
//                _commandInvoker.commandQueue_Basic.LogText.text = String.Join(", ", tempArr);
//            }
//        }
//        if (_commandInvoker.commandQueue_Chain.LogText != null)
//        {
//            if (_commandInvoker.commandQueue_Chain.LogText.isActiveAndEnabled)
//            {
//                _commandInvoker.commandQueue_Chain.LogText.text = String.Join(", ", _commandInvoker.commandQueue_Chain._commandNamesHistory.queueList.ToArray());
//            }
//        }


//        //Press RightClick twice in a row to Delete Self, Sample
//        if (_commandInvoker.CheckSequence(Fire2Fire2Fire1Fire2, Fire2Fire2Fire1Fire2.Count, Fire2Fire2Fire1Fire2ChainCommand) )//&& _commandInvoker.commandQueue_Chain.Contains(Fire2Fire2Fire1ChainCommand.aName) == false)
//        {
//            _commandInvoker.commandQueue_Chain.ExecuteCommand(Fire2Fire2Fire1Fire2ChainCommand,true);
//            Debug.Log("Cmd | Perform Special Cmd: " + Fire2Fire2Fire1Fire2ChainCommand.aName);
//        }
//         if (_commandInvoker.CheckSequence(Fire1Fire1Fire1, Fire1Fire1Fire1.Count, Fire1Fire1Fire1ChainCommand))//&& _commandInvoker.commandQueue_Chain.Contains(Fire2Fire2Fire1ChainCommand.aName) == false)
//        {
//            _commandInvoker.commandQueue_Chain.ExecuteCommand(Fire1Fire1Fire1ChainCommand, true);
//            Debug.Log("Cmd | Perform Special Cmd: " + Fire1Fire1Fire1ChainCommand.aName);
//        }
//        if (_commandInvoker.CheckSequence(Fire2Fire2Fire2, Fire2Fire2Fire2.Count,Fire2Fire2Fire2ChainCommand))//&& _commandInvoker.commandQueue_Chain.Contains(Fire2Fire2Fire1ChainCommand.aName) == false)
//        {
//            _commandInvoker.commandQueue_Chain.ExecuteCommand(Fire2Fire2Fire2ChainCommand, true);
//            Debug.Log("Cmd | Perform Special Cmd: " + Fire2Fire2Fire2ChainCommand.aName);
//        }
//    }
//}
