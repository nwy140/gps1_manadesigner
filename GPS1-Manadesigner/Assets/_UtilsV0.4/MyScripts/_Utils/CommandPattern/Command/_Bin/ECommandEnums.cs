public enum CommandCastStatus
{
    Cooldown,
    Active,
    Ready,
}

// Check Tekken's Input Combo StyleGuide : Ref: https://youtu.be/K-WhR09Q6TU?list=PLuLJclBWmeWXiumbbwToOD9spR-OzqScz&t=40
public enum ReadStyle
{
    Tap,
    Hold,
    Release
}