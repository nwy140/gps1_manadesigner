using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandManagerOld : MonoBehaviour // Command Buffers
{

    //public GenericDictionary<string,CommandQueue> commandQueues;
    // Variable Naming Convention Follows https://playmorekof.blogspot.com/2019/08/everything-i-know-about-fighting-game.html
    public CommandQueue commandQueue_Basic = new CommandQueue();   // ButtonDown or ButtonUp
    public CommandQueue commandQueue_NegativeEdge = new CommandQueue();  // Button Hold Companion . i.e DPad or Joystick, with axis 
    public CommandQueue commandQueue_Chain = new CommandQueue();  // Chain commands will be added to this queue when a specific combinations of basic commands is executed 


    private void Awake()
    {
        //StartCoroutine(commandQueue_Basic.autoDequeByTime());
        //StartCoroutine(commandQueue_NegativeEdge.autoDequeByTime());
    }
    // TODO: passive commands // called by gamemode . i.e status effect poison
    private void Update()
    {
        commandQueue_Basic.OnUpdate();
        commandQueue_NegativeEdge.OnUpdate();
        commandQueue_Chain.OnUpdate();
    }

    public int CurrentTick = 0;

    //Ref: Modified From Zinac's Answer: https://gamedev.stackexchange.com/questions/43708/fighting-game-and-input-buffering
    //public bool CheckSequence(List<string> sequence, List<ReadStyle> readStyles, int maxDuration)
    //{
    //    int w = sequence.Count - 1;
    //    for (int i = 0; i < maxDuration; ++i)
    //    {
    //        int bIndex = 0;
    //        int bufferSize = commandQueue_Basic._commandNamesHistory.queueList.Count;
    //        if (bufferSize != 0)
    //        {
    //            bIndex = Mathf.Clamp((CurrentTick - i + bufferSize) % bufferSize, 0, commandQueue_Basic._commandHistory.queueList.Count);
    //        }
    //        if (maxDuration > bufferSize)
    //        {
    //            break;
    //        }
    //        var curCmd = commandQueue_Basic._commandHistory.queueList[bIndex];
    //        if (curCmd.isExecuted == true)
    //        {
    //            // Linq Query // https://www.geeksforgeeks.org/linq-query-syntax/
    //            //var result = from s in stringList
    //            //             where s.Contains("Tutorials")
    //            //             select s;
    //            bool isAllowCheck = true;
    //            // Made up query syntax for Our Check Sequence
    //            if (sequence[w].Contains("?q="))
    //            {
    //                // +ve and -ve axis
    //                //if (sequence[w].Contains("+ve"))
    //                //{
    //                //    if (curCmd.axis <= 0)
    //                //    {
    //                //        isAllowCheck = false;
    //                //    }
    //                //}
    //                //if (sequence[w].Contains("-ve"))
    //                //{
    //                //    if (curCmd.axis >= 0)
    //                //    {
    //                //        isAllowCheck = false;
    //                //    }
    //                //}
    //            }
    //            //if (curCmd.readStyle != readStyles[w])
    //            //{
    //            //    isAllowCheck = false;
    //            //}
    //            if (isAllowCheck == true)
    //            {
    //                string direction = curCmd.aName;
    //                if (direction == sequence[w])
    //                    --w;
    //                if (w == -1)//-1)
    //                    return true;
    //            }
    //        }
    //    }
    //    return false;
    //}
    public float maxDefaultAllowedTimeDifferenceCheck = 2f;

    #region CheckSequence


    // also add ignore sequence, that does not affect index // ALSO CHECK IF IGNORE SEQUENCE IS IN SEQUENCE, IF SO, do not call continue or break;
    public bool CheckSequence(string[] sequence, int maxDuration, CommandBase chainCommand)
    {
        int w = sequence.Length - 1;
        List<CommandBase> commandsFound = new List<CommandBase>();
        for (int i = 0; i < maxDuration; ++i)
        {
            int bufferSize = commandQueue_Basic._commandNamesHistory.queueList.Count;

            int bIndex = 0;
            if (bufferSize != 0)
            {
                //bIndex = Mathf.Clamp((CurrentTick - i + bufferSize) % bufferSize, 0, commandQueue_Basic._commandHistory.queueList.Count - 1);
                bIndex = (commandQueue_Basic._commandHistory.queueList.Count - 1 - i);// % bufferSize;
            }
            if (maxDuration > bufferSize)
            {
                break;
            }
            CurrentTick = bIndex;
            var curCmd = commandQueue_Basic._commandHistory.queueList[bIndex];
            if (curCmd.instanceInfo.UsedToExecChainCommands.Contains(chainCommand.name)) break;//continue;
            if (curCmd.instanceInfo._isExecuted == true)
            {
                // Linq Query // https://www.geeksforgeeks.org/linq-query-syntax/
                //var result = from s in stringList
                //             where s.Contains("Tutorials")
                //             select s;
                bool isAllowCheck = true;
                // Made up query syntax for Our Check Sequence
                if (sequence[w].Contains("?q="))
                {
                    // +ve and -ve axis
                    //if (sequence[w].Contains("+ve"))
                    //{
                    //    if (curCmd.axis <= 0)
                    //    {
                    //        isAllowCheck = false;
                    //    }
                    //}
                    //if (sequence[w].Contains("-ve"))
                    //{
                    //    if (curCmd.axis >= 0)
                    //    {
                    //        isAllowCheck = false;
                    //    }
                    //}
                }
                if (i == maxDuration - 1)
                {
                    if (Mathf.Abs(curCmd.TimeStamp - Time.time) > maxDefaultAllowedTimeDifferenceCheck)
                    {
                        return false;
                    }
                }

                //if (curCmd.readStyle != readStyles[w])
                //{
                //    isAllowCheck = false;
                //}
                if (isAllowCheck == true)
                {
                    string direction = curCmd.aName;
                    if (direction == sequence[w])
                    {
                        if (curCmd.TimeStamp > 0)
                        {
                            --w;
                            commandsFound.Add(curCmd);
                        }
                    }
                    if (w == -1)//-1)
                    {
                        //curCmd.timeFrame = -100; // iterated over commands, will have timeFrame 0 as they won't be used again
                        //commandsFound.ForEach(x => x.timeFrame = -100);
                        foreach (var x in commandsFound) { if (x.instanceInfo.UsedToExecChainCommands.Contains(chainCommand.aName) == false) { x.instanceInfo.UsedToExecChainCommands.Add(chainCommand.aName); } }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool CheckSequence(string[] sequence,CommandBase chainCommand)
    {

        return CheckSequence(sequence, sequence.Length, chainCommand);
    }



    #endregion CheckSequence



}
