//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;


//// TODO : Integreate Unity's new input system
//public class CommandHolder : MonoBehaviour
//{
//    [SerializeField] CommandHelper _commandHelper;
//    public virtual CommandHelper commandInvoker { get => _commandHelper; set => _commandHelper = value; }

//    [SerializeField] ChainCommandInvoker _chainCommandInvoker;
//    public virtual ChainCommandInvoker chainCommandInvoker { get => _chainCommandInvoker; set => _chainCommandInvoker = value; }

//    public CommandBase command;
//    private void Update()
//    {
//        #region Basic
//        // Button Down
//        if (Input.GetButtonDown("Fire1"))
//        {
//            _commandHelper.commandQueue_Basic.ExecuteCommand(command, ReadStyle.Tap, true);
//        }
//        if (Input.GetButtonDown("Fire2"))
//        {
//            _commandHelper.commandQueue_Basic.ExecuteCommand(command, ReadStyle.Tap, true);
//        }
//        if (Input.GetButtonDown("Fire3"))
//        {
//            _commandHelper.commandQueue_Basic.UndoLastCommand();//(CommandFire2, true);
//        }
//        #endregion Basic

//        #region Negative Edge
//        // Button Up
//        //var tempCommand = CommandFire2;
//        if (Input.GetButton("Fire2"))
//        {
//            if (_commandHelper.commandQueue_NegativeEdge.Contains(_chainCommandInvoker.CommandFire2.aName) == false)
//            {
//                _commandHelper.commandQueue_NegativeEdge.ExecuteCommand(_chainCommandInvoker.CommandFire2, ReadStyle.Hold, true);
//            }
//        }
//        if (Input.GetButtonUp("Fire2"))
//        {
//            if (_commandHelper.commandQueue_NegativeEdge.Contains(_chainCommandInvoker.CommandFire2.aName) == true)
//            {
//                _commandHelper.commandQueue_NegativeEdge.Remove(_chainCommandInvoker.CommandFire2);
//            }
//        }
//        if (Input.GetButton("Fire1"))
//        {
//            if (_commandHelper.commandQueue_NegativeEdge.Contains(_chainCommandInvoker.CommandFire1.aName) == false)
//            {
//                _commandHelper.commandQueue_NegativeEdge.ExecuteCommand(_chainCommandInvoker.CommandFire1, ReadStyle.Hold, true);
//            }
//        }
//        if (Input.GetButtonUp("Fire1"))
//        {
//            if (_commandHelper.commandQueue_NegativeEdge.Contains(_chainCommandInvoker.CommandFire1.aName) == true)
//            {
//                _commandHelper.commandQueue_NegativeEdge.Remove(_chainCommandInvoker.CommandFire1);
//            }
//        }
//        #endregion Negative Edge
//        _commandHelper.commandQueue_Basic._commandHistory.queueList.ForEach(x => x.OnExternalUpdate());
//        _commandHelper.commandQueue_NegativeEdge._commandHistory.queueList.ForEach(x => x.OnExternalUpdate());

//    }
//}
