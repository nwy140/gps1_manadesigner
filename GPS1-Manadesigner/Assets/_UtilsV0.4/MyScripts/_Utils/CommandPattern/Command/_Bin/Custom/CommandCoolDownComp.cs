using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CommandCoolDownComp : MonoBehaviour
{

    public Image darkMask;
    public Text coolDownTextDisplay;

    [SerializeField] private CommandBase command;
    [SerializeField] private GameObject weaponHolder;
    private Image myButtonImage;
    private AudioSource abilitySource;
    private float coolDownDuration;
    private float nextReadyTime;
    private float coolDownTimeLeft;


    void Start()
    {
        Initialize(command, weaponHolder);
    }

    public void Initialize(CommandBase selectedAbility, GameObject weaponHolder)
    {
        //command = selectedAbility;
        //myButtonImage = GetComponent<Image>();
        //abilitySource = GetComponent<AudioSource>();
        //myButtonImage.sprite = command.aSprite;
        //darkMask.sprite = command.aSprite;
        //coolDownDuration = command.aBaseCoolDown;
        //command.Initialize(weaponHolder);
        //CommandReady();
    }

    // Update is called once per frame
    void Update()
    {
        bool coolDownComplete = (Time.time > nextReadyTime);
        if (coolDownComplete)
        {
            CommandReady();
            //if (Input.GetButtonDown(abilityButtonAxisName))
            //{
            //    ButtonTriggered();
            //}
        }
        else
        {
            CoolDown();
        }
    }

    private void CommandReady()
    {
        if (coolDownTextDisplay != null)
            coolDownTextDisplay.enabled = false;
        if (darkMask != null)
            darkMask.enabled = false;
    }

    private void CoolDown()
    {
        coolDownTimeLeft -= Time.deltaTime;
        float roundedCd = Mathf.Round(coolDownTimeLeft);
        if (coolDownTextDisplay != null)
            coolDownTextDisplay.text = roundedCd.ToString();
        if (darkMask != null)
            darkMask.fillAmount = (coolDownTimeLeft / coolDownDuration);
    }

    private void ButtonTriggered()
    {
        nextReadyTime = coolDownDuration + Time.time;
        coolDownTimeLeft = coolDownDuration;
        if (darkMask != null)
            darkMask.enabled = true;
        if (coolDownTextDisplay != null)
            coolDownTextDisplay.enabled = true;

        //abilitySource.clip = command.aSFXName;
        //abilitySource.Play();
        //command.TriggerAbility();

        //command.Execute();
    }
}