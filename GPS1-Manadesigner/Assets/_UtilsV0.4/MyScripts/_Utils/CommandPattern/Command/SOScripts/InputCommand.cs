using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "SInt_Utils/InputCommand", order = 1)]
public class InputCommand : CommandBase
{
    [Header("Extra Input Related Info ~ Can be used by Non Player AI as well")]
    public ExecExtraInfo execExtraInfo;
    [System.Serializable]
    public struct ExecExtraInfo
    {
        [Header("Input")]
        public int m_PlayerNumber;                  // Used to identify the different players.
                                                    // Variables and Method naming convention taken from InputSystemSamples tankDemo
        public bool m_Execd;                           // Whether or not the shell has been launched with this button press.
        public bool m_ExecButtonPressedThisFrame;      // Will be set to true when the Exec button is initially pressed.
        public bool m_ExecButtonReleasedThisFrame;     // Will be set to true when the Exec button is first released.
        public bool m_ExecButtonDown;                  // Will always be true while the Exec button is held down.

        [Header("Axis Only")]
        public float m_Axis;


    }
    public void SetExecExtraInfoByExecExtraInfo(ExecExtraInfo ex2)
    {
        if (ex2.m_ExecButtonPressedThisFrame || ex2.m_ExecButtonDown)
        {
            SetExecInfo(true);
        }
        else
        {
            SetExecInfo(false);
        }
    }
    [Header("Execution Charged Info")]
    public bool isExecChargedStyle;
    public bool isFullChargeAutoRelease;

    [HideInInspector] public Slider m_AimSlider;                      // A child of the tank that displays the current launch force.
    [HideInInspector] public float m_MinLaunchForce = 15f;            // The force given to the shell if the Exec button is not held.
    [HideInInspector] public float m_MaxLaunchForce = 30f;            // The force given to the shell if the Exec button is held for the max charge time.
    [HideInInspector] public float m_MaxChargeTime = 0.75f;           // How long the shell can charge for before it is Execd at max force.

    [HideInInspector] private float m_CurrentLaunchForce;             // The force that will be given to the shell when the Exec button is released.
    [HideInInspector] private float m_ChargeSpeed;                    // How fast the launch force increases, based on the max charge time.
    public override void OnEnable()
    {
        base.OnEnable();
        // The rate that the launch force charges up is the range of possible forces by the max charge time.
        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
        // When the tank is turned on, reset the launch force and the UI
        m_CurrentLaunchForce = m_MinLaunchForce;

        if (m_AimSlider != null)
        {
            m_AimSlider.value = m_MinLaunchForce;
        }
    }
    private void HandleInputSystem()
    {
        this.cooldownInfo.isCastingNow = execExtraInfo.m_ExecButtonPressedThisFrame;
        if (isFullChargeAutoRelease == false)
        {
            if (execExtraInfo.m_ExecButtonReleasedThisFrame)
                ExecRelease();
        }
        // The slider should have a default value of the minimum launch force.
        //m_AimSlider.value = m_MinLaunchForce;

        // If the max force has been exceeded and the shell hasn't yet been launched...
        if (m_CurrentLaunchForce >= m_MaxLaunchForce && !execExtraInfo.m_Execd)
        {

            // ... use the max force and launch the shell.
            m_CurrentLaunchForce = m_MaxLaunchForce;
            if (isFullChargeAutoRelease)
                ExecRelease();
        }
        // Otherwise, if the Exec button has just started being pressed...
        else if (execExtraInfo.m_ExecButtonPressedThisFrame && !execExtraInfo.m_ExecButtonDown)
        {
            // ... reset the Execd flag and reset the launch force.
            execExtraInfo.m_Execd = false;
            m_CurrentLaunchForce = m_MinLaunchForce;

            // Change the clip to the charging clip and start it playing.
            //m_ShootingAudio.clip = m_ChargingClip;
            //m_ShootingAudio.Play();

            execExtraInfo.m_ExecButtonDown = true;
            execExtraInfo.m_ExecButtonPressedThisFrame = false;

            ExecPress();
        }
        // Otherwise, if the Exec button is being held and the shell hasn't been launched yet...
        //else if (!execExtraInfo.m_ExecButtonPressedThisFrame && execExtraInfo.m_ExecButtonDown && !execExtraInfo.m_Execd && !execExtraInfo.m_ExecButtonReleasedThisFrame)
        else if (!execExtraInfo.m_ExecButtonPressedThisFrame && execExtraInfo.m_ExecButtonDown && !execExtraInfo.m_ExecButtonReleasedThisFrame)
        {
            //// Increment the launch force and update the slider. ~ Moved to ExecDownCharging
            //m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;
            //m_AimSlider.value = m_CurrentLaunchForce;

            ExecDown();
        }
        // Otherwise, if the Exec button is released and the shell hasn't been launched yet...
        else if (execExtraInfo.m_ExecButtonReleasedThisFrame && !execExtraInfo.m_Execd)
        {
            // ... launch the shell.
            TryExecCommand();
        }

        if (execExtraInfo.m_Execd)
        {
            Execd();
        }
    }
    public void SetExecInfo(bool IsPress)
    {
        if (IsPress)
        {
            // Only call this if not in cooldown state
            if (cooldownInfo.castState == CastState.ready)
            {
                if (execExtraInfo.m_ExecButtonDown == false)
                {
                    execExtraInfo.m_ExecButtonPressedThisFrame = true;
                }
                execExtraInfo.m_ExecButtonReleasedThisFrame = false;
            }
        }
        else
        {
            execExtraInfo.m_ExecButtonPressedThisFrame = false;
            if (execExtraInfo.m_Execd == false)
                execExtraInfo.m_ExecButtonReleasedThisFrame = true;
            execExtraInfo.m_ExecButtonDown = false;
        }
    }
    public override void TryExecCommand()
    {
        base.TryExecCommand();

        SetExecInfo(true);
        HandleInputSystem();
    }

    #region  Execution Method stubs ~ See Input Command SO
    public override void ExecPress()
    {
        base.ExecPress();
        if (info._DebugLog)
            Debug.Log(base.aName + base.id + " | " + execInfo.curExecState + " | Axis : " + execExtraInfo.m_Axis);
    }
    public override void ExecDown()
    {
        base.ExecDown();
        if (info._DebugLog)
            Debug.Log(aName + id + " | " + execInfo.curExecState + " | Axis : " + execExtraInfo.m_Axis);
        if (!execExtraInfo.m_Execd)
        {
            if (isExecChargedStyle)
                ExecDownCharging();
        }
    }

    public override void ExecDownCharging()
    {
        base.ExecDownCharging();
        if (info._DebugLog)
            Debug.Log(aName + id + " | " + execInfo.curExecState + " | Axis : " + execExtraInfo.m_Axis);

        //Increment the launch force and update the slider. 
        m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

        if (m_AimSlider != null)
            m_AimSlider.value = m_CurrentLaunchForce;
    }
    public override void ExecRelease()
    {
        base.ExecRelease();
        if (info._DebugLog)
            Debug.Log(aName + id + " | " + execInfo.curExecState + " | Axis : " + execExtraInfo.m_Axis);
        // Set the Execd flag so only Exec is only called once.
        execExtraInfo.m_Execd = true;

        // Reset the button flags.
        execExtraInfo.m_ExecButtonPressedThisFrame = false;
        execExtraInfo.m_ExecButtonReleasedThisFrame = false;
        execExtraInfo.m_ExecButtonDown = false;

        // Create an instance of the shell and store a reference to it's rigidbody.
        //Rigidbody shellInstance =
        //    Instantiate(m_Shell, m_ExecTransform.position, m_ExecTransform.rotation) as Rigidbody;

        // Set the shell's velocity to the launch force in the Exec position's forward direction.
        //shellInstance.velocity = m_CurrentLaunchForce * m_ExecTransform.forward;

        // Change the clip to the firing clip and play it.
        //m_ShootingAudio.clip = m_ExecClip;
        //m_ShootingAudio.Play();

        // Reset the launch force.  This is a precaution in case of missing button events.
        m_CurrentLaunchForce = m_MinLaunchForce;
    }
    public override void Execd()
    {
        base.Execd();
    }
    #endregion  Execution Method stubs ~ See Input Command SO

    #region Input System 

    public override void OnExternalUpdate()
    {
        base.OnExternalUpdate();
        HandleInputSystem();
    }

    //public void CalculateAxisName()
    //{
    //    if ( execExtraInfo.m_Axis > 0)
    //    {
    //        if (query.Contains("-ve_"))
    //            this.query.Replace("-ve_", "+ve_");
    //        else if (query.Contains("+ve_") == false)
    //            this.query += "+ve_";
    //    }
    //    else if ( execExtraInfo.m_Axis < 0)
    //    {
    //        if (query.Contains("+ve_"))
    //            this.query.Replace("+ve_", "-ve_");
    //        else if (query.Contains("-ve_") == false)
    //            this.query += "-ve_";
    //    }
    //    else
    //    {
    //        this.query.Replace("-ve_", "");
    //        this.query.Replace("+ve_", "");
    //    }
    //}
    // The callback from the TanksInputActions Player Input asset that is
    // triggered from the "Exec" action.
    public void OnInput(InputAction.CallbackContext context)
    {
        // We have setup our button press action to be Press and Release
        // trigger behavior in the Press interaction of the Input Action asset.
        // The isPressed property will be true
        // when OnExec is called during initial button press.
        // It will be false when OnExec is called during button release.
        this.execExtraInfo.m_Axis = context.ReadValue<float>();
        if (info._DebugLog)
            Debug.Log("Input System IsPressed: " + context.action.IsPressed());
        //SetExecInfo(context.action.IsPressed());
        SetExecInfo(context.action.IsPressed());

    }
    #endregion Input System 



    #region Unused
    // The callback from the TanksInputActions Player Input asset that is
    // triggered from the "Exec" action.
    public void OnInput(InputValue value)
    {
        // We have setup our button press action to be Press and Release
        // trigger behavior in the Press interaction of the Input Action asset.
        // The isPressed property will be true
        // when OnExec is called during initial button press.
        // It will be false when OnExec is called during button release.

        SetExecInfo(value.isPressed);
    }
    #endregion Unused
}
