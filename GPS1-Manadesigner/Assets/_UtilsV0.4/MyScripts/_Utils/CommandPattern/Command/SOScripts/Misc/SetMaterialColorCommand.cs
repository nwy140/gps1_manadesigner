using UnityEngine;

[CreateAssetMenu(menuName = "SInt_Utils/SetMaterialColorCommand", order = 1)]
public class SetMaterialColorCommand : InputCommand
{
    public Color newColor = Color.white;

    public override void ExecPress()
    {
        base.ExecPress();

        if (instanceInfo.commandTargetObj != null)
        {
            instanceInfo.commandTargetObj.GetComponent<Renderer>().material.color = newColor;
        }
    }
}
