//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[CreateAssetMenu(menuName = "SInt_Utils/CommandTemplateCooldown", order = 1)]
//public class CommandTemplateCooldown : CommandBase
//{

//    #region Cast State And Cooldown
//    [Header("Cast State And Cooldown")]
//    public float cooldownTime;
//    //public float activeTime;
//    public float curCoolDownTimeElapsed;

//    public virtual void Activate(GameObject parentObj) { }
//    public virtual void BeginCooldown(GameObject parentObj) { }
//    public virtual void StayCooldown(GameObject parentObj) { }
//    public virtual void EndCooldown(GameObject parentObj) { }

//    public enum CastState
//    {
//        ready, active, cooldown
//    }

//    public CastState castState = CastState.ready;

//    public bool isCastingAbilityNow;

//    public override void OnEnable()
//    {
//        base.OnEnable();
//    }
//    public override void OnExternalUpdate()
//    {
//        base.OnExternalUpdate();
//        HandleCooldown();
//    }

//    // Old Input System Redundant variables
//    //public KeyCode key; // change this to execute Execute
//    // Execute OnkeyDown
//    // Execute OnKey
//    // Execute Onkey
//    public void HandleCooldown()
//    {
//        // using if statements instead of switch to make things simpler and cleaner
//        if (castState == CastState.ready)
//        {
//            if (isCastingAbilityNow)
//            {
//                Debug.Log("A");

//                this.Activate(instigatorObj);
//                castState = CastState.active;
//                //activeTime = this.activeTime;
//                curCoolDownTimeElapsed = cooldownTime;
//            }
//        }
//        if (castState == CastState.active)
//        {
//            if (curCoolDownTimeElapsed > 0)
//            {
//                curCoolDownTimeElapsed -= Time.deltaTime;
//                this.StayCooldown(instigatorObj);
//            }
//            else
//            {
//                this.BeginCooldown(instigatorObj);
//                castState = CastState.cooldown;
//                //cooldownTime = this.cooldownTime;
//            }
//        }
//        if (castState == CastState.cooldown)
//        {
//            this.EndCooldown(instigatorObj);
//            castState = CastState.ready;
//        }
//    }
//    #endregion Cast State And Cooldown
//}
