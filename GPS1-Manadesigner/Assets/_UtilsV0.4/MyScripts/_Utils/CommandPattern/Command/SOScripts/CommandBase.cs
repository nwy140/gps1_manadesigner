﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;



// Referencing Command Patterns And Scriptable Objects implementation of Ability system
// Ref: https://learn.unity.com/tutorial/create-an-ability-system-with-scriptable-objects#5cf5ecededbc2a36a1bd53b7
[CreateAssetMenu(menuName = "SInt_Utils/CommandBase", order = 1)]
public class CommandBase : ScriptableObject, ICommand
{
    [Header("Scriptable Object Information")]
    public Info info;
    [System.Serializable]
    public struct Info
    {
        public string _aName;
        public string query; // i.e ?q=    +ve or -ve or hold or press or release
        public Sprite aSprite;
        public bool _DebugLog;
    }
    [Header("Runtime Scriptable Object Instance Information")]
    public InstanceInfoRuntime instanceInfo;
    [System.Serializable]
    public struct InstanceInfoRuntime
    {
        public bool _isExecuted; // sets this to true if timestamp is not 0
        public bool _isEnabled;
        public int _id; // Unique ID 
        public float TimeStamp;// Exact Time the command is executed, succesfully // Basic Commands that have been iterated over succesfully by CheckSequence, will have their timeframe set to -100
        [Header("Set In Runtime")]
        public float ElapsedTime;
        public GameObject commandOwnerRootObj; // owner gameobject of this command
        public GameObject commandTargetObj; // i.e Spawn Particle Effect At muzzle
        public CommandAccessorRefsBase commandAccessorRefsComp; // Place all your exec related methods in this component
        public bool isSoInstance;
        public List<string> UsedToExecChainCommands;  // Add all the chain command names that was executed as a result of This command to this list 
        [Header("ChainCommandOnly")]
        public string prevPrimaryCommandName;
        public CommandBase prevPrimaryCommand;
    }
    [Header("Double Click the scriptable object for better view in Runtime")]
    public CooldownInfo cooldownInfo;
    [System.Serializable]
    public struct CooldownInfo
    {
        [Header("Cast State And Cooldown")]
        public float cooldownTime;
        //public float activeTime;
        [Header("Set In Runtime")]
        public float curCoolDownTimeElapsed;
        public CastState castState;
        public bool isCastingNow;
        //public float delayTimeBeforeBeginCooldown;
    }

    #region Cast State And Cooldown
    public enum CastState
    {
        ready, active, cooldown
    }

    public virtual void Activate(GameObject ownerRootParentObj = null) { }
    public virtual void BeginCooldown(GameObject ownerRootParentObj = null) { }
    public virtual void StayCooldown(GameObject ownerRootParentObj = null) { }
    public virtual void EndCooldown(GameObject ownerRootParentObj = null) { }


    // Old Input System Redundant variables
    //public KeyCode key; // change this to execute Execute
    // Execute OnkeyDown
    // Execute OnKey
    // Execute Onkey
    public void HandleCooldown(GameObject ownerRootParentObj)
    {
        // using if statements instead of switch to make things simpler and cleaner
        if (cooldownInfo.castState == CastState.ready)
        {
            if (cooldownInfo.isCastingNow)
            {
                this.Activate(ownerRootParentObj);
                cooldownInfo.castState = CastState.active;
                //activeTime = this.activeTime;
                cooldownInfo.curCoolDownTimeElapsed = cooldownInfo.cooldownTime;
            }
        }
        if (cooldownInfo.castState == CastState.active)
        {
            if (cooldownInfo.curCoolDownTimeElapsed > 0)
            {
                cooldownInfo.curCoolDownTimeElapsed -= Time.deltaTime;
                this.StayCooldown(ownerRootParentObj);
            }
            else
            {
                this.BeginCooldown(ownerRootParentObj);
                cooldownInfo.castState = CastState.cooldown;
                //cooldownTime = this.cooldownTime;
            }
        }
        if (cooldownInfo.castState == CastState.cooldown)
        {
            this.EndCooldown(ownerRootParentObj);
            cooldownInfo.castState = CastState.ready;
        }
    }
    #endregion Cast State And Cooldown
    //var Command External Accessor Component 
    #region Getters and Setters for Info and InfoInstance
    public virtual string aName { get { string temp = this.name.Replace("(Clone)", ""); if (temp == "") { temp = this.GetType().Name; } return temp; } set { info._aName = value; } }
    public virtual float TimeStamp { get => instanceInfo.TimeStamp; set => instanceInfo.TimeStamp = value; }
    public virtual float ElapsedTime { get => instanceInfo.ElapsedTime; set => instanceInfo.ElapsedTime = value; }

    //public virtual float lifespan { get => _lifespan; set => _lifespan = value; }
    public virtual int id { get => GetInstanceID(); set => instanceInfo._id = value; }
    #endregion Getters and Setters for Info and InfoInstance
    public virtual void TryExecCommand() // force execute command without Calling SimulateInput or OnInput
    {
        if (instanceInfo._isEnabled == false) return;
        //instanceInfo._isExecuted = true;
        if (instanceInfo.TimeStamp == 0)
        {
            //_execEnterTimestamp = Time.frameCount;
            instanceInfo.TimeStamp = Time.time;
        }
        if (instanceInfo.ElapsedTime == 0)
        {
            instanceInfo.ElapsedTime = Time.time;
        }
    }
    public virtual void OnExternalUpdate()
    {
        if (instanceInfo._isEnabled == false) return;

        //base.OnExternalUpdate();
        CalculateElapsedTime();
        if (instanceInfo.commandOwnerRootObj != null)
        {
            HandleCooldown(instanceInfo.commandOwnerRootObj);
        }
    }

    public virtual void CalculateElapsedTime()
    {
        if (instanceInfo.ElapsedTime != 0 && instanceInfo._isEnabled)
        {
            instanceInfo.ElapsedTime += Time.deltaTime;
        }

    }
    #region  Execution Method stubs ~ See Input Command SO    [System.Serializable]
    [Header("Execution Info")]
    public ExecutionInfo execInfo;
    [System.Serializable]
    public class ExecutionInfo
    {
        [Header("Set In Runtime")]
        public ExecStatus curExecState;

        // Command Holder Component Events : Event Listener for the Holder components to subscribe to
        public UnityAction ExecPressHolderEvent;
        public UnityAction ExecDownHolderEvent;
        public UnityAction ExecDownChargingHolderEvent;
        public UnityAction ExecReleaseHolderEvent;
        public UnityAction ExecdHolderEvent;

        [Header("Extra Events Listener Outside of CommandHolder")]
        // optional Events : Requires Scriptable Objects 
        public CommandEvent ExecPressEvent;
        public CommandEvent ExecDownEvent;
        public CommandEvent ExecDownChargingEvent;
        public CommandEvent ExecReleaseEvent;
        public CommandEvent ExecdEvent;

    }

    // Scriptable Objects Unity Event Ref: https://www.youtube.com/watch?v=gXD2z_kkAXs&list=PLuLJclBWmeWVQYdl943Fw2hgX-PQHspNF&index=11 

    public virtual void ExecPress()
    {
        if (execInfo.ExecPressEvent != null) execInfo.ExecPressEvent.Raise(instanceInfo.commandOwnerRootObj);
        if (execInfo.ExecPressHolderEvent != null) execInfo.ExecPressHolderEvent.Invoke();

        execInfo.curExecState = ExecStatus.ExecPress;

    }
    public virtual void ExecDown()
    {
        if (execInfo.ExecDownEvent != null) execInfo.ExecDownEvent.Raise(instanceInfo.commandOwnerRootObj);
        if (execInfo.ExecDownHolderEvent  != null) execInfo.ExecDownHolderEvent.Invoke();

        execInfo.curExecState = ExecStatus.ExecDown;
    }

    public virtual void ExecDownCharging()
    {
        if (execInfo.ExecDownChargingEvent != null) execInfo.ExecDownChargingEvent.Raise(instanceInfo.commandOwnerRootObj);
        if (execInfo.ExecDownChargingHolderEvent != null) execInfo.ExecDownChargingHolderEvent.Invoke();

        execInfo.curExecState = ExecStatus.ExecDownCharging;
    }

    public virtual void ExecRelease()
    {
        if (execInfo.ExecReleaseEvent != null) execInfo.ExecReleaseEvent.Raise(instanceInfo.commandOwnerRootObj);
        if (execInfo.ExecReleaseHolderEvent != null) execInfo.ExecReleaseHolderEvent.Invoke();

        execInfo.curExecState = ExecStatus.ExecRelease;
    }

    public virtual void Execd()
    {
        if (execInfo.ExecdEvent != null) execInfo.ExecdEvent.Raise(instanceInfo.commandOwnerRootObj);
        if (execInfo.ExecdHolderEvent != null) execInfo.ExecdHolderEvent.Invoke();

        execInfo.curExecState = ExecStatus.Execd;
    }
    #endregion  Execution Method stubs ~ See Input Command SO


    public virtual void InitGameObj(GameObject ownerRootParentObj)
    {
        instanceInfo.commandOwnerRootObj = ownerRootParentObj;
    }
    public virtual void Init() // Init BaseValues
    {
        info._aName = aName;
        instanceInfo._id = id;

        //if (_lifespan >= 0 && isSoInstance && Application.isPlaying)
        //{
        //    //Destroy(this, _lifespan);
        //    //_ = DisableSelf(_lifespan);
        //}
    }

    public virtual CommandBase CreateNewInstanceOfSelf(GameObject newInstigatorObjRef = null)
    {
        var newInstance = GameObject.Instantiate(this);
        newInstance.instanceInfo.isSoInstance = true;
        newInstance.Init();

        if (newInstigatorObjRef != null)
        {
            newInstance.InitGameObj(newInstigatorObjRef);
        }

        return newInstance;
    }


    //async Task DisableSelf(float time)
    //{
    //    await Task.Delay((int)time* 1000);
    //    //isEnabled = false;
    //}

    #region Build-In Events
    public virtual void Awake() { }

    public virtual void OnEnable()
    {
        Init();
        execInfo.curExecState = ExecStatus.None;
        instanceInfo._isExecuted = false;
        instanceInfo._isEnabled = true;
    }

    public virtual void OnDisable()
    {
        execInfo.curExecState = ExecStatus.None;
        instanceInfo._isEnabled = false;
    }

    public virtual void OnDestroy() { }


    #endregion Build-In Events
    #region Unused
    //public virtual void OnExecEnter() { }
    //public virtual void OnExecStay() { }
    //public virtual void OnExecExit() { }
    public virtual void Undo() { }
    #endregion Unused
}

public enum ExecStatus
{
    None,
    ExecPress,
    ExecDown,
    ExecDownCharging,
    ExecRelease,
    Execd,
}