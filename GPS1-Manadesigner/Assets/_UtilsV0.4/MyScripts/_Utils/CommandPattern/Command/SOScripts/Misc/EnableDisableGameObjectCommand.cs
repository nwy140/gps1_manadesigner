using UnityEngine;

[CreateAssetMenu(menuName = "SInt_Utils/EnableDisableGameObjectCommand", order = 1)]
public class EnableDisableGameObjectCommand : InputCommand
{
    public override void ExecPress()
    {
        base.ExecPress();
        if (instanceInfo.commandTargetObj != null)
            instanceInfo.commandTargetObj.SetActive(!instanceInfo.commandTargetObj.activeInHierarchy);
    }

    //public override void ExecDown()
    //{
    //    base.ExecDown();
    //    ExecPress();
    //}
}
