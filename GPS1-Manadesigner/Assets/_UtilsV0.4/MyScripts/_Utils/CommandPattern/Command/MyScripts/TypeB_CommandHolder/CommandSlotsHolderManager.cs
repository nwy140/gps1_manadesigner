using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CommandSlotsHolderManager : MonoBehaviour
{
    public List<CommandSlotHolder> commandSlotsInChild; // All Input Commands' isSOInstance should be set false by default in this script
    public List<string> commandSlotsNames = new List<string>();
    public GameObject commandOwnerRootObj;
    public int PlayerNumber;
    [SerializeField] CommandQueueHelper _commandHelper;
    public virtual CommandQueueHelper commandHelper { get => _commandHelper; set => _commandHelper = value; }
    private void Awake()
    {
        if (commandOwnerRootObj == null)
        {
            commandOwnerRootObj = gameObject;
        }
        commandSlotsInChild = new List<CommandSlotHolder>(GetComponentsInChildren<CommandSlotHolder>());
        //commandSlotsInChild.ForEach(x => commandSlotsNames.Add(x.commandInstanceInSlot.aName));;

        foreach (var c in commandSlotsInChild.ToArray())
        {
            if (c.isActiveAndEnabled)
            {
                commandSlotsNames.Add(c.commandInstanceInSlot.aName);
                c.commandInstanceInSlot.execExtraInfo.m_PlayerNumber = PlayerNumber;
            }
            else
            {
                commandSlotsInChild.Remove(c);
            }
        }
    }

    public CommandSlotHolder FindCommandSlotByName(string commandName)
    {
        if (this.commandSlotsNames.Contains(commandName))
        {
            return commandSlotsInChild[this.commandSlotsNames.IndexOf(commandName)];
        }
        else
        {
            return null;
        }
    }
}