using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class CommandSlotHolder : MonoBehaviour
{
    [Header("Scriptable Object Asset Preset Settings")]
    public InputCommand commandInstanceSOAsset; // Instance of the Command in this slot

    [Header("Cloned & Instantiated In Runtime")]
    public InputCommand commandInstanceInSlot; // Instance of the Command in this slot

    [HideInInspector] public GameObject commandOwnerRootObj; // Root object, owner that is execs this command
    [Header("Obj Refs")]
    public GameObject commandTargetObj;

    CommandQueueHelper _commandQueueHelper;

    [HideInInspector] public CommandAccessorRefsBase accessorComp;
    public bool isChainCommand;
    private void Awake()
    {
        _commandQueueHelper = GetComponentInParent<CommandQueueHelper>();
        accessorComp = GetComponent<CommandAccessorRefsBase>();
        commandOwnerRootObj = _commandQueueHelper.commandOwnerRootObj;
        commandInstanceInSlot = (InputCommand)commandInstanceSOAsset.CreateNewInstanceOfSelf(commandOwnerRootObj);
        commandInstanceInSlot.instanceInfo.commandTargetObj = commandTargetObj;
        commandInstanceInSlot.instanceInfo.commandAccessorRefsComp = GetComponent<CommandAccessorRefsBase>();
    }
    private void OnEnable()
    {
        commandInstanceInSlot.instanceInfo._isEnabled = true;
        commandInstanceInSlot.execInfo.ExecPressHolderEvent += accessorComp.ExecPress;
        commandInstanceInSlot.execInfo.ExecDownHolderEvent += accessorComp.ExecDown;
        commandInstanceInSlot.execInfo.ExecDownChargingHolderEvent += accessorComp.ExecDownCharging;
        commandInstanceInSlot.execInfo.ExecReleaseHolderEvent += accessorComp.ExecRelease;
        commandInstanceInSlot.execInfo.ExecdHolderEvent += accessorComp.Execd;
    }
    private void OnDisable()
    {
        commandInstanceInSlot.instanceInfo._isEnabled = false;
        commandInstanceInSlot.execInfo.ExecPressHolderEvent -= accessorComp.ExecPress;
        commandInstanceInSlot.execInfo.ExecDownHolderEvent -= accessorComp.ExecDown;
        commandInstanceInSlot.execInfo.ExecDownChargingHolderEvent -= accessorComp.ExecDownCharging;
        commandInstanceInSlot.execInfo.ExecReleaseHolderEvent -= accessorComp.ExecRelease;
        commandInstanceInSlot.execInfo.ExecdHolderEvent += accessorComp.Execd;
    }

    //private void OnValidate()
    //{

    //    if (commandInstanceInSlot != null)
    //    {
    //        this.name = commandInstanceInSlot.aName;
    //    }
    //}
    private void Update()
    {
        //transform.GetChild(0).name = "Extra Event Listeners (Old)";
        if (commandInstanceInSlot != null)
        {
            if (commandInstanceInSlot.instanceInfo.isSoInstance == true)
            {
                commandInstanceInSlot.OnExternalUpdate();
                if (isChainCommand == false)
                {
                    _commandQueueHelper.ExecCommand_HoldRelease((InputCommand)commandInstanceInSlot);
                }
            }
        }


    }

    public void OnInput(InputAction.CallbackContext context)
    {
        if (isActiveAndEnabled)
            _commandQueueHelper.ExecCommand_InputSystem(context, commandInstanceInSlot);
    }

    // Simulate Input can be Called by Both Ai or Player
    public void OnSimulateInput(bool IsPressedThisFrame, float axis)
    {
        if (isActiveAndEnabled)
            _commandQueueHelper.ExecCommand_SimulateInputSystem(IsPressedThisFrame, axis, commandInstanceInSlot);
        //_commandHelper.ExecCommand_SimulateInputSystem(true, 1, commandInstanceInSlot);
    }
    // Simulate Input can be Called by Both Ai or Player
    public void OnSimulateInputPress1Frame()
    {
        StartCoroutine(OnSimulateInputPress1Frame_Couroutine());
    }
    IEnumerator OnSimulateInputPress1Frame_Couroutine()
    {
        if (isActiveAndEnabled)
            _commandQueueHelper.ExecCommand_SimulateInputSystem(true, 1, commandInstanceInSlot);
        yield return null;
        _commandQueueHelper.ExecCommand_SimulateInputSystem(false, 0, commandInstanceInSlot);
        yield return null;
    }
    public void OnSimulateInput(bool IsPressedThisFrame)
    {
        if (isActiveAndEnabled)
            _commandQueueHelper.ExecCommand_SimulateInputSystem(IsPressedThisFrame, IsPressedThisFrame ? 1 : 0, commandInstanceInSlot);
        //_commandHelper.ExecCommand_SimulateInputSystem(true, 1, commandInstanceInSlot);
    }


}

