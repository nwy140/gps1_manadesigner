using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ChainCommandInvoker : MonoBehaviour
{
    [SerializeField] CommandQueueHelper _commandQueueHelper;
    public virtual CommandQueueHelper commandQueueHelper { get => _commandQueueHelper; set => _commandQueueHelper = value; }

    public CommandSlotsHolderManager commandSlotsHolderManager;
    // i.e <commandName, <List<string> sequence, CommandBase>
    #region Commands List
    #endregion Commands List

    [Header("Invoke commands from below with matching sequence was found in CommandHelper")]
    public List<ChainCommandData> chainCommandDatas;

    private void Awake()
    {
        if (_commandQueueHelper == null)
            commandQueueHelper = GetComponent<CommandQueueHelper>();
        for (int i = 0; i < chainCommandDatas.Count; i++)
        {
            // Turn all commands into instances of themselves
            if (chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot != null)
                chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot.CreateNewInstanceOfSelf();
        }
    }
    private void Update()
    {

        for (int i = 0; i < chainCommandDatas.Count; i++)
        {
            var prevPrimaryCommand = (InputCommand)chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot.instanceInfo.prevPrimaryCommand;
            var chainCommandInstance = ((InputCommand)chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot);
            if (_commandQueueHelper.CheckSequence("Primary", chainCommandDatas[i]))//&& _commandInvoker.commandQueue_Chain.Contains(Fire2Fire2Fire1ChainCommand.aName) == false)
            {// 
             //if (ada(commandSlotsHolderManager.FindCommandSlotByName(chainCommandDatas[i].Sequence[chainCommandDatas[i].Sequence.Length - 1]).commandInstanceInSlot, chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot))
             //{
                chainCommandInstance.instanceInfo.prevPrimaryCommandName = chainCommandDatas[i].Sequence[chainCommandDatas[i].Sequence.Length - 1];
                chainCommandInstance.instanceInfo.prevPrimaryCommand = commandSlotsHolderManager.FindCommandSlotByName(chainCommandInstance.instanceInfo.prevPrimaryCommandName).commandInstanceInSlot;
                commandQueueHelper.commandQueuesDictionary["Chain"].TryExecuteCommand(chainCommandInstance, false);
                //chainCommandInstance.execExtraInfo = commandSlotsHolderManager.FindCommandSlotByName(chainCommandDatas[i].Sequence[chainCommandDatas[i].Sequence.Length - 1]).commandInstanceInSlot.execExtraInfo;

                chainCommandInstance.SetExecExtraInfoByExecExtraInfo(commandSlotsHolderManager.FindCommandSlotByName(chainCommandDatas[i].Sequence[chainCommandDatas[i].Sequence.Length - 1]).commandInstanceInSlot.execExtraInfo);
            }
            else
            {
                //chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot.SetExecInfo(false);
                chainCommandInstance = chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot;

                if (prevPrimaryCommand == null)
                {
                    //chainCommandInstance.execExtraInfo = default; // reset all values in exec extra info
                    chainCommandInstance.SetExecExtraInfoByExecExtraInfo(default);
                }
                else
                {
                    //chainCommandInstance.execExtraInfo = prevPrimaryCommand.execExtraInfo; // take exec extra info from prevPrimaryCommand
                    chainCommandInstance.SetExecExtraInfoByExecExtraInfo(prevPrimaryCommand.execExtraInfo); 
                }

                if (chainCommandInstance.execExtraInfo.m_ExecButtonReleasedThisFrame == true ||( chainCommandInstance.execExtraInfo.m_ExecButtonDown == false && chainCommandInstance.execExtraInfo.m_ExecButtonPressedThisFrame == false))
                {
                    chainCommandDatas[i].chainCommandSlot.commandInstanceInSlot.instanceInfo.prevPrimaryCommand = null;
                }
            }
        }
    }

    #region Unused
    //string[] GetSequenceByCommandName(CommandBase command)
    //{
    //    return chainSequences[chainCommandDictionary[command]].Split('_');
    //}
    #endregion Unused
}

