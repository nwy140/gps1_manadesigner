﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CommandStack
{
    // DSA Video On Stack: https://youtu.be/oz9cEqFynHU?list=PLuLJclBWmeWWfSv_il_sJWLrdU6C_QYFr&t=507   
    public Stack<ICommand> _commandHistory = new Stack<ICommand>();

    public virtual void ExecuteCommand(ICommand command)
    {
        command.TryExecCommand();
        _commandHistory.Push(command);
    }

    public virtual void UndoLastCommand()
    {
        if (_commandHistory.Count <= 0)
            return;

        _commandHistory.Pop().Undo();
    }

}

