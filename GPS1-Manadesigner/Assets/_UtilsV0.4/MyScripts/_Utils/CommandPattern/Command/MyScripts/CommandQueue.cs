﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CommandQueue
{
    // DSA Video On Stack: https://youtu.be/oz9cEqFynHU?list=PLuLJclBWmeWWfSv_il_sJWLrdU6C_QYFr&t=507   
    public SerializableQueueList<string> _commandNamesHistory = new SerializableQueueList<string>();
    public SerializableQueueList<float> _commandTimeStamps = new SerializableQueueList<float>();
    public SerializableQueueList<CommandBase> _commandHistory = new SerializableQueueList<CommandBase>();
    public int maxCount = 30;//max Queue Size
    public Text LogText;

    //public List<CommandBase> _commandHistoryInspectorView = new List<CommandBase>();
    //public float timeForAutoDequeue = 0;


    //bool isAutoDeqeueCouroutineRunning;
    //public IEnumerator autoDequeByTime()
    //{
        //if (isAutoDeqeueCouroutineRunning == false)
        //{
        //    isAutoDeqeueCouroutineRunning = true;
        //    while (timeForAutoDequeue > 0)
        //    {
        //        yield return new WaitForSeconds(timeForAutoDequeue);
        //        if (_commandHistory.Count > 0)
        //        {
        //            _commandHistory.Dequeue();
        //        }
        //        if (_commandNamesHistory.Count > 0)
        //        {
        //            _commandNamesHistory.Dequeue();
        //        }
        //        if (_commandTimeStamps.Count > 0)
        //        {
        //            _commandTimeStamps.Dequeue();
        //        }
        //    }
        //    isAutoDeqeueCouroutineRunning = false;
        //}
        //yield return null;
    //}

    public void OnUpdate()
    {
        foreach (var c in _commandHistory.queueList)
        {
            c.CalculateElapsedTime();
        }
        while (Count > maxCount && Count > maxCount)
        {
            Dequeue();
        }
        DisplayLogText();
    }

    public void DisplayLogText()
    {
        if (LogText != null)
        {
            LogText.text = "";
            // Loop over strings.
            foreach (var s in _commandNamesHistory.queueList)
            {
                LogText.text += s + " | ";
            }
        }
    }

    public void Enqueue(CommandBase command)
    {
        if (Count > 0)
        {
            if (_commandNamesHistory.queueList[Count - 1] == command.aName && _commandTimeStamps.queueList[Count - 1] == command.TimeStamp)
            {
                return;
            }
            if (Count > 1 && _commandNamesHistory.queueList[Count - 2] == command.aName && _commandTimeStamps.queueList[Count - 2] == command.TimeStamp)
            {
                return;
            }
        }
        _commandHistory.Enqueue(command);
        _commandNamesHistory.Enqueue(command.aName);
        _commandTimeStamps.Enqueue(command.TimeStamp);
        OnQueueModified();
    }

    public CommandBase Dequeue()
    {
        OnQueueModified();
        _commandNamesHistory.Dequeue();
        _commandTimeStamps.Dequeue();
        return _commandHistory.Dequeue(); 
    }
    public void OnQueueModified()
    {
        //Camera.main.GetComponent<MonoBehaviour>().StartCoroutine(autoDequeByTime());
    }
    public CommandBase TryExecuteCommand(CommandBase command, bool willInstanteACloneSO = false)
    {
        var cmd = command;
        // Instantiate Clone scriptable Object instance // Ref: https://forum.unity.com/threads/create-copy-of-scriptableobject-during-runtime.355933/
        if (willInstanteACloneSO)
        {
            cmd = command.CreateNewInstanceOfSelf();
            cmd.info._DebugLog = false;
        }
        cmd.TryExecCommand();
        Enqueue(cmd);
        return cmd;
    }

    public virtual void UndoLastCommand()
    {
        if (_commandHistory.Count <= 0)
            return;
        Dequeue().Undo();

        //if (_commandHistoryInspectorView.Count > 0)
        //{
        //    _commandHistoryInspectorView.RemoveAt(0);
        //}
        //_commandHistory.CopyTo(_commandHistoryInspectorView, _commandHistoryInspectorView.Count - 1);
        //_commandHistoryInspectorView = new List<CommandBase>(_commandHistory.ToArray());
    }

    #region Custom methods

    public void Remove(CommandBase c) // Only used this method for the HoldDown element in the dictionary
    {
        //return _commandHistory.queueList.Remove(c) && _commandNamesHistory.queueList.Remove(c.aName);
        int index = _commandNamesHistory.queueList.IndexOf(c.aName);
        RemoveAt(index);
    }

    public void RemoveAt(int index)
    {
        _commandHistory.RemoveAt(index);
        _commandNamesHistory.RemoveAt(index);
        _commandTimeStamps.RemoveAt(index);
        OnQueueModified();
    }
    public bool Contains(string commandName)
    {
        return _commandNamesHistory.Contains(commandName);
    }
    public void Clear()
    {
        _commandNamesHistory.queueList.Clear();
        _commandHistory.queueList.Clear();
        _commandTimeStamps.queueList.Clear();
        OnQueueModified();
    }
    public int Count { get => _commandNamesHistory.Count; }
    #endregion Custom methods
}



