using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ChainCommandInvokerOldTypeA : MonoBehaviour
{
    [SerializeField] CommandQueueHelper _commandQueueHelper;
    public virtual CommandQueueHelper commandQueueHelper { get => _commandQueueHelper; set => _commandQueueHelper = value; }

    // i.e <commandName, <List<string> sequence, CommandBase>
    #region Commands List
    #endregion Commands List

    [Header("Invoke commands from below with matching sequence was found in CommandHelper")]
    public List<ChainCommandDataOld> chainCommandDatas;

    private void Awake()
    {
        for (int i = 0; i < chainCommandDatas.Count; i++)
        {
            // Turn all commands into instances of themselves
            chainCommandDatas[i].command.InitGameObj(chainCommandDatas[i].instigatorObj);
            chainCommandDatas[i].command.CreateNewInstanceOfSelf();
        }
    }
    private void Update()
    {
        foreach (var c in chainCommandDatas)
        {
            c.command.OnExternalUpdate();
        }
        for (int i = 0; i < chainCommandDatas.Count; i++)
        {
            if (CheckSequenceOld("Primary", chainCommandDatas[i]))//&& _commandInvoker.commandQueue_Chain.Contains(Fire2Fire2Fire1ChainCommand.aName) == false)
            {
                var chainCmdInstance = _commandQueueHelper.commandQueuesDictionary["Chain"].TryExecuteCommand(chainCommandDatas[i].command, true);
                chainCmdInstance.instanceInfo.prevPrimaryCommandName = chainCommandDatas[i].Sequence[chainCommandDatas[i].Sequence.Length - 1];
                Debug.Log("Cmd | Perform Special Cmd: " + chainCommandDatas[i].command.aName);
            }
        }
    }

    public bool CheckSequenceOld(string entryQueueName, ChainCommandDataOld c)
    {
        if (c.holdDownSequence.Length > 0)
        {
            // Check if all elements in hold down sequence is inside command HoldDown Queue
            if (c.holdDownSequence.All(elem => _commandQueueHelper.commandQueuesDictionary["HoldDown"]._commandNamesHistory.Contains(elem)) == false)//commandQueuesDictionary["HoldDown"]._commandNamesHistory.Contains(c.command.aName) == false)
            {
                return false;
            }
        }
        return _commandQueueHelper.CheckSequence(entryQueueName, c.Sequence, c.Sequence_Query,null, c.Sequence.Length, c.command);
    }

    #region Unused
    //string[] GetSequenceByCommandName(CommandBase command)
    //{
    //    return chainSequences[chainCommandDictionary[command]].Split('_');
    //}
    #endregion Unused
}

[System.Serializable]
public class ChainCommandDataOld
{
    public CommandBase command;
    public string[] Sequence;
    public GameObject instigatorObj;
    [Header("Optional")]
    public string[] Sequence_Query; // +ve/-ve and readstyle
    public int[] Sequence_Axis; // negative or positive axis
    public string[] holdDownSequence;

    // TODO : Below
    public string ignoreCategory; // i.e Axis, Tertiary, Secondary // TODO: Implement
    [Header("Negative Ecge has not been implemented yet. Not all game use it. Buts its a nice to have")]
    public string[] negativeEdgeSequence;
}