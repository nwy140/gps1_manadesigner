using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputHandlerOld_NewInputSystem : MonoBehaviour
{
    public List<CommandBase> inputCommands; // All Input Commands' isSOInstance should be set false by default in this script
    List<string> inputCommandsNames = new List<string>();
    //public List<CommandBase> inputCommands_AxisOnly; // All Input Commands' isSOInstance should be set to false

    public GameObject instigatorObj;

    [SerializeField] CommandQueueHelper _commandQueueHelper;
    public virtual CommandQueueHelper commandQueueHelper { get => _commandQueueHelper; set => _commandQueueHelper = value; }
    private void Awake()
    {
        if (instigatorObj == null)
        {
            instigatorObj = gameObject;
        }
        for (int i = 0; i < inputCommands.Count; i++)
        {
            inputCommands[i] = inputCommands[i].CreateNewInstanceOfSelf(instigatorObj);
            inputCommandsNames.Add(inputCommands[i].aName);
        }
        //for (int i = 0; i < inputCommands_AxisOnly.Count; i++)
        //{
        //    inputCommands_AxisOnly[i] = inputCommands_AxisOnly[i].CreateNewInstanceOfSelf(instigatorObj);
        //}
    }

    public CommandBase GetCommandByNameInCommandList(string commandName)
    {
        return inputCommands[inputCommandsNames.IndexOf(commandName)];
    }

    private void Update()
    {
        for (int i = 0; i < inputCommands.Count; i++)
        {
            inputCommands[i].OnExternalUpdate();
            _commandQueueHelper.ExecCommand_HoldRelease((InputCommand)inputCommands[i]);
        }
        //for (int i = 0; i < inputCommands_AxisOnly.Count; i++)
        //{
        //    inputCommands_AxisOnly[i].OnExternalUpdate();
        //    ExecutionStyleB_HoldRelease((InputCommand)inputCommands_AxisOnly[i]);
        //}
    }
    // Called By the PlayerActions Component (Requires Invoke Unity Event Behavior)
    // And The New Unity Input System's RebindUISampleActionsModified SO Asset's Generated Class
    public void OnCommandSlot0(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("CommandSlot0"));
    }

    public void OnCommandSlot1(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("CommandSlot1"));
    }

    public void OnCommandSlot2(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("CommandSlot2"));
    }

    public void OnCommandSlot3(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("CommandSlot3"));
    }

    public void OnSecondaryCommandSlot4(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("CommandSlot4"));
    }

    public void OnSecondaryCommandSlot5(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("SecondaryCommandSlot5"));
    }
    #region 1D Vector Axis

    public void OnTertiaryCommandSlotAxis6(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("TertiaryCommandSlotAxis6"));

    }

    public void OnTertiaryCommandSlotAxis7(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("TertiaryCommandSlotAxis7"));

    }
    #endregion 1D Vector Axis

    public void OnSelect(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("Select"));


    }
    public void OnStart(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("Start"));
    }


    public void OnDPadAxisHorizontal(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("DPadAxisHorizontal"));

    }

    public void OnDPadAxisVertical(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("DPadAxisVertical"));

    }

    public void OnLeftStickAxisHorizontal(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("LeftStickAxisHorizontal"));

    }

    public void OnLeftStickAxisVertical(InputAction.CallbackContext context)
    {
        _commandQueueHelper.ExecCommand_InputSystem(context, (InputCommand)GetCommandByNameInCommandList("LeftStickAxisVertical"));

    }

    public void OnRightStickAxisHorizontal(InputAction.CallbackContext context)
    {
        //ExecutionStyleA_InputSystem(context, 14);
    }

    public void OnRightStickAxisVertical(InputAction.CallbackContext context)
    {
        //ExecutionStyleA_InputSystem(context, 15);
    }






    //RebindUISampleActionsModified inputActions;


    //// Update is called once per frame
    //void Update()
    //{
    //    //foreach ( var c in inputCommands)
    //    //{
    //    //    if(c is InputCommand)
    //    //    {
    //    //        c
    //    //    }
    //    //}
    //}

}

// https://forum.unity.com/threads/playerinput-prefab-calls-action-events-when-using-player-input-manager.1120189/
// https://forum.unity.com/threads/unity-new-input-system-multiplayereventsystem-and-inputsystemuiinputmodule-is-commented-out.885214/


/*
 #region Execution Style B (Axis)

    // Add all Press keys at this frame to Primary dictionary element
    // Add all heldDown keys to HoldDown Dictionary Element, Remove them from that element when Released Keys

    // TODO: SimulateInput Commands for AI
    void ExecutionStyleB_InputSystem(InputAction.CallbackContext context, bool isXAxis = false, int index = 0, string queueDictionaryElementName = "Primary")
    {
        var curCommand = ((InputCommand)inputCommands_AxisOnly[index]);
        curCommand.execExtraInfo.m_Axis = isXAxis ? context.ReadValue<Vector2>().x : context.ReadValue<Vector2>().y;
        //if(curCommand.execExtraInfo.m_Axis==0 ) return;
        
        curCommand.OnInput(context);
        if (context.action.WasPressedThisFrame())
        {
            ExecutionStyleB_Press(((InputCommand)inputCommands_AxisOnly[index]), queueDictionaryElementName);
        }
    }
    void ExecutionStyleB_Press(InputCommand curCommand, string queueDictionaryElementName = "Primary")
    {
        if (curCommand.m_ExecButtonPressedThisFrame)
        {
            //var tempCommand = curCommand.CreateNewInstanceOfSelf();
            // Enqueue an instance of our current input command// For Logging And Input Buffer Purposes

            // Do not generate a new instance of command for axis
            _commandHelper.commandQueuesDictionary[queueDictionaryElementName].TryExecuteCommand(curCommand, true);
        }
    }
    void ExecutionStyleB_HoldRelease(InputCommand curCommand)
    {
        if (_commandHelper.commandQueuesDictionary["Axis"]._commandNamesHistory.Contains(curCommand.aName) == false)
        {
            if (curCommand.m_ExecButtonDown && curCommand.execExtraInfo.m_Axis != 0)
            {
                // Do not generate a new instance of command for axis
                _commandHelper.commandQueuesDictionary["Axis"].TryExecuteCommand(curCommand, false);
            }
        }
        else
        {
            if (curCommand.execExtraInfo.m_Axis == 0)
            {
                _commandHelper.commandQueuesDictionary["Axis"].Remove(curCommand);
            }
        }
    }
    #endregion Execution Style B
    #region 2D Vector Axis (Unity Input System does not work well with 2D Vector Axis, 1D Vectors are still better
    //public void OnLeftStickAxis(InputAction.CallbackContext context)
    //{
    //    //ExecutionStyleB_InputSystem(context);
    //    Debug.Log("lEFTaX");
    //    ExecutionStyleB_InputSystem(context, true, 0, "Primary");
    //    ExecutionStyleB_InputSystem(context, false, 1, "Primary");
    //}

    //public void OnRightStickAxis(InputAction.CallbackContext context)
    //{
    //    //ExecutionStyleB_InputSystem(context, true, 14);
    //    //ExecutionStyleB_InputSystem(context, false, 15);
    //}
    //public void OnDPad(InputAction.CallbackContext context)
    //{
    //    ExecutionStyleB_InputSystem(context, true, 2, "Primary");
    //    ExecutionStyleB_InputSystem(context, false, 3, "Primary");
    //}


    #endregion 2D Vector Axis

 */