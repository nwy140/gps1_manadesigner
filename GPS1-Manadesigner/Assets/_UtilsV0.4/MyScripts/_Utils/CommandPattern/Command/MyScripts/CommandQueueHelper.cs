using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class CommandQueueHelper : MonoBehaviour // Command Buffers
{
    public GameObject commandOwnerRootObj;

    //public GenericDictionary<string,CommandQueue> commandQueues;
    // Naming Convention Follows https://playmorekof.blogspot.com/2019/08/everything-i-know-about-fighting-game.html
    public GenericDictionary<string, CommandQueue> commandQueuesDictionary = new GenericDictionary<string, CommandQueue>()
        {
            { "Primary", new CommandQueue()}, // Primary - i.e CommandSlot0-CommandSlot3
            { "Secondary", new CommandQueue()} ,
            { "Tertiary", new CommandQueue()},
            { "Axis", new CommandQueue()},
            { "HoldDown", new CommandQueue()},
            { "NegativeEdge", new CommandQueue()},  //Negative Edge Ref: https://www.giantbomb.com/negative-edge/3015-1144/
            { "Chain", new CommandQueue()},
        };

    private void Start()
    {
        foreach (var c in commandQueuesDictionary)
        {
            c.Value.Clear();
        }
    }
    private void OnDisable()
    {
        foreach (var c in commandQueuesDictionary)
        {
            c.Value.Clear();
        }
    }
    //private void OnApplicationFocus(bool focus)
    //{
    //    if (focus == false && Debug.isDebugBuild == false)
    //    {
    //        foreach (var c in commandQueuesDictionary)
    //        {
    //            c.Value.Clear();
    //        }
    //    }
    //}
    // TODO: passive commands // called by gamemode . i.e status effect poison
    private void Update()
    {
        foreach (var c in commandQueuesDictionary)
        {
            c.Value.OnUpdate();

        }
    }

    public int CurrentTick = 0;


    public float maxDefaultAllowedTimeDifferenceCheck = 2f;

    #region CheckSequence
    // also add ignore sequence, that does not affect index // ALSO CHECK IF IGNORE SEQUENCE IS IN SEQUENCE, IF SO, do not call continue or break;
    public bool CheckSequence(string entryQueueName, string[] sequence, string[] Sequence_Query, string[] ignoreCategory, int maxDuration, CommandBase chainCommand)
    {
        int w = sequence.Length - 1;
        List<CommandBase> commandsFound = new List<CommandBase>();
        for (int i = 0; i < maxDuration; ++i)
        {
            int bufferSize = commandQueuesDictionary[entryQueueName].Count;

            int bIndex = 0;
            if (bufferSize != 0)
            {
                //bIndex = Mathf.Clamp((CurrentTick - i + bufferSize) % bufferSize, 0, commandQueue_Basic._commandHistory.queueList.Count - 1);
                bIndex = (commandQueuesDictionary[entryQueueName].Count - 1 - i);// % bufferSize;
            }
            if (maxDuration > bufferSize)
            {
                break;
            }
            CurrentTick = bIndex;
            var curCmd = commandQueuesDictionary[entryQueueName]._commandHistory.queueList[bIndex];
            if (ignoreCategory.Length > 0)
            {

                if (ignoreCategory.Any(elem => (curCmd.info._aName.Contains(elem) && elem!="") == true) )
                {
                    maxDuration++;
                }
            }
            if (curCmd.instanceInfo.UsedToExecChainCommands.Contains(chainCommand.name)) break;//continue;

            if (curCmd.instanceInfo._isExecuted == false)
            {
                // Linq Query // https://www.geeksforgeeks.org/linq-query-syntax/
                //var result = from s in stringList
                //             where s.Contains("Tutorials")
                //             select s;
                bool isAllowCheck = true;
                // Made up query syntax for Our Check Sequence
                //if (Sequence_Query[w].Contains("?q="))
                if (Sequence_Query.Length > 0 && w < sequence.Length)
                {
                    //+ve and - ve axis
                    if (curCmd is InputCommand && sequence[w]!="")
                    {
                        var inputCmd = (InputCommand)curCmd;
                        if (Sequence_Query[w].Contains("+ve"))
                        {
                            if (inputCmd.execExtraInfo.m_Axis < 0)
                            {
                                isAllowCheck = false;
                            }
                        }
                        else if (Sequence_Query[w].Contains("-ve"))
                        {
                            if (inputCmd.execExtraInfo.m_Axis > 0)
                            {
                                isAllowCheck = false;
                            }
                        }
                    }
                }
                if (i == maxDuration - 1)
                {
                    if (Mathf.Abs(curCmd.TimeStamp - Time.time) > maxDefaultAllowedTimeDifferenceCheck)
                    {
                        return false;
                    }
                }

                //if (curCmd.readStyle != readStyles[w])
                //{
                //    isAllowCheck = false;
                //}
                if (isAllowCheck == true)
                {
                    string direction = curCmd.aName;
                    if (direction == sequence[w])
                    {
                        if (curCmd.TimeStamp > 0)
                        {
                            --w;
                            commandsFound.Add(curCmd);
                        }
                    }
                    if (w == -1)//-1)
                    {
                        //curCmd.timeFrame = -100; // iterated over commands, will have timeFrame 0 as they won't be used again
                        //commandsFound.ForEach(x => x.timeFrame = -100);
                        foreach (var x in commandsFound)
                        {
                            if (x.instanceInfo.UsedToExecChainCommands.Contains(chainCommand.aName) == false)
                            {
                                x.instanceInfo.UsedToExecChainCommands.Add(chainCommand.aName);
                            }
                            x.instanceInfo._isExecuted = true;
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public bool CheckSequence(string entryQueueName, ChainCommandData c)
    {
        if (c.holdDownSequence.Length > 0)
        {
            // Check if all elements in hold down sequence is inside command HoldDown Queue
            if (c.holdDownSequence.All(elem => commandQueuesDictionary["HoldDown"]._commandNamesHistory.Contains(elem)) == false)//commandQueuesDictionary["HoldDown"]._commandNamesHistory.Contains(c.command.aName) == false)
            {
                return false;
            }
        }
        return CheckSequence(entryQueueName, c.Sequence, c.Sequence_Query, c.ignoreCategory, c.Sequence.Length, c.chainCommandSlot.commandInstanceInSlot);
    }


    #endregion CheckSequence

    #region Execution Style A
    // Exposed to Both AI And PlayerInput 
    // Can be used to Simulate Input Commands for AI
    // Do not write AI and Player scripts seperately,
    // Be Modular, have them all call from these methods  v    

    // Add all Press keys at this frame to Primary dictionary element
    // Add all heldDown keys to HoldDown Dictionary Element, Remove them from that element when Released Keys

    // TODO: SimulateInput Commands for AI
    public void ExecCommand_InputSystem(InputAction.CallbackContext context, InputCommand curCommand, string queueDictionaryElementName = "Primary")
    {
        curCommand.OnInput(context);
        if (context.action.WasPressedThisFrame())
        {
            ExecCommand_Press(curCommand, queueDictionaryElementName);
        }
    }
    public void ExecCommand_SimulateInputSystem(bool IsPressedThisFrame, float axis, InputCommand curCommand, string queueDictionaryElementName = "Primary")
    {
        curCommand.execExtraInfo.m_Axis = axis;
        curCommand.SetExecInfo(IsPressedThisFrame);
        if (IsPressedThisFrame)
        {
            ExecCommand_Press(curCommand, queueDictionaryElementName);
        }
    }
    public void ExecCommand_SimulateInputSystem(bool IsPressedThisFrame, InputCommand curCommand, string queueDictionaryElementName = "Primary")
    {
        curCommand.SetExecInfo(IsPressedThisFrame);
        if (IsPressedThisFrame)
        {
            curCommand.execExtraInfo.m_Axis = 1;

            ExecCommand_Press(curCommand, queueDictionaryElementName);
        }
        else
        {
            curCommand.execExtraInfo.m_Axis = 0;
        }
    }
    public void ExecCommand_Press(InputCommand curCommand, string queueDictionaryElementName = "Primary")
    {
        if (curCommand.execExtraInfo.m_ExecButtonPressedThisFrame)
        {
            //var tempCommand = curCommand.CreateNewInstanceOfSelf();
            // Enqueue an instance of our current input command// For Logging And Input Buffer Purposes
            var temp = this.commandQueuesDictionary[queueDictionaryElementName].TryExecuteCommand(curCommand, true);
            temp.info._DebugLog = false;
        }
    }
    public void ExecCommand_HoldRelease(InputCommand curCommand)
    {
        if (this.commandQueuesDictionary["HoldDown"]._commandNamesHistory.Contains(curCommand.aName) == false)
        {
            if (curCommand.execExtraInfo.m_ExecButtonDown)
            {
                this.commandQueuesDictionary["HoldDown"].TryExecuteCommand(curCommand, true);
            }
        }
        else
        {
            if (curCommand.execExtraInfo.m_ExecButtonDown == false)
            {
                this.commandQueuesDictionary["HoldDown"].Remove(curCommand);
            }
        }
    }
    #endregion Execution Style A


}
/*
    public bool CheckSequence(string[] sequence, int maxDuration, CommandBase chainCommand)
    {
        int w = sequence.Length - 1;
        List<CommandBase> commandsFound = new List<CommandBase>();
        for (int i = 0; i < maxDuration; ++i)
        {
            int bufferSize = commandQueue_Basic._commandNamesHistory.queueList.Count;

            int bIndex = 0;
            if (bufferSize != 0)
            {
                //bIndex = Mathf.Clamp((CurrentTick - i + bufferSize) % bufferSize, 0, commandQueue_Basic._commandHistory.queueList.Count - 1);
                bIndex = (commandQueue_Basic._commandHistory.queueList.Count - 1 - i);// % bufferSize;
            }
            if (maxDuration > bufferSize)
            {
                break;
            }
            CurrentTick = bIndex;
            var curCmd = commandQueue_Basic._commandHistory.queueList[bIndex];
            if (curCmd.infoInstanceOnly.UsedToExecChainCommands.Contains(chainCommand.name)) break;//continue;
            if (curCmd.isExecuted == true)
            {
                // Linq Query // https://www.geeksforgeeks.org/linq-query-syntax/
                //var result = from s in stringList
                //             where s.Contains("Tutorials")
                //             select s;
                bool isAllowCheck = true;
                // Made up query syntax for Our Check Sequence
                if (sequence[w].Contains("?q="))
                {
                    // +ve and -ve axis
                    //if (sequence[w].Contains("+ve"))
                    //{
                    //    if (curCmd.axis <= 0)
                    //    {
                    //        isAllowCheck = false;
                    //    }
                    //}
                    //if (sequence[w].Contains("-ve"))
                    //{
                    //    if (curCmd.axis >= 0)
                    //    {
                    //        isAllowCheck = false;
                    //    }
                    //}
                }
                if (i == maxDuration - 1)
                {
                    if (Mathf.Abs(curCmd.TimeStamp - Time.time) > maxDefaultAllowedTimeDifferenceCheck)
                    {
                        return false;
                    }
                }

                //if (curCmd.readStyle != readStyles[w])
                //{
                //    isAllowCheck = false;
                //}
                if (isAllowCheck == true)
                {
                    string direction = curCmd.aName;
                    if (direction == sequence[w])
                    {
                        if (curCmd.TimeStamp > 0)
                        {
                            --w;
                            commandsFound.Add(curCmd);
                        }
                    }
                    if (w == -1)//-1)
                    {
                        //curCmd.timeFrame = -100; // iterated over commands, will have timeFrame 0 as they won't be used again
                        //commandsFound.ForEach(x => x.timeFrame = -100);
                        foreach (var x in commandsFound) { if (x.infoInstanceOnly.UsedToExecChainCommands.Contains(chainCommand.aName) == false) { x.infoInstanceOnly.UsedToExecChainCommands.Add(chainCommand.aName); } }
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public bool CheckSequence(string[] sequence, CommandBase chainCommand)
    {

        return CheckSequence(sequence, sequence.Length, chainCommand);
    }
 */
//[System.Serializable]
//public class comandQueue_IndividualData{
//    public int id = 0;
//    public CommandQueue commandQueue;
//}

//Ref: Modified From Zinac's Answer: https://gamedev.stackexchange.com/questions/43708/fighting-game-and-input-buffering
//public bool CheckSequence(List<string> sequence, List<ReadStyle> readStyles, int maxDuration)
//{
//    int w = sequence.Count - 1;
//    for (int i = 0; i < maxDuration; ++i)
//    {
//        int bIndex = 0;
//        int bufferSize = commandQueue_Basic._commandNamesHistory.queueList.Count;
//        if (bufferSize != 0)
//        {
//            bIndex = Mathf.Clamp((CurrentTick - i + bufferSize) % bufferSize, 0, commandQueue_Basic._commandHistory.queueList.Count);
//        }
//        if (maxDuration > bufferSize)
//        {
//            break;
//        }
//        var curCmd = commandQueue_Basic._commandHistory.queueList[bIndex];
//        if (curCmd.isExecuted == true)
//        {
//            // Linq Query // https://www.geeksforgeeks.org/linq-query-syntax/
//            //var result = from s in stringList
//            //             where s.Contains("Tutorials")
//            //             select s;
//            bool isAllowCheck = true;
//            // Made up query syntax for Our Check Sequence
//            if (sequence[w].Contains("?q="))
//            {
//                // +ve and -ve axis
//                //if (sequence[w].Contains("+ve"))
//                //{
//                //    if (curCmd.axis <= 0)
//                //    {
//                //        isAllowCheck = false;
//                //    }
//                //}
//                //if (sequence[w].Contains("-ve"))
//                //{
//                //    if (curCmd.axis >= 0)
//                //    {
//                //        isAllowCheck = false;
//                //    }
//                //}
//            }
//            //if (curCmd.readStyle != readStyles[w])
//            //{
//            //    isAllowCheck = false;
//            //}
//            if (isAllowCheck == true)
//            {
//                string direction = curCmd.aName;
//                if (direction == sequence[w])
//                    --w;
//                if (w == -1)//-1)
//                    return true;
//            }
//        }
//    }
//    return false;
//}