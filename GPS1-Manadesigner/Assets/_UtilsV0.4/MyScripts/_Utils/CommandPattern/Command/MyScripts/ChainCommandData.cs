using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChainCommandData
{
    public CommandSlotHolder chainCommandSlot;
    public string[] Sequence;
    [Header("Optional")]
    public string[] Sequence_Query; // +ve/-ve and readstyle
    public string[] holdDownSequence;

    public string[] ignoreCategory; // i.e Axis, Tertiary, Secondary // TODO: Implement
    // TODO : Below
    [Header("Negative Ecge has not been implemented yet. Not all game use it. Buts its a nice to have")]
    public string[] negativeEdgeSequence;
}


//Redundant
//public int[] Sequence_Axis; // negative or positive axis