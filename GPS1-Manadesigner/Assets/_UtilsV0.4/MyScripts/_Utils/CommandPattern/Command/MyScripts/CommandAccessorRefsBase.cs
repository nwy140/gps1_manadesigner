using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


// To Create your own Monobehavior Components for the Command Abilities Scriptable Object
// Make a class that derives from this script, override the methods you need and put your logic there

// Or you could create a class that derives from my scriptable object's Command class instead.
public class CommandAccessorRefsBase : MonoBehaviour
{
    [HideInInspector] public CommandSlotHolder commandSlotHolder;
    private void Awake()
    {
        if (commandSlotHolder == null && GetComponent<CommandSlotHolder>() != null)
        {
            commandSlotHolder = GetComponent<CommandSlotHolder>();
        }
    }
    #region  Execution Method Invoked from Scriptable Object UnityAction ~ See Input Command SO    [System.Serializable]
    [Header("Make a child class of this script and name it based on your desired command," +
        "\nOverride its methods and put your logic and refs there for your desired command" +
        "\nCall anything else via the Unity Events below\n"+
        "\nOr you could create a class that derives from \nmy scriptable object's Command class instead,and put your logic there instead.\n")]

    public CommandSlotUnityEvents commandSlotUnityEvents;
    [System.Serializable]
    public struct CommandSlotUnityEvents
    {
        public UnityEvent ExecPressUnityEvent;
        public UnityEvent ExecDownUnityEvent;
        public UnityEvent ExecDownChargingUnityEvent;
        public UnityEvent ExecReleaseUnityEvent;
        public UnityEvent ExecdUnityEvent;
    }
    public virtual void ExecPress()
    {
        commandSlotUnityEvents.ExecPressUnityEvent.Invoke();
    }
    public virtual void ExecDown()
    {
        commandSlotUnityEvents.ExecDownUnityEvent.Invoke();

    }

    public virtual void ExecDownCharging()
    {
        commandSlotUnityEvents.ExecDownChargingUnityEvent.Invoke();

    }

    public virtual void ExecRelease()
    {
        commandSlotUnityEvents.ExecReleaseUnityEvent.Invoke();

    }

    public virtual void Execd()
    {
        commandSlotUnityEvents.ExecdUnityEvent.Invoke();


    }
    #endregion  Execution Method Invoked from Scriptable Object UnityAction ~ See Input Command SO    [System.Serializable]

}


/* Unused
//private void OnEnable()
//{
//    commandSlotHolder.commandInstanceInSlot.instanceInfo._isEnabled = true;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecPressHolderEvent += ExecPress;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecDownHolderEvent += ExecDown;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecDownChargingHolderEvent += ExecDownCharging;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecReleaseHolderEvent += ExecRelease;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecdHolderEvent += Execd;
//}
//private void OnDisable()
//{
//    commandSlotHolder.commandInstanceInSlot.instanceInfo._isEnabled = false;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecPressHolderEvent -= ExecPress;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecDownHolderEvent -= ExecDown;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecDownChargingHolderEvent -= ExecDownCharging;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecReleaseHolderEvent -= ExecRelease;
//    commandSlotHolder.commandInstanceInSlot.execInfo.ExecdHolderEvent -= Execd;
//}

 */