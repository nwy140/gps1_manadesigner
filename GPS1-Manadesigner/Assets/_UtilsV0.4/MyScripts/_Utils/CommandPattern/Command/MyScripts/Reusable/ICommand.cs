﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICommand
{
    void TryExecCommand();
    void Undo();

    // Optional void Redo();
}

