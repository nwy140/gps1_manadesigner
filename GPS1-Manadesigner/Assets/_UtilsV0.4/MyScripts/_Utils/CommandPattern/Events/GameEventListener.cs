﻿
using UnityEngine;
using UnityEngine.Events;

// Ref: https://unity.com/how-to/architect-game-code-scriptable-objects#architect-events &&  https://www.youtube.com/watch?v=gXD2z_kkAXs&list=PLuLJclBWmeWVQYdl943Fw2hgX-PQHspNF&index=11 

public class GameEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public virtual void OnEventRaised()
    {
        Response.Invoke();
    }
}