﻿
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

// Ref:  https://www.youtube.com/watch?v=gXD2z_kkAXs&list=PLuLJclBWmeWVQYdl943Fw2hgX-PQHspNF&index=11 

public class CommandEventListener : GameEventListener
{
    public string commandName;

    [Header("Set In Start()")]
    public GameObject instigatorObj; // owner gameobject of this command
    public CommandBase matchingCommand;
    public CommandAccessorRefsBase accessorRefs;

    InputHandlerOld_NewInputSystem _inputHandlerOld;
    CommandSlotsHolderManager _commandSlotsHolderManager;
    public bool isCalled;
    private void Awake()
    {
        accessorRefs = GetComponentInParent<CommandAccessorRefsBase>();
        isCalled = false;
        //commandName = transform.parent.transform.parent.name;
    }


    GameObject comparedInstigatorObj; // temp 
    private void Start()
    {
        _inputHandlerOld = GetComponentInParent<InputHandlerOld_NewInputSystem>();
        if (_inputHandlerOld != null)
        {
            InitTypeA();
            comparedInstigatorObj = _inputHandlerOld.instigatorObj;
        }
        else
        {
            _commandSlotsHolderManager = GetComponentInParent<CommandSlotsHolderManager>();
            if (_commandSlotsHolderManager != null)
            {
                InitTypeB();
                comparedInstigatorObj = _commandSlotsHolderManager.commandOwnerRootObj;
            }
        }
        StartCoroutine(Start_Wait1Frame());
    }

    IEnumerator Start_Wait1Frame()
    {
        yield return null;
        if (instigatorObj == null)
        {
            // check if event instigator is same as command owner's instigator objref. 
            // For identifying who this command event belongs to
            instigatorObj = comparedInstigatorObj;
        }
    }

    public override void OnEventRaised()
    {
        if (((CommandEvent)Event).instigatorObj == matchingCommand.instanceInfo.commandOwnerRootObj)
        {
            base.OnEventRaised();
            isCalled = !isCalled;
        }
    }
    public void InitTypeB()
    {
        if (_commandSlotsHolderManager.commandSlotsNames.Contains(commandName))
        {
            int index = _commandSlotsHolderManager.commandSlotsNames.IndexOf(commandName);
            matchingCommand = _commandSlotsHolderManager.commandSlotsInChild[index].commandInstanceInSlot;
        }
        Init();
    }
    public void InitTypeA()
    {

        matchingCommand = _inputHandlerOld.inputCommands.Find(x => x.aName == commandName);
        //if(matchingCommand==null)
        //    matchingCommand = inputHandler.inputCommands_AxisOnly.Find(x => x.aName == commandName);
        Init();
    }

    public void Init()
    {
        if (Event == null)
        {
            if (gameObject.name == nameof(matchingCommand.execInfo.ExecPressEvent))
            {
                Event = matchingCommand.execInfo.ExecPressEvent;
            }
            if (gameObject.name == nameof(matchingCommand.execInfo.ExecDownEvent))
            {
                Event = matchingCommand.execInfo.ExecDownEvent;
            }
            if (gameObject.name == nameof(matchingCommand.execInfo.ExecDownChargingEvent))
            {
                Event = matchingCommand.execInfo.ExecDownChargingEvent;
            }
            if (gameObject.name == nameof(matchingCommand.execInfo.ExecReleaseEvent))
            {
                Event = matchingCommand.execInfo.ExecReleaseEvent;
            }
            if (gameObject.name == nameof(matchingCommand.execInfo.ExecdEvent))
            {
                Event = matchingCommand.execInfo.ExecdEvent;
            }
        }
    }
}