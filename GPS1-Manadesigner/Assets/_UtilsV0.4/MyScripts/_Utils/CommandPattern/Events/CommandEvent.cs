﻿

using System.Collections.Generic;
using UnityEngine;

// Ref: https://unity.com/how-to/architect-game-code-scriptable-objects#architect-events &&  https://www.youtube.com/watch?v=gXD2z_kkAXs&list=PLuLJclBWmeWVQYdl943Fw2hgX-PQHspNF&index=11 
[CreateAssetMenu]
public class CommandEvent : GameEvent
{
    [HideInInspector] public GameObject instigatorObj;
    public virtual void Raise(GameObject instigatorObjRef)
    {
        instigatorObj = instigatorObjRef;
        Raise();
    }
}