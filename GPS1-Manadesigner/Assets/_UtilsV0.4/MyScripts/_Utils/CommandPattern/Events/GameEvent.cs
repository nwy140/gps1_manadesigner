﻿

using System.Collections.Generic;
using UnityEngine;

// Ref: https://unity.com/how-to/architect-game-code-scriptable-objects#architect-events &&  https://www.youtube.com/watch?v=gXD2z_kkAXs&list=PLuLJclBWmeWVQYdl943Fw2hgX-PQHspNF&index=11 
//[CreateAssetMenu]
public class GameEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GameEventListener> eventListeners =
        new List<GameEventListener>();

    public virtual void Raise()
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised();
    }

    public virtual void RegisterListener(GameEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public virtual void UnregisterListener(GameEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}