using System;
using System.Text.RegularExpressions;
using UnityEngine.UI;

////TODO: have updateBindingUIEvent receive a control path string, too (in addition to the device layout name)

namespace UnityEngine.InputSystem.Samples.RebindUI
{
    /// <summary>
    /// This is an example for how to override the default display behavior of bindings. The component
    /// hooks into <see cref="RebindActionUI.updateBindingUIEvent"/> which is triggered when UI display
    /// of a binding should be refreshed. It then checks whether we have an icon for the current binding
    /// and if so, replaces the default text display with an icon.
    /// </summary>
    public class RebindIconsHelper : MonoBehaviour
    {
        public Sprite icon;

        public KeyMouseIcons keyboardMouse;
        public GamepadIcons xbox;
        public GamepadIcons ps4;
        protected void OnEnable()
        {
            // Hook into all updateBindingUIEvents on all RebindActionUI components in our hierarchy.
            var rebindUIComponents = transform.GetComponentsInChildren<RebindActionUI>();
            foreach (var component in rebindUIComponents)
            {
                component.updateBindingUIEvent.AddListener(OnUpdateBindingDisplay);
                component.UpdateBindingDisplay();
            }
        }
        protected void OnUpdateBindingDisplay(RebindActionUI component, string bindingDisplayString, string deviceLayoutName, string controlPath)
        {

            if (string.IsNullOrEmpty(deviceLayoutName) || string.IsNullOrEmpty(controlPath))
                return;
            //var tmp = Char.ToLowerInvariant(name[0]) + name.Substring(1);

            Debug.Log(deviceLayoutName + "_ " + controlPath);
            var icon = default(Sprite);

            if (InputSystem.IsFirstLayoutBasedOnSecond(deviceLayoutName, "DualShockGamepad"))
                icon = ps4.GetSprite(controlPath);
            else if (InputSystem.IsFirstLayoutBasedOnSecond(deviceLayoutName, "Gamepad"))
                icon = xbox.GetSprite(controlPath);
            if (InputSystem.IsFirstLayoutBasedOnSecond(deviceLayoutName, "Keyboard") || InputSystem.IsFirstLayoutBasedOnSecond(deviceLayoutName, "Mouse"))
                icon = keyboardMouse.GetSprite(controlPath);

            var textComponent = component.bindingText;

            // Grab Image component.
            var imageGO = textComponent.transform.parent.Find("ActionBindingIcon");
            var imageComponent = imageGO.GetComponent<Image>();

            if (icon != null)
            {
                textComponent.gameObject.SetActive(false);
                imageComponent.sprite = icon;
                imageComponent.gameObject.SetActive(true);
            }
            else
            {
                textComponent.gameObject.SetActive(true);
                imageComponent.gameObject.SetActive(false);
            }
        }

        [Serializable]
        public struct GamepadIcons
        {
            public Sprite buttonSouth;
            public Sprite buttonNorth;
            public Sprite buttonEast;
            public Sprite buttonWest;
            public Sprite startButton;
            public Sprite selectButton;
            public Sprite leftTrigger;
            public Sprite rightTrigger;
            public Sprite leftShoulder;
            public Sprite rightShoulder;
            public Sprite dpad;
            public Sprite dpadUp;
            public Sprite dpadDown;
            public Sprite dpadLeft;
            public Sprite dpadRight;
            public Sprite leftStick;
            public Sprite rightStick;
            public Sprite leftStickPress;
            public Sprite rightStickPress;

            public Sprite GetSprite(string controlPath)
            {
                // From the input system, we get the path of the control on device. So we can just
                // map from that to the sprites we have for gamepads.
                switch (controlPath)
                {
                    case "buttonSouth": return buttonSouth;
                    case "buttonNorth": return buttonNorth;
                    case "buttonEast": return buttonEast;
                    case "buttonWest": return buttonWest;
                    case "start": return startButton;
                    case "select": return selectButton;
                    case "leftTrigger": return leftTrigger;
                    case "rightTrigger": return rightTrigger;
                    case "leftShoulder": return leftShoulder;
                    case "rightShoulder": return rightShoulder;
                    case "dpad": return dpad;
                    case "dpad/up": return dpadUp;
                    case "dpad/down": return dpadDown;
                    case "dpad/left": return dpadLeft;
                    case "dpad/right": return dpadRight;
                    case "leftStick": return leftStick;
                    case "rightStick": return rightStick;
                    case "leftStickPress": return leftStickPress;
                    case "rightStickPress": return rightStickPress;
                }
                return null;
            }
        }
        [Serializable]
        public struct KeyMouseIcons
        {
            [Header("Mouse")]
            public Sprite None;
            public Sprite LeftMouse, RightMouse, MiddleMouse, ForwardMouse, BackMouse;
            public Sprite DeltaMouse;
            //public Sprite ScrollYUp;
            //public Sprite ScrollYDown;
            //public Sprite ScrollY;

            // Ref: https://github.com/needle-mirror/com.unity.inputsystem/blob/c249a5aed5d0ffceb662733a672af934b86ebc16/InputSystem/Devices/Keyboard.cs
            [Header("Keyboard")]
            public Sprite Space;
            public Sprite Enter;
            public Sprite Tab;
            public Sprite Backquote;
            public Sprite Quote;
            public Sprite Semicolon;
            public Sprite Comma;
            public Sprite Period;
            public Sprite Slash;
            public Sprite Backslash;
            public Sprite LeftBracket;
            public Sprite RightBracket;
            public Sprite Minus;
            public Sprite Equals;
            public Sprite A;
            public Sprite B;
            public Sprite C;
            public Sprite D;
            public Sprite E;
            public Sprite F;
            public Sprite G;
            public Sprite H;
            public Sprite I;
            public Sprite J;
            public Sprite K;
            public Sprite L;
            public Sprite M;
            public Sprite N;
            public Sprite O;
            public Sprite P;
            public Sprite Q;
            public Sprite R;
            public Sprite S;
            public Sprite T;
            public Sprite U;
            public Sprite V;
            public Sprite W;
            public Sprite X;
            public Sprite Y;
            public Sprite Z;
            public Sprite Digit1;
            public Sprite Digit2;
            public Sprite Digit3;
            public Sprite Digit4;
            public Sprite Digit5;
            public Sprite Digit6;
            public Sprite Digit7;
            public Sprite Digit8;
            public Sprite Digit9;
            public Sprite Digit0;
            public Sprite LeftShift;
            public Sprite RightShift;
            public Sprite LeftAlt;
            public Sprite RightAlt;
            public Sprite AltGr; //= RightAlt;
            public Sprite LeftCtrl;
            public Sprite RightCtrl;
            public Sprite LeftMeta;
            public Sprite RightMeta;
            public Sprite LeftWindows;//= LeftMeta;
            public Sprite RightWindows;// = RightMeta;
            public Sprite LeftApple;// = LeftMeta;
            public Sprite RightApple;// = RightMeta;
            public Sprite LeftCommand;// = LeftMeta;
            public Sprite RightCommand;// = RightMeta;
            public Sprite ContextMenu;
            public Sprite Escape;
            public Sprite LeftArrow;
            public Sprite RightArrow;
            public Sprite UpArrow;
            public Sprite DownArrow;
            public Sprite Backspace;
            public Sprite PageDown;
            public Sprite PageUp;
            public Sprite Home;
            public Sprite End;
            public Sprite Insert;
            public Sprite Delete;
            public Sprite CapsLock;
            public Sprite NumLock;
            public Sprite PrintScreen;
            public Sprite ScrollLock;
            public Sprite Pause;
            public Sprite NumpadEnter;
            public Sprite NumpadDivide;
            public Sprite NumpadMultiply;
            public Sprite NumpadPlus;
            public Sprite NumpadMinus;
            public Sprite NumpadPeriod;
            public Sprite NumpadEquals;
            public Sprite Numpad0;
            public Sprite Numpad1;
            public Sprite Numpad2;
            public Sprite Numpad3;
            public Sprite Numpad4;
            public Sprite Numpad5;
            public Sprite Numpad6;
            public Sprite Numpad7;
            public Sprite Numpad8;
            public Sprite Numpad9;
            public Sprite F1;
            public Sprite F2;
            public Sprite F3;
            public Sprite F4;
            public Sprite F5;
            public Sprite F6;
            public Sprite F7;
            public Sprite F8;
            public Sprite F9;
            public Sprite F10;
            public Sprite F11;
            public Sprite F12;
            public Sprite OEM1;
            public Sprite OEM2;
            public Sprite OEM3;
            public Sprite OEM4;
            public Sprite OEM5;
            public Sprite IMESelected;
            string CamelCase(string s)
            {
                var x = s.Replace("_", "");
                if (x.Length == 0) return "null";
                x = Regex.Replace(x, "([A-Z])([A-Z]+)($|[A-Z])",
                    m => m.Groups[1].Value + m.Groups[2].Value.ToLower() + m.Groups[3].Value);
                return char.ToLower(x[0]) + x.Substring(1);
            }
            string PascalCase(string s)
            {
                var x = CamelCase(s);
                return char.ToUpper(x[0]) + x.Substring(1);
            }
            public Sprite GetSprite(string controlPath)
            {
                controlPath = PascalCase(controlPath);
                Debug.Log(controlPath);


                // From the input system, we get the path of the control on device. So we can just
                // map from that to the sprites we have for gamepads.
                switch (controlPath)
                {

                    //Mouse 
                    //Left, Right, Middle, Forward, Back;

                    case "LeftButton": return LeftMouse;
                    case "RightButton": return RightMouse;
                    case "MiddleButton": return MiddleMouse;
                    case "ForwardButton": return ForwardMouse;
                    case "BackButton": return BackMouse;
                    case "Delta": return DeltaMouse;
                    case "ScrollY": return MiddleMouse;
                    // Keyboard
                    case "": return None;
                    case "None ": return None;
                    case "Space ": return Space;
                    case "Enter": return Enter;
                    case "Tab": return Tab;
                    case "Backquote ": return Backquote;
                    case "Quote ": return Quote;
                    case "Semicolon": return Semicolon;
                    case "Comma": return Comma;
                    case "Period": return Period;
                    case "Slash": return Slash;
                    case "Backslash": return Backslash;
                    case "LeftBracket": return LeftBracket;
                    case " :  rightBracket": return RightBracket;
                    case "Minus": return Minus;
                    case "Equals": return Equals;
                    case "A": return A;
                    case "B": return B;
                    case "C": return C;
                    case "D": return D;
                    case "e ": return E;
                    case "F": return F;
                    case "G": return G;
                    case "H": return H;
                    case "I": return I;
                    case "J": return J;
                    case "K": return K;
                    case "L": return L;
                    case "M": return M;
                    case "N": return N;
                    case "O": return O;
                    case "P": return P;
                    case "Q": return Q;
                    case " :  r": return R;
                    case "S": return S;
                    case "T": return T;
                    case "U": return U;
                    case "V": return V;
                    case "W": return W;
                    case "X": return X;
                    case "Y": return Y;
                    case "Z": return Z;
                    case "1": return Digit1;
                    case "2": return Digit2;
                    case "3": return Digit3;
                    case "4": return Digit4;
                    case "5": return Digit5;
                    case "6": return Digit6;
                    case "7": return Digit7;
                    case "8": return Digit8;
                    case "9": return Digit9;
                    case "0": return Digit0;
                    case "LeftShift": return LeftShift;
                    case " :  rightShift": return RightShift;
                    case "LeftAlt": return LeftAlt;
                    case " :  rightAlt": return RightAlt;
                    case "AltGr=RightAlt": return AltGr = RightAlt;
                    case "LeftCtrl": return LeftCtrl;
                    case " :  rightCtrl": return RightCtrl;
                    case "LeftMeta": return LeftMeta;
                    case " :  rightMeta": return RightMeta;
                    case "LeftWindows=LeftMeta": return LeftWindows = LeftMeta;
                    case " :  rightWindows=RightMeta": return RightWindows = RightMeta;
                    case "LeftApple=LeftMeta": return LeftApple = LeftMeta;
                    case " :  rightApple=RightMeta": return RightApple = RightMeta;
                    case "LeftCommand=LeftMeta": return LeftCommand = LeftMeta;
                    case " :  rightCommand=RightMeta": return RightCommand = RightMeta;
                    case "ContextMenu": return ContextMenu;
                    case "Escape ": return Escape;
                    case "LeftArrow": return LeftArrow;
                    case " :  rightArrow": return RightArrow;
                    case "UpArrow": return UpArrow;
                    case "DownArrow": return DownArrow;
                    case "Backspace ": return Backspace;
                    case "PageDown": return PageDown;
                    case "PageUp": return PageUp;
                    case "Home ": return Home;
                    case "End": return End;
                    case "Insert": return Insert;
                    case "Delete ": return Delete;
                    case "CapsLock": return CapsLock;
                    case "NumLock": return NumLock;
                    case "PrintScreen": return PrintScreen;
                    case "ScrollLock": return ScrollLock;
                    case "Pause ": return Pause;
                    case "NumpadEnter": return NumpadEnter;
                    case "NumpadDivide ": return NumpadDivide;
                    case "NumpadMultiply": return NumpadMultiply;
                    case "NumpadPlus": return NumpadPlus;
                    case "NumpadMinus": return NumpadMinus;
                    case "NumpadPeriod": return NumpadPeriod;
                    case "NumpadEquals": return NumpadEquals;
                    case "Numpad0": return Numpad0;
                    case "Numpad1": return Numpad1;
                    case "Numpad2": return Numpad2;
                    case "Numpad3": return Numpad3;
                    case "Numpad4": return Numpad4;
                    case "Numpad5": return Numpad5;
                    case "Numpad6": return Numpad6;
                    case "Numpad7": return Numpad7;
                    case "Numpad8": return Numpad8;
                    case "Numpad9": return Numpad9;
                    case "F1": return F1;
                    case "F2": return F2;
                    case "F3": return F3;
                    case "F4": return F4;
                    case "F5": return F5;
                    case "F6": return F6;
                    case "F7": return F7;
                    case "F8": return F8;
                    case "F9": return F9;
                    case "F10": return F10;
                    case "F11": return F11;
                    case "F12": return F12;
                    case "OEM1": return OEM1;
                    case "OEM2": return OEM2;
                    case "OEM3": return OEM3;
                    case "OEM4": return OEM4;
                    case "OEM5": return OEM5;
                    case "IMESelected": return IMESelected;
                    default: return null;
                }
                return null;
            }
        }
    }
}