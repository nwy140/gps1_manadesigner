using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


// Version of Unity Input system must be com.unity.inputsystem@1.1 for this script to work
public class RebindSaveLoad : MonoBehaviour
{
    [SerializeField]
    InputActionAsset inputActions;

    private void ReloadBindings()
    {
        var rebinds = PlayerPrefs.GetString("rebinds");
        if (string.IsNullOrEmpty(rebinds) == false)
        {
            inputActions.LoadBindingOverridesFromJson(rebinds);
        } 
    }
    private void ReloadBindings_PlayerInput()
    {
        var rebinds = PlayerPrefs.GetString("rebinds");
        if (string.IsNullOrEmpty(rebinds) == false)
        {
            inputActions.LoadBindingOverridesFromJson(rebinds);
        }
    }
    public PlayerInput pInput;

    private void Awake()
    {
        ReloadBindings();
        if (pInput == null)
        {
            var targetObj = GameObject.FindGameObjectWithTag("Player");
            if (targetObj != null)
            {
                pInput = targetObj.GetComponent<PlayerInput>();
            }
        }
        ReloadBindings_PlayerInput();
    }
    private void OnEnable()
    {
        ReloadBindings();
        ReloadBindings_PlayerInput();
    }

    private void OnDisable()
    {
        var rebinds = inputActions.SaveBindingOverridesAsJson();
        PlayerPrefs.SetString("rebinds", rebinds);
        ReloadBindings();
        ReloadBindings_PlayerInput();
    }
}
