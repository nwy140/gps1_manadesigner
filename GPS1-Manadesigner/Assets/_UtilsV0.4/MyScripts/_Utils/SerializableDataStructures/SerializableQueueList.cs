// Custom MadeInspector Serializable Queue made Created by myself, Implemented using List // Unity does not serialize C#'s built in Queue Class in the inspector
using System.Collections.Generic;
[System.Serializable]
public class SerializableQueueList<T>
{
    public List<T> queueList = new List<T>();

    public void Enqueue(T t)
    {
        Add(t);
    }

    public T Dequeue()
    {
        T temp = queueList[0];
        RemoveAt(0);

        return temp;
    }
    public void OnQueueModified()
    {

    }

    public void Clear()
    {
        queueList.Clear();
    }
    #region Custom methods
    public T Peek()
    {
        return queueList[0];
    }
    public virtual int Count { get => queueList.Count; }// set => _id = value; }
    public T[] ToArray()
    {
        return queueList.ToArray();
    }
    public void Add(T t)
    {
        queueList.Add(t);
        OnQueueModified();
    }
    public bool Remove(T t)
    {
        bool result = queueList.Remove(t); ;
        OnQueueModified();
        return result;
    }
    public void RemoveAt(int i)
    {
        queueList.RemoveAt(i);
        OnQueueModified();
    }
    public bool Contains(T t)
    {
        return queueList.Contains(t);
    }

    //public  int Count { get => queueList.Count; }//set => _timeFrame = value; }

    #endregion Custom methods
}
