using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickPauseFix : MonoBehaviour
{
    PauseMenuScripts pauseMenuScript;
    private void Awake()
    {
        pauseMenuScript = FindObjectOfType<PauseMenuScripts>();
    }
    private void OnEnable()
    {
        if (pauseMenuScript == null)
        {
            pauseMenuScript = FindObjectOfType<PauseMenuScripts>();
        }
        else
        {
            pauseMenuScript.enabled = false;
        }
    }
    private void OnDisable()
    {
        if (pauseMenuScript == null)
        {
            pauseMenuScript = FindObjectOfType<PauseMenuScripts>();
        }
        else
        {
            pauseMenuScript.enabled = true;
            pauseMenuScript.isPaused = false;
        }
    }
}
