#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class PolyColToMesh : MonoBehaviour
{
#if UNITY_EDITOR
    PolygonCollider2D col;
    private void Awake()
    {
        TryGetComponent(out col);
        Mesh mesh = col.CreateMesh(true, true);
        //Mesh mesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);
        GameObject go = new GameObject();// Instantiate(new GameObject(),transform);
        go.AddComponent<MeshFilter>();
        go.AddComponent<MeshRenderer>();
        go.GetComponent<MeshFilter>().mesh = mesh;
        //AssetDatabase.CreateAsset( [mesh object here], [path to asset] );
        AssetDatabase.CreateAsset(mesh, Application.dataPath + "a");
        AssetDatabase.SaveAssets();
    }
#endif
}
