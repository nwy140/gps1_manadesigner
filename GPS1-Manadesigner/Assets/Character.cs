using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public storage Storage;

    [Header("Item Tool Tip")]
    [SerializeField] ItemToolTip itemToolTip;
    [SerializeField] Image draggableItem;
    [SerializeField] ItemSaveManager itemSaveManager;

    [SerializeField] itemSlots[] totalItemSlots;
    [SerializeField] Image[] totalSlotBorders;
    [SerializeField] Sprite unselectedSprite;
    [SerializeField] Sprite selectSprite;

    [Header("Position")]
    [SerializeField] Vector3 iniPos;
    [SerializeField] Vector3 lastPos;

    private itemSlots draggedSlot;

    public itemSlots itemToDelete;

    private void Awake()
    {
        Storage.OnLeftClickEvent += AddToDiscard;
        Storage.OnPointerEnterEvent += ShowToolTip;
        Storage.OnPointerExitEvent += HideToolTip;
        //Storage.OnBeginDragEvent += BeginDrag;
        //Storage.OnEndDragEvent += EndDrag;
        //Storage.OnDragEvent += Drag;
        //Storage.OnDropEvent += Drop;

        if (itemSaveManager == null)
        {
            itemSaveManager = FindObjectOfType<ItemSaveManager>();
        }

        for (int i = 0; i < totalItemSlots.Length; i++)
        {
            //Debug.Log($"Total ItemSlots Item = {totalItemSlots[i].Item.name}, Item To Delete = {itemToDelete.Item.name}");
            totalItemSlots[i].slotBorder = totalSlotBorders[i];
        }

        itemSaveManager.LoadInventory(this);

        //for (int i = 0; i < Storage.inventoryUpgrade.GetNumberOfSlots(); i++)
        //{
        //    if (Storage.itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<itemSlots>().Item != null)
        //    {
        //        Storage.itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<itemSlots>().Item.maxStack = Storage.inventoryUpgrade.GetStackAmount();
        //        Debug.Log($"Max Stack {Storage.itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<itemSlots>().Item.maxStack}");
        //    }
        //}

        this.gameObject.SetActive(false);
    }

    private void Start()
    {
        //for (int i = 0; i < totalItemSlots.Length; i++)
        //{
        //    //Debug.Log($"Total ItemSlots Item = {totalItemSlots[i].Item.name}, Item To Delete = {itemToDelete.Item.name}");
        //    totalItemSlots[i].slotBorder = totalSlotBorders[i];
        //}
    }

    private void OnValidate()
    {
        if (itemToolTip == null)
        {
            itemToolTip = FindObjectOfType<ItemToolTip>();
        }
    }

    public void Open()
    {
        GetComponent<RectTransform>().transform.localPosition = iniPos;
    }

    public void Close()
    {
        GetComponent<RectTransform>().transform.localPosition = lastPos;
    }

    private void AddToDiscard(itemSlots itemSlot)
    {
        itemToDelete = itemSlot;

        foreach (Image image in totalSlotBorders)
        {
            image.sprite = unselectedSprite;
        }

        if (itemToDelete.Item != null)
        {
            itemToDelete.slotBorder.sprite = selectSprite;
            Debug.Log("Item to Delete: " + itemToDelete);
        }
        else
        {
            Debug.Log("Item not found");
        }
    }

    public void BeginDiscard()
    {
        foreach(Image image in totalSlotBorders)
        {
            image.sprite = unselectedSprite;
        }

        if (itemToDelete != null)
        {
            Storage.discardItem(itemToDelete);
        }
        else
        {
            Debug.Log("Nothing to Delete");
        }
    }

    #region Tool Tip

    private void ShowToolTip(itemSlots itemSlot)
    {
        item resourceItem = itemSlot.Item as item;
        if (resourceItem != null)
        {
            itemToolTip.ShowToolTip(resourceItem);
        }
    }

    private void HideToolTip(itemSlots itemSlot)
    {
        itemToolTip.HideToolTip();
    }

    //public bool IsCharacterPanelOpen()
    //{
    //    if (this.gameObject.activeInHierarchy)
    //    {
    //        return true;
    //    }
    //    else;
    //    {
    //        return false;
    //    }
    //}

    //private void BeginDrag(itemSlots itemSlot)
    //{
    //    if (itemSlot.Item != null)
    //    {
    //        draggedSlot = itemSlot;
    //        draggableItem.sprite = itemSlot.Item.Icon;
    //        draggableItem.transform.position = Input.mousePosition;
    //        draggableItem.enabled = true;
    //    }
    //}

    //private void EndDrag(itemSlots itemSlot)
    //{
    //    draggedSlot = null;
    //    draggableItem.enabled = false;
    //}

    //private void Drag(itemSlots itemSlot)
    //{
    //    if(draggableItem.enabled)
    //    {
    //        draggableItem.transform.position = Input.mousePosition;
    //    }
    //}

    //private void Drop(itemSlots dropItemSlot)
    //{
    //    if(dropItemSlot.CanReceiveItem(draggedSlot.Item) && draggedSlot.CanReceiveItem(dropItemSlot.Item))
    //    {
    //        item draggedItem = draggedSlot.Item;
    //        draggedItem = dropItemSlot.Item;
    //        dropItemSlot.Item = draggedItem;
    //    }
    //}

    #endregion
}
