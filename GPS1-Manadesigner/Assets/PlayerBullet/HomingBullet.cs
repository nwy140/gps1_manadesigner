using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingBullet : PlayerBullet
{
    public EnemyHealth[] enemies;
    public EnemyHealth targetedEnemy;
    Rigidbody2D bulletRB2;

    new void Start()
    {
        base.Start();
        //transform.rotation = new Quaternion(0, 0, transform.rotation.z - 90, 0);
        enemies = FindObjectsOfType<EnemyHealth>();
        bulletRB2 = GetComponent<Rigidbody2D>();
        //Debug.Log($"Enemies Count = {enemies.Length}");
    }

    void Update()
    {
        //if (targetedEnemy != null)
        //{
        //    HeatSeekEnemy();
        //}

        Vector2 dir = bulletRB2.velocity.normalized;

        if (dir != Vector2.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(Vector3.forward, dir);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, 1000 * Time.fixedDeltaTime);
        }

        foreach (EnemyHealth enemy in enemies)
        {
            if(enemy == null) { return; }
            if (Vector2.Distance(transform.position, enemy.transform.position) < 5)
            {
                targetedEnemy = enemy;
            }
        }

        //Vector2 dir = bulletRB2.velocity.normalized;

        //if (dir != Vector2.zero)
        //{
        //    Quaternion toRotation = Quaternion.LookRotation(Vector3.forward, dir);
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, 1000 * Time.fixedDeltaTime);
        //}
    }

    private void FixedUpdate()
    {
        if (targetedEnemy != null)
        {
            HeatSeekEnemy();
            Debug.Log($"Heat Seeking");
        }

        //foreach (EnemyHealth enemy in enemies)
        //{
        //    if (Vector2.Distance(transform.position, enemy.transform.position) < 10)
        //    {
        //        targetedEnemy = enemy;
        //    }
        //}
    }

    public void HeatSeekEnemy()
    {
        //Method 1

        //Vector2 direction = (Vector2)targetedEnemy.transform.position - (Vector2)bulletRB2.position;

        //direction.Normalize();

        //float rotateAmount = Vector3.Cross(direction, bulletRB2.velocity.normalized).z;

        //bulletRB2.angularVelocity = -rotateAmount * 500000f;


        //Method 2

        Vector2 dir = -(transform.position - targetedEnemy.transform.position).normalized;

        bulletRB2.velocity = bulletRB2.velocity.normalized * BulletSpeed;

        //bulletRB2.velocity += dir * 10;

        bulletRB2.velocity = new Vector2(dir.x * BulletSpeed, dir.y * BulletSpeed);

        //if (bulletRB2.velocity.magnitude > BulletSpeed)
        //{
        //    float excess = bulletRB2.velocity.magnitude - BulletSpeed;
        //    bulletRB2.velocity -= dir * excess / 2;
        //    bulletRB2.velocity += dir * 10;
        //}



    }
}
