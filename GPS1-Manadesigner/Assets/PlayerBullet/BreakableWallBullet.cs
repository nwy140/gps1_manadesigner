using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableWallBullet : PlayerBullet
{
    [SerializeField] AreaOfEffect areaOfEffect;

    private void OnDestroy()
    {
        Instantiate(areaOfEffect, transform.position, Quaternion.identity);
    }
}
