using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerBulletProperties
{
    public float bulletDamage;
    public float bulletSpeed;
}

public class PlayerBullet : MonoBehaviour
{
    public PlayerBulletProperties PlayerBulletProperties;
    Rigidbody2D bulletRigidbody2D;

    protected void Start()
    {
        bulletRigidbody2D = GetComponent<Rigidbody2D>();

        Destroy(this.gameObject, 1f);
    }

    private void Update()
    {
        Vector2 dir = bulletRigidbody2D.velocity.normalized;

        if (dir != Vector2.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(Vector3.forward, dir);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, 10000 * Time.fixedDeltaTime);
        }
    }

    public float BulletDamage
    {
        get
        {
            return PlayerBulletProperties.bulletDamage;
        }
        set
        {
            PlayerBulletProperties.bulletDamage = value;
        }
    }

    public float BulletSpeed
    {
        get
        {
            return PlayerBulletProperties.bulletSpeed;
        }
        set
        {
            PlayerBulletProperties.bulletSpeed = value;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnHitBox(collision.gameObject);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnHitBox(collision.gameObject);
    }
    public void OnHitBox(GameObject other)
    {
        if(other.GetComponent<EnemyHealth>())
        {
            EnemyHealth enemy = other.GetComponent<EnemyHealth>();
            AudioManager.instance.PlaySFX("fishHurt");
            enemy.GetDamage(BulletDamage);
            Destroy(this.gameObject);
        }
    }
}
