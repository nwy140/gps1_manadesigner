using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAttack : Player
{
    UpgradeHolder upgradeHolder;

    [Header("Upgrades")]
    [SerializeField] Weapon currentWeaponUpgrade;

    [Header("Attack")]
    [SerializeField] PlayerBullet playerBulletPrefab;
    //Rigidbody2D playerRigidbody2D;
    float attackTime;
    float xDirection;
    float yDirection;
    Vector2 direction;

    public Transform spawnSocket;
    public float spawnBaseForceMultiplier = 100f;

    [Header("Improvements")]
    public Transform turret;
    public float turretLerpRotSpeed = 5f;
    public float turretRotZOffset = 90;

    private void Start()
    {
        upgradeHolder = GetComponent<UpgradeHolder>();
        currentWeaponUpgrade = upgradeHolder.WeaponUpgrade;
        playerBulletPrefab = currentWeaponUpgrade.GetPlayerBullet();
    }

    private void FixedUpdate()
    {
        if (Convert.ToBoolean(PlayerPrefs.GetInt("ShootAimbyMovement")) == false)
        { 
            RotateTurretToMousePos();
        }

        AttackBehaviour3();
    }
    public void RotateTurretToMousePos()
    {
        if (turret != null)
        {
                //Ref: https://forum.unity.com/threads/using-right-analogue-stick-to-control-character-facing-direction.358084/
                Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Debug.Log(targetPos);
                //Vector3 targetPos = turret.position + moveHorizontalAxis * 100 * Vector3.right + moveVerticalAxis * 100 * Vector3.up;
                 
                Debug.DrawLine(turret.position, targetPos + Vector3.one * 10);
                //Ref: https://answers.unity.com/questions/585035/lookat-2d-equivalent-.html?page=2&pageSize=5&sort=votes
                Vector3 diff = targetPos - turret.position;
                diff.Normalize();
                float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                Quaternion targetRot = Quaternion.Euler(0f, 0f, rot_z - 90);
                targetRot.eulerAngles += Vector3.forward * turretRotZOffset;
                turret.rotation = Quaternion.Slerp(turret.rotation, targetRot, Time.deltaTime * turretLerpRotSpeed);
        }
    }
    PlayerBullet CreateBullet(float bulletDamage, float bulletSpeed)
    {
        PlayerBullet playerBullet = null;
        if (spawnSocket != null)
        {
            playerBullet = Instantiate(playerBulletPrefab, spawnSocket.position, spawnSocket.rotation);
        }
        else
        {
            playerBullet = Instantiate(playerBulletPrefab, transform.position, Quaternion.identity);
        }

        playerBullet.BulletDamage = bulletDamage;
        playerBullet.BulletSpeed = bulletSpeed;
        return playerBullet;
    }
    private void AttackBehaviour()
    {
        if (attackTime <= 0)
        {

            xDirection = 0;
            yDirection = 0;

            if (Input.GetKey(KeyCode.RightArrow))
            {
                xDirection = 1;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                xDirection = -1;
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                yDirection = 1;
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                yDirection = -1;
            }


            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
            {
                PlayerBullet playerBullet = CreateBullet(currentWeaponUpgrade.GetAttackDamage(), currentWeaponUpgrade.GetAttackSpeed());
                Rigidbody2D playerBulletRigidbody2D = playerBullet.GetComponent<Rigidbody2D>();
                playerBulletRigidbody2D.velocity = new Vector2(xDirection, yDirection).normalized * currentWeaponUpgrade.GetAttackSpeed();
                attackTime = currentWeaponUpgrade.GetAttackRate();

                if (Input.GetKey(KeyCode.RightArrow))
                {
                    playerBullet.transform.rotation = Quaternion.Slerp(playerBullet.transform.rotation, new Quaternion(playerBullet.transform.rotation.x, playerBullet.transform.rotation.y, 0, playerBullet.transform.rotation.w), 1);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    playerBullet.transform.rotation = Quaternion.Slerp(playerBullet.transform.rotation, new Quaternion(playerBullet.transform.rotation.x, playerBullet.transform.rotation.y, 180, playerBullet.transform.rotation.w), 1);
                }
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    playerBullet.transform.rotation = Quaternion.Slerp(playerBullet.transform.rotation, new Quaternion(playerBullet.transform.rotation.x, playerBullet.transform.rotation.y, 1, playerBullet.transform.rotation.w), 1);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    playerBullet.transform.rotation = Quaternion.Slerp(playerBullet.transform.rotation, new Quaternion(playerBullet.transform.rotation.x, playerBullet.transform.rotation.y, -1, playerBullet.transform.rotation.w), 1);
                }
            }
        }
        else
        {
            attackTime -= Time.deltaTime;
        }
    }

    private void AttackBehaviour2()
    {
        if (direction == Vector2.zero)
        {
            direction = new Vector2(1, 0);
        }

        if (playerRigidbody2D.velocity != Vector2.zero)
        {
            direction = playerRigidbody2D.velocity.normalized;
        }

        if (attackTime <= 0)
        {
            //SimulateShoot();
        }
        else
        {
            attackTime -= Time.deltaTime;
        }
    }
    public void AttackBehaviour3()
    {

        if (attackTime <= 0)
        {
            //SimulateShoot();
        }
        else
        {
            attackTime -= Time.deltaTime;
        }
    }
    public void SimulateShoot()
    {
        PlayerBullet playerBullet = CreateBullet(currentWeaponUpgrade.GetAttackDamage(), currentWeaponUpgrade.GetAttackSpeed());
        Rigidbody2D playerBulletRigidbody2D = playerBullet.GetComponent<Rigidbody2D>();

        playerBulletRigidbody2D.AddRelativeForce(Vector2.right * spawnBaseForceMultiplier * currentWeaponUpgrade.GetAttackSpeed() );
        //playerBulletRigidbody2D.velocity = playerBulletRigidbody2D.transform.forward * currentWeaponUpgrade.GetAttackSpeed() * 1000;

        //Quaternion toRotation = Quaternion.LookRotation(Vector3.forward, direction);
        //playerBullet.transform.rotation = Quaternion.RotateTowards(playerBullet.transform.rotation, toRotation, 10000 * Time.deltaTime);

        attackTime = currentWeaponUpgrade.GetAttackRate();
    }
 
    public void OnInput(InputAction.CallbackContext context)
    {
        if (context.action.WasPressedThisFrame() && Time.timeScale>0)
        {
            if (attackTime <= 0)
            {
                SimulateShoot();
            }
        }
        //if (isActiveAndEnabled)
        //    _commandQueueHelper.ExecCommand_InputSystem(context, commandInstanceInSlot);
    }

    // Simulate Input can be Called by Both Ai or Player
    public void OnSimulateInput(bool IsPressedThisFrame, float axis)
    {
        //if (isActiveAndEnabled)
        //    _commandQueueHelper.ExecCommand_SimulateInputSystem(IsPressedThisFrame, axis, commandInstanceInSlot);
        //_commandHelper.ExecCommand_SimulateInputSystem(true, 1, commandInstanceInSlot);
    }
}
