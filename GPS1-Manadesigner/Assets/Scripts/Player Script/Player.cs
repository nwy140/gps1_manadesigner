using UnityEngine;

public abstract class Player : MonoBehaviour
{
    [SerializeField] protected SpriteRenderer sprite;
    protected Rigidbody2D playerRigidbody2D;
    protected bool isDead = false;

    void Awake()
    {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
    }
}