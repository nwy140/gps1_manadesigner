using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerHealth : Player
{
    UpgradeHolder upgradeHolder;

    [Header("Player")]
    PlayerMovement playerMovement;

    [Header("Upgrades")]
    [SerializeField] Hull currentHullUpgrade;
    //[SerializeField] Turbine currentTurbineUpgrade;
    [SerializeField] SelfRepair currentSelfRepairUpgrade;
    [SerializeField] OxygenTank currentOxygenTankUpgrade;

    [Space]

    [Header("Health")]
    [SerializeField] float healthDepletionAmount = 0.01f;
    [SerializeField] float healthDepletionRate = 3.0f;
    [SerializeField] GameObject loseScreen;
    float healthDepletionTime = 0f;
    [SerializeField] Slider healthBar;
    [SerializeField] GameObject healthChargeGroup;
    [SerializeField] GameObject healthCharge;
    public float health;
    int availableHealthKits;

    [Space]

    [Header("Oxygen")]
    [SerializeField] float oxygenDepletionAmount = 1f;
    [SerializeField] float oxygenDepletionRate = 3f;
    [SerializeField] Slider oxygenSupplyBar;
    PressureStageChecker pressureStageChecker;
    float pressureStrength;
    float selfRepairTime;
    float oxygenSupply;
    float oxygenDepletionTime;
    private bool isOxygenTankEmpty;
    bool playOnce = false;

    private void Awake()
    {

    }

    private void Start()
    {
        playerMovement = gameObject.GetComponent<PlayerMovement>();

        if (DayCounter.instance != null)
        {
            if (DayCounter.instance.days > 14)
            {

                if (loseScreen != null)
                {
                    loseScreen.SetActive(true);
                }
            }
        }
        upgradeHolder = GetComponent<UpgradeHolder>();
        pressureStageChecker = FindObjectOfType<PressureStageChecker>();

        currentHullUpgrade = upgradeHolder.HullUpgrade;
        //currentTurbineUpgrade = upgradeHolder.TurbineUpgrade;
        currentSelfRepairUpgrade = upgradeHolder.SelfRepairUpgrade;
        currentOxygenTankUpgrade = upgradeHolder.OxygenTankUpgrade;

        health = currentHullUpgrade.GetHealth();
        pressureStrength = currentHullUpgrade.GetPressureStrength();
        availableHealthKits = currentSelfRepairUpgrade.GetAvailableHealthKits();
        //availableHealthKits = PlayerPrefs.GetInt("healthKits");
        oxygenSupply = currentOxygenTankUpgrade.GetOxygenSupplyAmount();

        if(healthChargeGroup.transform.childCount < availableHealthKits)
        {
            for(int i = healthChargeGroup.transform.childCount; i < availableHealthKits; i++)
            {
                GameObject charge = Instantiate(healthCharge, transform.position, Quaternion.identity);
                charge.transform.SetParent(healthChargeGroup.transform);
                charge.transform.localScale = Vector3.one;
            }
        }
        else
        {
            for (int i = healthChargeGroup.transform.childCount- 1; i >= availableHealthKits; i--)
            {
                Destroy(healthChargeGroup.transform.GetChild(i).gameObject);
            }
        }

        if (PlayerPrefs.GetInt("healthKits") > -1)
        {
            availableHealthKits = PlayerPrefs.GetInt("healthKits");

            for (int i = 0; i < healthChargeGroup.transform.childCount; i++)
            {
                healthChargeGroup.transform.GetChild(i).transform.GetChild(0).gameObject.SetActive(false);
            }

            for (int i = 0; i < availableHealthKits; i++)
            {
                healthChargeGroup.transform.GetChild(i).transform.GetChild(0).gameObject.SetActive(true);
            }
        }

        if ((PlayerPrefs.GetFloat("health") > 0))
        {
            health = PlayerPrefs.GetFloat("health");
        }

        //health = currentHullUpgrade.GetHealth() * 1 / 4;
        //health = currentHullUpgrade.GetHealth();

        if (currentOxygenTankUpgrade.IsUniqueFeatureUsed())
        {
            if (!currentOxygenTankUpgrade.IsLowTierUpgrade())
            {
                oxygenDepletionRate = currentOxygenTankUpgrade.GetDefaultOxygenDepletionRate();
            }
            else
            {
                StartCoroutine(DelayHealthDepletion());
            }
        }
        else
        {
            oxygenDepletionRate = currentOxygenTankUpgrade.GetOxygenDepletionRate(pressureStageChecker.GetStage());
        }

        if (PlayerPrefs.GetFloat("oxygenSupply") > 0)
        {
            oxygenSupply = PlayerPrefs.GetFloat("oxygenSupply");
        }

        if (oxygenSupply > 0)
        {
            InvokeRepeating("TankFall", oxygenDepletionRate, oxygenDepletionRate);
        }

        //oxygenSupply = 0;
    }

    IEnumerator DelayHealthDepletion()
    {
        yield return new WaitForSeconds(5);
        InvokeRepeating("TankFall", oxygenDepletionRate, oxygenDepletionRate);
    }

    private void Update()
    {
        if (isDead == true) { return; }
        //Debug.Log(health);
        HealthAndOxygenBehavior();

        if (this.health <= 0)
        {
            KillPlayer();
        }
    }

    public Hull GetHull()
    {
        return currentHullUpgrade;
    }

    public void GetDamage(float damage)
    {
        if (playerMovement.IsInvincible()) { return; }

        if (damage > 0 && this.health != this.health - damage)
        {
            OnHPDeplete.Invoke();
        }
        this.health -= damage;
    }

    public void KillPlayer()
    {
        this.sprite.enabled = false;
        this.GetComponent<BoxCollider2D>().enabled = false;
        CancelInvoke();
        StartCoroutine(EnableLoseScreen());
        isDead = true;
        OnKO.Invoke();
        // Delete all persistent data path save files
        DeletePersistentPathDirAndPlayerPrefs();

    }
    private void OnDisable()
    {
        //DeletePersistentPathDirAndPlayerPrefs();
    }
    private void OnDestroy()
    {
        //DeletePersistentPathDirAndPlayerPrefs();
    }
    public void DeletePersistentPathDirAndPlayerPrefs()
    {
        saveHealth();
        // Ref: https://answers.unity.com/questions/1619142/delete-all-save-files-in-persistentdatapth.html
        string[] filePaths = Directory.GetFiles(Application.persistentDataPath, "*.dat*");
        foreach (string filePath in filePaths) File.Delete(filePath);
    }
    IEnumerator EnableLoseScreen()
    {
        yield return new WaitForSeconds(1f);
        loseScreen.SetActive(true);
    }

    void HealCommand()
    {
        if (availableHealthKits > 0 && healthBar.value < healthBar.maxValue)
        {
            healthChargeGroup.transform.GetChild(availableHealthKits - 1).transform.GetChild(0).gameObject.SetActive(false);
            health += currentSelfRepairUpgrade.GetHealthReplenishPercentage() / 100 * currentHullUpgrade.GetHealth();
            availableHealthKits--;
            PlayerPrefs.SetInt("healthKits", availableHealthKits);
            Debug.Log("availeble health kits: " + PlayerPrefs.GetInt("healthKits"));
            AudioManager.instance.PlaySFX("heal");
        }
        else if (healthBar.value == healthBar.maxValue)
        {
            Debug.Log("Health is full " + healthBar.value);
        }
        else
        {
            Debug.Log("All out of kits");
        }
    }
    private void HealthAndOxygenBehavior()
    {
        if (healthBar != null || oxygenSupplyBar != null)
        {
            healthBar.value = healthBar.maxValue * (this.health / currentHullUpgrade.GetHealth());
            oxygenSupplyBar.value = oxygenSupplyBar.maxValue * (this.oxygenSupply / currentOxygenTankUpgrade.GetOxygenSupplyAmount());
        }

        // HealCommand();
        //if(Input.GetKeyDown(KeyCode.Q))
        //{
        //    if (availableHealthKits > 0)
        //    {
        //        healthChargeGroup.transform.GetChild(availableHealthKits - 1).transform.GetChild(0).gameObject.SetActive(false);
        //        health += currentSelfRepairUpgrade.GetHealthReplenishPercentage() / 100 * currentHullUpgrade.GetHealth();
        //        availableHealthKits--;
        //    }
        //    else
        //    {
        //        Debug.Log("All out of kits");
        //    }
        //}

        if (oxygenSupply <= 0)
        {
            isOxygenTankEmpty = true;
            if (health > 0)
            {
                if(currentOxygenTankUpgrade.IsUniqueFeatureUsed() && currentOxygenTankUpgrade.IsLowTierUpgrade())
                {
                    //if(playOnce == false)
                    //{
                    //    Invoke("HealthDeplete", 5);
                    //}
                    //playOnce = true;
                    Invoke("SetPlayOnce", 5);
                    if(playOnce == true)
                    {
                        HealthDeplete();
                    }
                }
                else
                {
                    //if (healthDepletionTime <= 0)
                    //{
                    //    HealthDeplete();
                    //    healthDepletionTime = healthDepletionRate;
                    //}
                    //else
                    //{
                    //    healthDepletionTime -= Time.deltaTime;
                    //}
                    HealthDeplete();
                }
            }
        }

        if (!isOxygenTankEmpty && currentSelfRepairUpgrade.IsUniqueFeatureUsed() && !currentSelfRepairUpgrade.IsLowTierUpgrade())
        {
            if (selfRepairTime <= 0 && health <= currentHullUpgrade.GetHealth())
            {
                //Debug.Log("Regen");
                health += currentSelfRepairUpgrade.GetHealthRegenerationAmount();
                selfRepairTime = currentSelfRepairUpgrade.GetHealthRegenerationRate();
            }
            else
            {
                selfRepairTime -= Time.deltaTime;
            }
        }

        PlayerPrefs.SetFloat("oxygenSupply", oxygenSupply);

    }

    public UnityEvent OnHPDeplete;

    void SetPlayOnce()
    {
        playOnce = true;
    }

    void HealthDeplete()
    {
        if (healthDepletionTime <= 0)
        {
            //HealthDeplete();
            health -= healthDepletionAmount;
            healthDepletionTime = healthDepletionRate;
        }
        else
        {
            healthDepletionTime -= Time.deltaTime;
        }
        //health -= healthDepletionAmount;
    }

    void TankFall()
    {
        oxygenSupply -= oxygenDepletionAmount;
    }
    public UnityEvent OnKO;
    public void OnInputSecondaryCommandSlot4(InputAction.CallbackContext context)
    {
        if (context.action.phase == InputActionPhase.Started)
        {
            HealCommand();
        }
    }

    public void saveHealth()
    {
        if (health > 0)
        {
            //PlayerPrefs.SetInt("healthKits", 3);
            PlayerPrefs.SetFloat("health", health);
        }
        else if (health <= 0)
        {
            PlayerPrefs.DeleteKey("health");
            //PlayerPrefs.DeleteKey("healthKits");
            PlayerPrefs.SetInt("healthKits", 3);
        }
    }

}
