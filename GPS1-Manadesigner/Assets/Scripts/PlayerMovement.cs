using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerMovement : Player
{
    UpgradeHolder upgradeHolder;

    [Space]

    [Header("Upgrade")]
    [SerializeField] Turbine currentTurbineUpgrade;
    [SerializeField] Inventory currentInventoryUpgrade;

    [Space]

    [Header("Movement")]
    [Range(0.0f, 1.0f)]
    [SerializeField] float dampingWhenStopping;
    [Range(0.0f, 1.0f)]
    [SerializeField] float dampingWhenTurning;
    [Range(0.0f, 1.0f)]
    [SerializeField] float dampingBasic;
    [SerializeField] GameObject[] bubbles;
    [SerializeField] ParticleSystem woosh;
    [SerializeField] ParticleSystem woosh2;
    
    [Header("Inventory")]
    [SerializeField] Slider resourceCollectionBar;
    [SerializeField] Text slotsAreFullText;
    [SerializeField] private storage storage;
    [SerializeField] private float startingCollectionTime = 3;
    private float collectionTime = 0;
    public float dashSpeed = 100f;
    public float timeToStopDash = 1f;
    public float cooldown = 5f;
    private Collider2D col;
    private Rigidbody2D rig;

    public GameObject[] allEnemy;
    //private Transform closestEnemy;
    //private bool enemyContact;
    public bool cooldownFlag;
    public bool isDashing;
    public bool isInvincible = false;

    //Rigidbody2D playerRigidbody2D;

    public float moveHorizontalAxis; // new Input System
    public float moveVerticalAxis;

    public Animator anim;

    PlayerAttack pAttack;



    private void Awake()
    {
        TryGetComponent(out pAttack);
        col = GetComponent<Collider2D>();
        rig = GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        upgradeHolder = GetComponent<UpgradeHolder>();

        currentTurbineUpgrade = upgradeHolder.TurbineUpgrade;
        currentInventoryUpgrade = upgradeHolder.InventoryUpgrade;

        //storage.SetInventoryUpgrade(currentInventoryUpgrade);

        playerRigidbody2D = GetComponent<Rigidbody2D>();
        InvokeRepeating("InstantiateBubbles", 0.3f, 0.3f);
    }

    private void Update()
    {
        if (resourceCollectionBar == null) { return; }

        Vector2 screenPos = Camera.main.WorldToScreenPoint(new Vector2(transform.position.x, transform.position.y + 1));
        resourceCollectionBar.transform.position = screenPos;
        resourceCollectionBar.value = resourceCollectionBar.maxValue * (collectionTime / startingCollectionTime);

        Vector2 slotScreenPos = Camera.main.WorldToScreenPoint(new Vector2(transform.position.x, transform.position.y + 2));
        slotsAreFullText.transform.position = slotScreenPos;

    }

    private void FixedUpdate()
    {
        if (isDead == true) { return; }
        Movement();
        if (Convert.ToBoolean(PlayerPrefs.GetInt("ShootAimbyMovement")) == true)
        {
            var turret = pAttack.turret;
            var turretRotZOffset = pAttack.turretRotZOffset;
            var turretLerpRotSpeed = pAttack.turretLerpRotSpeed;
            // Rotate Turret By Move Axis
            if (turret != null)
            {
                if (turret != null && moveHorizontalAxis != 0 || moveVerticalAxis != 0)
                {
                    //Ref: https://forum.unity.com/threads/using-right-analogue-stick-to-control-character-facing-direction.358084/
                    Vector3 targetPos = turret.position + moveHorizontalAxis * 100 * Vector3.right + moveVerticalAxis * 100 * Vector3.up;
                    Debug.DrawLine(turret.position, targetPos + Vector3.one * 10);
                    //Ref: https://answers.unity.com/questions/585035/lookat-2d-equivalent-.html?page=2&pageSize=5&sort=votes
                    Vector3 diff = targetPos - turret.position;
                    diff.Normalize();
                    float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                    Quaternion targetRot = Quaternion.Euler(0f, 0f, rot_z - 90);
                    targetRot.eulerAngles += Vector3.forward * turretRotZOffset;
                    turret.rotation = Quaternion.Slerp(turret.rotation, targetRot, Time.deltaTime * turretLerpRotSpeed);

                }
            }
        }

        Command_Interact();
    }

    public void Command_Interact()
    {
        if (resourceCollectionBar == null) { return; }
        if (/*!storage.IsFull() && */commandSlot3Context.action!=null)
        {
            if (commandSlot3Context.action.IsPressed())
            {
                if (ResourceProperty != null/* && storage.CheckIfSlotIsEmpty(ResourceProperty.GetItemType())*/)
                {
                    if (storage.CheckIfSlotIsEmpty(ResourceProperty.GetItemType()))
                    {
                        resourceCollectionBar.gameObject.SetActive(true);
                        if (collectionTime < startingCollectionTime)
                        {
                            collectionTime += Time.deltaTime;
                        }
                        else
                        {
                            if (storage.addItem(ResourceProperty.GetItemType()))
                            {
                                Destroy(ResourceProperty.gameObject);
                                resourceCollectionBar.gameObject.SetActive(false);
                                AudioManager.instance.PlaySFX("CollectEnd");
                            }
                        }
                    }
                    else
                    {
                        slotsAreFullText.gameObject.SetActive(true);
                        Debug.Log("This slot is full");
                    }
                }
                //else
                //{
                //    slotsAreFullText.gameObject.SetActive(true);
                //    Debug.Log("This slot is full");
                //}
            }
            else
            {
                resourceCollectionBar.gameObject.SetActive(false);
                slotsAreFullText.gameObject.SetActive(false);
                collectionTime = 0;
            }
        }
    }



    //Ref: https://docs.unity3d.com/Packages/com.unity.inputsystem@0.9/manual/Migration.html
    public void OnInputMoveHorizontalAxis(InputAction.CallbackContext context)
    {
        moveHorizontalAxis = context.action.ReadValue<float>();
    }
    public void OnInputMoveVerticalAxis(InputAction.CallbackContext context)
    {
        moveVerticalAxis = context.action.ReadValue<float>();

        
    }
    InputAction.CallbackContext commandSlot3Context;
    public void OnInputCommandSlot3(InputAction.CallbackContext context)
    {
        commandSlot3Context = context;
    }
    private void Movement()
    {
        float delta = currentTurbineUpgrade.GetTurbineSpeed();

        float horizontalVelocity = playerRigidbody2D.velocity.x;
        horizontalVelocity +=moveHorizontalAxis;

        float verticalVelocity = playerRigidbody2D.velocity.y;
        verticalVelocity += moveVerticalAxis;

        anim.SetBool("IsMoving", moveHorizontalAxis != 0 || moveVerticalAxis != 0);
        #region Flip Player
        if (moveHorizontalAxis < 0)
        {
            sprite.gameObject.transform.localScale = new Vector2(-1, 1);
        }
        else if (moveHorizontalAxis > 0)
        {
            sprite.gameObject.transform.localScale = new Vector2(1, 1);
        }
        #endregion

        #region Horizontal Damping
        if (Mathf.Abs(moveHorizontalAxis) < 0.01f)
        {
            horizontalVelocity *= Mathf.Pow((1f - dampingWhenStopping) * delta, Time.deltaTime);
        }
        else if (Mathf.Sign(moveHorizontalAxis) != Mathf.Sign(horizontalVelocity))
        {
            horizontalVelocity *= Mathf.Pow((1f - dampingWhenTurning) * delta, Time.deltaTime);
        }
        else
        {
            horizontalVelocity *= Mathf.Pow((1f - dampingBasic) * delta, Time.deltaTime);
        }
        #endregion

        #region Vertical Damping
        if (Mathf.Abs(moveVerticalAxis) < 0.01f)
        {
            verticalVelocity *= Mathf.Pow((1f - dampingWhenStopping) * delta, Time.deltaTime);
        }
        else if (Mathf.Sign(moveVerticalAxis) != Mathf.Sign(verticalVelocity))
        {
            verticalVelocity *= Mathf.Pow((1f - dampingWhenTurning) * delta, Time.deltaTime);
        }
        else
        {
            verticalVelocity *= Mathf.Pow((1f - dampingBasic) * delta, Time.deltaTime);
        }
        #endregion

        playerRigidbody2D.velocity = new Vector2(horizontalVelocity, verticalVelocity);

        if (currentTurbineUpgrade.IsUniqueFeatureUsed())
        {
            Vector2 playerDric = playerRigidbody2D.velocity.normalized;
            dash(dashSpeed * playerDric.x, dashSpeed * playerDric.y);
        }

    }

    void InstantiateBubbles()
    {
        if (bubbles.Length <= 0) { return; }
        int random = UnityEngine.Random.Range(0, bubbles.Length);

        GameObject bubble = Instantiate(bubbles[random], transform.position, Quaternion.identity);
        //Debug.Log("Bubbles!");
        Destroy(bubble, 1f);
    }

    private void dash(float speedX, float speedY)
    {

        if (Input.GetKeyDown(KeyCode.LeftShift) && cooldownFlag == false)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            rig.AddForce(new Vector2(speedX, speedY), ForceMode2D.Impulse);
            cooldownFlag = true;
            isDashing = true;
            StartCoroutine("timeToStop");
            if (moveHorizontalAxis > 0)
            {
                sprite.gameObject.transform.localScale = new Vector2(-1, 1);
                woosh.Play();
            }
            else if (moveHorizontalAxis < 0)
            {
                sprite.gameObject.transform.localScale = new Vector2(1, 1);
                woosh2.Play();
            }

        }
        else
        {

            rig.AddForce(new Vector2(0, 0), ForceMode2D.Impulse);

        }

        StartCoroutine("dashCooldown");

    }


    Resource ResourceProperty { get; set; }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Resource>())
        {
            Resource resource = collision.gameObject.GetComponent<Resource>();
            ResourceProperty = resource;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Resource>())
        {
            collectionTime = 0;
            ResourceProperty = null;
        }
    }

    IEnumerator dashCooldown()
    {

        yield return new WaitForSeconds(cooldown);
        cooldownFlag = false;

    }

    IEnumerator timeToStop()
    {

        yield return new WaitForSeconds(timeToStopDash);
        isDashing = false;
        transform.GetChild(0).gameObject.SetActive(true);
        //rig.velocity = Vector2.zero;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnemyHealth enemy;

        if (currentTurbineUpgrade.IsUniqueFeatureUsed() && !currentTurbineUpgrade.IsLowTierUpgrade() && isDashing)
        {
            if (collision.gameObject.GetComponent<EnemyHealth>())
            {
                enemy = collision.gameObject.GetComponent<EnemyHealth>();
                enemy.GetDamage(100);
                rig.velocity *= Vector2.zero;
                isDashing = false;
                transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    public bool IsInvincible()
    {
        if (currentTurbineUpgrade.IsUniqueFeatureUsed() && !currentTurbineUpgrade.IsLowTierUpgrade() && isDashing)
        {
            isInvincible = true;
        }
        return isInvincible;
    }
}
