using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turbineUnique : MonoBehaviour
{

    private Collider2D col;
    private Rigidbody2D rig;
    public float speed = 4f;
    public float timeToStopDash = 1f;
    public float cooldown = 5f;

    public GameObject[] allEnemy;
    //private Transform closestEnemy;
    //private bool enemyContact;
    private bool cooldownFlag;


    private void Awake()
    {

        col = GetComponent<Collider2D>();
        rig = GetComponent<Rigidbody2D>();

    }

    private void Start()
    {

        //closestEnemy = null;
        //enemyContact = false;
        cooldownFlag = false;

    }

    //private Transform getNearestEnemy()
    //{

    //    allEnemy = GameObject.FindGameObjectsWithTag("Enemy");
    //    float nearestDistance = Mathf.Infinity;
    //    Transform trans = null;

    //    foreach(GameObject go in allEnemy)
    //    {

    //        float currentDistance;
    //        currentDistance = Vector2.Distance(transform.position, go.transform.position);
    //        if (currentDistance < nearestDistance)
    //        {

    //            nearestDistance = currentDistance;
    //            trans = go.transform;

    //        }

    //    }

    //    return trans;

    //}

    private void dash(float speedX, float speedY)
    {

        if (Input.GetKeyDown(KeyCode.LeftShift) && cooldownFlag == false)
        {

            rig.AddForce(new Vector2(speedX, speedY), ForceMode2D.Impulse);
            cooldownFlag = true;
            StartCoroutine("timeToStop");

        }
        else
        {

            rig.AddForce(new Vector2(0, 0), ForceMode2D.Impulse);

        }

        StartCoroutine("dashCooldown");

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        for (int size = 0; size < allEnemy.Length; size++)
        {

            if (collision.gameObject == allEnemy[size])
            {

                Debug.Log("hit");
                rig.velocity = Vector2.zero;

            }

        }

    }

    private void Update()
    {

        if (Input.GetKey(KeyCode.D))
        {

            dash(speed, 0);

        }
        else if (Input.GetKey(KeyCode.A))
        {

            dash(-speed, 0);

        }
        else if (Input.GetKey(KeyCode.W))
        {

            dash(0, speed);

        }
        else if (Input.GetKey(KeyCode.S))
        {

            dash(0, -speed);

        }

    }

    IEnumerator dashCooldown()
    {

        yield return new WaitForSeconds(cooldown);
        cooldownFlag = false;

    }

    IEnumerator timeToStop()
    {

        yield return new WaitForSeconds(timeToStopDash);
        rig.velocity = Vector2.zero;

    }

}