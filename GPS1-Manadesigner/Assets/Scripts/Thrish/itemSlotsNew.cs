using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class itemSlotsNew : MonoBehaviour, IPointerClickHandler
{

    [SerializeField] Image Image;
    [SerializeField] Text itemAmountText;
    public event System.Action<itemNew> OnRightClickEvent;

    [SerializeField] private itemNew _item;
    public itemNew Item
    {
        get { return _item; }
        set
        {
            _item = value;
            if (_item == null)
            {
                Image.enabled = false;
            }
            else
            {
                Image.sprite = _item.Sprite;
                Image.enabled = true;
            }
        }
    }

    private int _amount;
    public int Amount
    {
        get { return _amount; }
        set
        {
            _amount = value;
            itemAmountText.enabled = _item != null && _item.MaxStack > 1 && _amount > 1;
            if (itemAmountText.enabled)
            {
                itemAmountText.text = _amount.ToString();
            }
        }
    }


    private void OnValidate()
    {

        if (Image == null)
        {

            Image = GetComponent<Image>();

        }

        if (itemAmountText == null)
        {

            itemAmountText = GetComponentInChildren<Text>();

        }

    }



    public void OnPointerClick(PointerEventData eventData)
    {

        if (eventData != null && eventData.button == PointerEventData.InputButton.Right)
        {

            if (Item != null && OnRightClickEvent != null)
            {

                OnRightClickEvent(Item);

            }

        }

    }
}
