using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class itemNew : ScriptableObject
{

    public string resourceName;
    public Sprite Sprite;
    [Range(1, 999)]
    public int MaxStack = 10;

    public virtual itemNew getCopy()
    {

        return this;

    }

}
