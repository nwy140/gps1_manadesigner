using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventoryNew : MonoBehaviour
{
    [SerializeField] itemNew item;
    [SerializeField] List<itemNew> items;
    [SerializeField] Transform itemsParent;
    [SerializeField] itemSlotsNew[] itemSlots;

    public event System.Action<itemNew> OnItemRightClickEvent;

    private void Awake()
    {

        for (int i = 0; i < itemSlots.Length; i++)
        {

            itemSlots[i].OnRightClickEvent += OnItemRightClickEvent;

        }

    }

    private void OnValidate()
    {

        if (itemsParent != null)
        {

            itemSlots = itemsParent.GetComponentsInChildren<itemSlotsNew>();

            refreshUI();

        }

    }

    private void refreshUI()
    {

        int i = 0;

        for (; i < items.Count && i < itemSlots.Length; i++)
        {

            itemSlots[i].Item = items[i];

        }

        for (; i < itemSlots.Length; i++)
        {

            itemSlots[i].Item = null;

        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {

            addItem(item);

        }
    }

    public bool addItem(itemNew item)
    {

        if (IsFull())
        {
            return false;
        }
        else
        {
            bool isSimilarItemFound = false;

            for (int i = 0; i < itemSlots.Length; i++)
            {
                if (itemSlots[i].Item == item)
                {
                    if (itemSlots[i].Amount < item.MaxStack)
                    {
                        isSimilarItemFound = true;
                        itemSlots[i].Amount++;
                    }
                }
            }

            if (isSimilarItemFound == false)
            {
                for (int i = 0; i < itemSlots.Length; i++)
                {
                    if (itemSlots[i].Item == null)
                    {
                        itemSlots[i].Item = this.item;
                    }

                }
                items.Add(item);

                refreshUI();
            }
            return true;
        }

        //if (IsFull())
        //{

        //    return false;

        //}
        //else
        //{
        //    items.Add(item);
        //    refreshUI();
        //    return true;
        //}

        //for (int i = 0; i < itemSlots.Length; i++)
        //{
        //    if (itemSlots[i].Item == null || (itemSlots[i].Amount < item.MaxStack))
        //    {
        //        itemSlots[i].Item = item;
        //        itemSlots[i].Amount++;
        //        //return true;
        //    }

        //}
        //return false;
    }

    public bool removeItem(item item)
    {

        for (int i = 0; i < itemSlots.Length; i++)
        {
            if (itemSlots[i].Item == item)
            {
                itemSlots[i].Amount--;
                if (itemSlots[i].Amount == 0)
                {
                    itemSlots[i].Item = null;
                }
                return true;
            }

        }
        return false;

    }

    public bool IsFull()
    {

        return items.Count >= itemSlots.Length;

    }

}

