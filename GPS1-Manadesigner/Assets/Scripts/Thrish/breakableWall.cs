using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;

public class breakableWall : MonoBehaviour
{

    public GameObject bulletPrefab;
    public ParticleSystem par;
    public ParticleSystem par2;
    public ParticleSystem par3;

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Breakable Wall Bullet"))
        {
            Debug.Log("collided");
            Destroy(gameObject);
            Destroy(collision.gameObject);
            par.Play();
            par2.Play();
            par3.Play();
        }

    }

}
