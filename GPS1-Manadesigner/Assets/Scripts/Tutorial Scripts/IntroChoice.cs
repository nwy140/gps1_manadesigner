using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroChoice : MonoBehaviour
{
    public static Introduction instance;

    [Header("Panels")]
    //public GameObject introPanel;
    public GameObject choicePanel;

    [Header("Scientist Sprites")]
    public GameObject scientistTalk;
    public GameObject scientistIdle;

    [Header("Index")]
    public int index = 0;

    DialogueSystem dialogue;

    private void Start()
    {
        dialogue = DialogueSystem.instance;
        choicePanel.SetActive(false);
    }

    void Update()
    {
        if (Introduction.instance.askAgain)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
                {
                    if (index >= brief.Length)
                    {
                        return;
                    }

                    Talk(brief[index]);
                    index++;

                    if (index == brief.Length)
                    {
                        SceneManager.LoadScene("Tutorial (Gameplay)");
                        Debug.Log("End of story.");
                    }

                    if(index == 1 || index == 5 || index == 9 || index == 11 || index == 13 || index == 15 || index == 18)
                    {
                        scientistTalk.SetActive(false);
                        scientistIdle.SetActive(true);
                    }

                    else
                    {
                        scientistTalk.SetActive(true);
                        scientistIdle.SetActive(false);
                    }
                }
            }
        }

        else if(Introduction.instance.hasUnderstand)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
                {
                    if (index >= endBriefing.Length)
                    {
                        return;
                    }

                    Talk(endBriefing[index]);
                    index++;

                    if (index == endBriefing.Length)
                    {
                        SceneManager.LoadScene("Tutorial (Gameplay)");
                        Debug.Log("End of story.");
                    }

                    if(index == 1 || index == 3 || index == 5 || index == 6)
                    {
                        scientistTalk.SetActive(true);
                        scientistIdle.SetActive(false);
                    }

                    else
                    {
                        scientistTalk.SetActive(false);
                        scientistIdle.SetActive(true);
                    }
                }
            }
        }
    }
    void Talk(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string scientist = (parts.Length >= 2) ? parts[1] : "";

        dialogue.StartTalking(speech, scientist);
    }

    [TextArea]
    public string noticeToAllTechs = "Leave the last element in every dialogue boxes empty or there'll be an index boundary error.";

    public string[] brief = new string[]
    {
        "The way Liam�s eyes squint at you is almost imperceptible, but you catch it regardless. It makes you flinch a little. :You",
        "Alright, fine. Pay attention this time. :Liam",
        "As you know, the USSR has been making rapid advancements on their nautical travel technology. ",
        "Word has it that they�re close to reaching the bottom.",
        "His lips form into a thin line. You can hear the disdain behind his words. :You",
        "Naturally, that means the Cold War is drawing to a close very soon, with the USSR coming out as the victors. :Liam",
        "Which is, well, not very ideal for us, no.",
        "That�s where you come in.",
        "Liam inclines his head at you. :You",
        "You�ll be helping us gather live specimens for our research, hopefully efficiently enough, to let us better equip the submarine for deeper diving. :Liam",
        "His hand gestures towards the door that leads to the submarine docking station. :You",
        "And also do the diving to the bottom of the ocean, preferably before the USSR does. :Liam",
        "He finishes, and folds his arms together. :You",
        "We don�t know how long we have left, exactly, but we know it�s soon. :Liam",
        "Liam takes out a clipboard from his coat and writes something down. :You",
        "We�ll have to act quickly, especially when we can�t know when it�ll happen. :Liam",
        "Let�s get you acquainted with the submarine, first. It�s a little� different from the conventional military models.",
        "You trail behind Liam as he leads you to the submarine, visibly vibrating in excitement. :You",
        " "
    };

    public string[] endBriefing = new string[]
    {
        "Good, good. :Liam",
        "Satisfied, he nods. :You",
        "We don�t know how long we have left, exactly, but we know it�s soon. :Liam",
        "Liam takes out a clipboard from his coat and writes something down. :You",
        "We�ll have to act quickly, especially when we can�t know when it�ll happen. :Liam",
        "Let�s get you acquainted with the submarine, first. It�s a little� different from the conventional military models.",
        "You trail behind Liam as he leads you to the submarine, visibly vibrating in excitement. :You",
        " "
    };
}
