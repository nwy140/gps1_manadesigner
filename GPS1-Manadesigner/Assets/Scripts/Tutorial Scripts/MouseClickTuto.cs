using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClickTuto : Tutorial
{
    public GameObject indication;

    public override void CheckIfHappening()
    {
        if (indicationExist)
        {
            indication.SetActive(true);
        }

        if (Input.GetMouseButtonDown(0))
        {
            TutoManager.instance.EndTuto();
            indication.SetActive(false);
        }
    }
}
