using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Introduction : MonoBehaviour
{
    public static Introduction instance;

    [Header("Panels")]
    public GameObject introPanel;
    public GameObject choicePanel;

    [Header("Scientist Sprites")]
    public GameObject scientistTalk;
    public GameObject scientistIdle;
    public GameObject glasses;
    

    [Header("Index")]
    public int index = 0;
    [HideInInspector]public bool askAgain = false;
    [HideInInspector] public bool hasUnderstand = false;

    DialogueSystem dialogue;

    void Start()
    {
        dialogue = DialogueSystem.instance;
        choicePanel.SetActive(false);
    }

    private void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
            {
                if (index >= intro.Length)
                {
                    return;
                }

                Talk(intro[index]);
                index++;

                if (index == intro.Length)
                {
                    scientistIdle.SetActive(true);
                    choicePanel.SetActive(true);
                    introPanel.SetActive(false);
                }

                if (index >= 5 && index <= 6 || index == 8 || index >= 10 && index <= 15 || index >= 17 && index <= 19 || index >= 21 && index <= 24)
                {
                    scientistTalk.SetActive(true);
                    scientistIdle.SetActive(false);
                    glasses.SetActive(false);
                }

                else if(index == 7)
                {
                    scientistTalk.SetActive(false);
                    scientistIdle.SetActive(false);
                    glasses.SetActive(true);
                }

                else if(index == 9 || index == 16 || index == 20)
                {
                    scientistTalk.SetActive(false);
                    scientistIdle.SetActive(true);
                    glasses.SetActive(false);
                }

                else
                {
                    scientistTalk.SetActive(false);
                    scientistIdle.SetActive(false);
                    glasses.SetActive(false);
                }

            }
        }
    }

    void Talk(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string scientist = (parts.Length >= 2) ? parts[1] : "";

        dialogue.StartTalking(speech, scientist);
    }

    public void ExplainAgain()
    {
        askAgain = true;
        choicePanel.SetActive(false);
        introPanel.SetActive(true);
    }

    public void EndBrief()
    {
        hasUnderstand = true;
        choicePanel.SetActive(false);
        introPanel.SetActive(true);
    }


    [Header("Dialogue Lines")]
    public string[] intro = new string[]
    {
        "As the Navy�s finest diver and submarine operator, you�ve been transferred to a new assignment.",
        "You think it�s way above your paygrade, especially after listening to the brief.",
        "Still, you weren�t in a position to decline. They seemed� desperate, almost.",
        "A sharply dressed man in a lab coat enters the room, fluorescent lights reflecting off the lens of his glasses.",
        "Ah, you must be the new diver they issued. :Liam",
        "Heard you were one of the best.",
        "He adjusts his glasses. You nod. :You",
        "Good, good. :Liam",
        "He offers you a hand to shake, and you accept it. :You",
        "Liam McCoy. Head researcher of the NaMSA Mariana outpost. :Liam", 
        "The tag on his coat�s lapel says as much. :You", 
        "I�ll be the one overseeing your time here, for all intents and purposes. :Liam", 
        "I know you must�ve gotten a brief prior coming here, but let�s just make sure we�re on the same page.",

        //---------------------- REPEAT BRIEF ----------------------

        "As you know, the USSR has been making rapid advancements on their nautical travel technology. ",
        "Word has it that they�re close to reaching the bottom.",
        "His lips form into a thin line. You can hear the disdain behind his words. :You",
        "Naturally, that means the Cold War is drawing to a close very soon, with the USSR coming out as the victors. :Liam",
        "Which is, well, not very ideal for us, no.",
        "That�s where you come in.",
        "Liam inclines his head at you. :You",
        "You�ll be helping us gather live specimens for our research, hopefully efficiently enough, to let us better equip the submarine for deeper diving. :Liam",
        "His hand gestures towards the door that leads to the submarine docking station. :You",
        "And also do the diving to the bottom of the ocean, preferably before the USSR does. :Liam",

        //---------------------- END BRIEF ----------------------

        "Any questions?"
    };

    
}
