using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public int sequence;
    public bool indicationExist;
    [TextArea(3, 10)]
    public string tutoDialogue;


    private void Awake()
    {
        TutoManager.instance.tutorials.Add(this);
    }

    public virtual void CheckIfHappening()
    {

    }
}
