using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutoManager : MonoBehaviour
{
    public List<Tutorial> tutorials = new List<Tutorial>();
    public Text dialogue;

    private static TutoManager tuto;
    Tutorial current;

    public static TutoManager instance
    {
        get
        {
            if(tuto == null)
            {
                tuto = GameObject.FindObjectOfType<TutoManager>();
                Debug.Log("There's no tutorial manager.");
            }

            return tuto;
        }
    }

    void Start()
    {
        NextTutorial(0);
    }

    void Update()
    {
        if(current)
        {
            current.CheckIfHappening();
        }
    }

    public Tutorial TutoSequence(int order)
    {
        for (int i = 0; i < tutorials.Count; i++)
        {
            if(tutorials[i].sequence == order)
            {
                return tutorials[i];
            }
        }

        return null;
    }

    public void NextTutorial(int onSequence)
    {
        current = TutoSequence(onSequence);

        if (!current)
        {
            TutoComplete();
            return;
        }

        dialogue.text = current.tutoDialogue;
    }

    public void TutoComplete()
    {
        dialogue.text = "Click 'YES' to return back to the outpost.";
    }

    public void EndTuto()
    {
        NextTutorial(current.sequence + 1);
    }

}
