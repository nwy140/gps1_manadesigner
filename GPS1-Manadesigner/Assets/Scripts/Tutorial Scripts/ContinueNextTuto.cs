using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueNextTuto : Tutorial
{
    public GameObject indication;
    float timer = 0f;
    float delay = 3.5f;

    public override void CheckIfHappening()
    {
        if (indicationExist)
        {
            indication.SetActive(true);
        }

        else
        {
            indication.SetActive(false);
        }

        timer += Time.deltaTime;

        if (timer > delay)
        {
            TutoManager.instance.EndTuto();
            indication.SetActive(false);
        }
    }
}
