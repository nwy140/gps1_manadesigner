using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldKeyInputTuto : Tutorial
{
    public KeyCode collectKey;
    
    public override void CheckIfHappening()
    {
        Time.timeScale = 1;

        if (Input.GetKeyUp(collectKey))
        {
            TutoManager.instance.EndTuto();
        }
    }
}
