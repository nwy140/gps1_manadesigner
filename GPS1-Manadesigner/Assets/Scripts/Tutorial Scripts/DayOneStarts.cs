using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DayOneStarts : MonoBehaviour
{
    public static DayOneStarts instance;
    DialogueSystem dialogue;

    public int index = 0;

    [Header("Scientist Sprites")]
    public GameObject scientistTalk;
    public GameObject scientistIdle;
    public GameObject glasses;

    [Header("Day Counter UI")]
    public GameObject actualDayCounter;
    public GameObject timerPlaceholder;

    private void Start()
    {
        dialogue = DialogueSystem.instance;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
            {
                if (index >= dayOne.Length)
                {
                    return;
                }

                Talk(dayOne[index]);
                index++;

                if (index == dayOne.Length)
                {
                    PlayerPrefs.SetFloat("health", -1);
                    PlayerPrefs.SetFloat("oxygenSupply", -1);
                    PlayerPrefs.SetInt("healthKits", -1);
                    SceneManager.LoadScene("Stage Prototype");
                    Debug.Log("End of tutorial.");
                }

                if (index >= 11 && index <= 15)
                {
                    actualDayCounter.SetActive(true);
                    timerPlaceholder.SetActive(false);
                }

                if (index == 1 || index == 4 || index == 6 || index == 9 || index >= 11 && index <= 12)
                {
                    scientistTalk.SetActive(true);
                    scientistIdle.SetActive(false);
                    glasses.SetActive(false);
                }

                else if (index == 2 || index == 10 || index == 15)
                {
                    scientistTalk.SetActive(false);
                    scientistIdle.SetActive(false);
                    glasses.SetActive(true);
                }

                else if (index == 3 || index == 5 || index == 7 || index == 8 || index == 10)
                {
                    scientistTalk.SetActive(false);
                    scientistIdle.SetActive(true);
                    glasses.SetActive(false);
                }

                else
                {
                    scientistTalk.SetActive(false);
                    scientistIdle.SetActive(false);
                    glasses.SetActive(false);
                }

            }
        }
    }

    void Talk(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string scientist = (parts.Length >= 2) ? parts[1] : "";

        dialogue.StartTalking(speech, scientist);
    }

    public string[] dayOne = new string[]
       {
        "There you are, finally! :Liam",
        "He doesn�t seem to be very happy. : ",
        "You�re not late, are you? You ask Liam.",
        "No, no, it�s-- ugh. :Liam",
        "It�s clear that he�s stressed out of his mind, restless feet walking back and forth, restless fingers clicking on his pen. : ",
        "The USSR. We�ve just received intel they�re inbound to the bottom in 14 days. 14 days! That�s barely enough time to do anything. I was expecting soon, but- :Liam",
        "The panic in his voice is thick, so unlike what you�ve seen of him yesterday. : ",
        "You try reassuring him that you�ll do your best, and this calms him, somewhat.",
        "Okay. Okay. :Liam",
        "Liam takes a deep breath and gathers his composure. Then, he stares straight into your eyes, deathly serious. : ",
        "We had set up a day counter for you to keep track on it. You can even check in your pause menu while you're in the ocean.",
        "If you�re really the Navy�s top diver, then you better start now if we�re going to stand a chance. :Liam",
        "You tell him that you�ve got this and hurriedly get changed into your gear. : ",
        "Liam clasps a firm grip onto your shoulder, and nudges you towards the submarine docking station.",
        "We�re counting on you. :Liam",
        " "
    };
}
