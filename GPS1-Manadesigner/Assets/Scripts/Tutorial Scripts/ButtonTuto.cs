using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTuto : Tutorial
{
    public GameObject inventory;
    public GameObject indicator;

    public override void CheckIfHappening()
    {
        if(indicationExist)
        {
            indicator.SetActive(true);
        }

        if (Input.GetMouseButtonDown(0))
        {
            inventory.SetActive(true);
            TutoManager.instance.EndTuto();
            indicator.SetActive(false);
        }
    }
}
