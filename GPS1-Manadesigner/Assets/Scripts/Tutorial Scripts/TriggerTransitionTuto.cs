using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTransitionTuto : Tutorial
{
    public Transform outpostTransition;
    bool ongoingTuto = false;
    
    public override void CheckIfHappening()
    {
        ongoingTuto = true;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!ongoingTuto)
            return;

        if(collision.transform == outpostTransition)
        {
            TutoManager.instance.EndTuto();
            ongoingTuto = false;
        }
    }
}
