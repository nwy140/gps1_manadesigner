using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutoOutpostDialogue : MonoBehaviour
{
    public static TutoOutpostDialogue instance;

    [Header("Indication Panels")]
    public GameObject coverPanel;
    public GameObject tierIndicator;
    public GameObject buyIndicator;

    [Header("Index")]
    public GameObject scientist;
    public int index = 0;

    DialogueSystem dialogue;

    void Start()
    {
        dialogue = DialogueSystem.instance;
    }

    private void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
            {
                if (index >= tutorial.Length)
                {
                    return;
                }

                Talk(tutorial[index]);
                index++;

                if(index == tutorial.Length)
                {
                    SceneManager.LoadScene("Day One Begins");
                    Debug.Log("End of outpost tutorial.");
                }

                if(index == 8)
                {
                    coverPanel.SetActive(false);
                }

                else if (index == 9)
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        index++;
                    }
                }

                else if (index == 12)
                {
                    tierIndicator.SetActive(true);
                }

                else if(index == 13)
                {
                    tierIndicator.SetActive(false);
                    buyIndicator.SetActive(true);
                }

                else if(index == 14)
                {
                    buyIndicator.SetActive(false);

                    float timer = 0f;
                    timer += Time.time;
                }

                else if(index == 27)
                {
                    scientist.SetActive(false);
                }
            }
        }
    }

    void Talk(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string scientist = (parts.Length >= 2) ? parts[1] : "";

        dialogue.StartTalking(speech, scientist);
    }


    [Header("Dialogue Lines")]

    [TextArea]
    public string noticeToAllTechs = "Leave the last element in every dialogue boxes empty or there'll be an index boundary error.";

    public string[] tutorial = new string[]
    {
        "A quiet, content hum from Liam. : ",
        "Pointing at the shelf and wooden counter that was definitely not here from before, he gives you a serious look.",
        "This is where you�ll be handing in your �haul of the day�. :Liam",
        "You see him do air quotes when he says that. : ",
        "We need a certain amount of specimens to be able to complete research on a specific submarine component. :Liam",
        "Once you turn them in, we�ll be able to hand you the research results, which, in your case, is an upgraded component to help you navigate the ocean better.",
        "I�ve set up something to help you get into the routine. ",
        "Liam jabs a finger at the only component on the shelf-- the Oxygen Tank. : ",
        "Click on it to see what upgrades are available for it. :Liam",
        "If you hover over it, you�ll see the details of what and how many of each resource you�ll need to exchange to obtain the upgrade.",
        "Now, click on it.",
        "The upgrade will be available during your next dive, so basically the next day.",
        "Also, let me briefly explain what each components function.",
        "Turbines will speed up the submarine.",
        "Inventory boxes will expand the amount of storage.",
        "Hull shell withstands the water pressure as you dive down. You could say they are like bubble barriers.",
        "Self repair kits could come in handy when your submarine is damaged.",
        "And upgraded weapons... Well, they're just the usual weapons. Could even break through walls, you might say.",
        "Liam tilts his head questioningly at you. : ",
        "Got it? :Liam",
        "You nod. : ",
        "The tension from his shoulders seems to disappear when you did that.",
        "Good. If you�ll excuse me, I�ll be going back to work. :Liam",
        "He doesn�t spare another moment before taking his leave. : ",
        "See you tomorrow. :Liam",
        "You are left to your own devices, unsupervised. : ",
        "You spend the remaining time of your day checking if your submarine is in good order and touring around the outpost.",
        "So far, it�s off to a great start, you think. ",
        " "
    };

}
