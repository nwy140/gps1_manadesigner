using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyInputTuto : Tutorial
{
    public List<KeyCode> keyCodes = new List<KeyCode>();
    
    public override void CheckIfHappening()
    {
        for (int i = 0; i < keyCodes.Count; i++)
        {
            if(Input.GetKey(keyCodes[i]))
            {
                keyCodes.RemoveAt(i);
                break;
            }
        }

        if(keyCodes.Count == 0)
        {
            TutoManager.instance.EndTuto();
        }
    }

}
