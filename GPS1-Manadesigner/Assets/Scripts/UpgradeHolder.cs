using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeHolder : MonoBehaviour
{
    private PlayerSkills playerSkills;
    [SerializeField] private storage storage;

    [Header("Upgrade Save Manager")]
    [SerializeField] UpgradeSaveManager upgradeSaveManager;

    [Header("All Upgrades")]
    [SerializeField] Turbine[] turbineUpgrades;
    [SerializeField] Weapon[] weaponUpgrades;
    [SerializeField] Hull[] hullUpgrades;
    [SerializeField] SelfRepair[] selfRepairUpgrades;
    [SerializeField] OxygenTank[] oxygenTankUpgrades;
    [SerializeField] Inventory[] inventoryUpgrades;

    [Header("Current Upgrades")]
    [SerializeField] Turbine currentTurbineUpgrade;
    [SerializeField] Weapon currentWeaponUpgrade;
    [SerializeField] Hull currentHullUpgrade;
    [SerializeField] SelfRepair currentSelfRepairUpgrade;
    [SerializeField] OxygenTank currentOxygenTankUpgrade;
    [SerializeField] Inventory currentInventoryUpgrade;

    private void Awake()
    {
        playerSkills = new PlayerSkills();
        playerSkills.OnSkillUnlocked += PlayerSkills_OnSkillUnlocked;
        upgradeSaveManager = FindObjectOfType<UpgradeSaveManager>();
        upgradeSaveManager.LoadUpgrade(this);
        playerSkills.GetCurrentUpgrades(this);
        //storage.Init();
    }

    private void PlayerSkills_OnSkillUnlocked(object sender, PlayerSkills.OnSkillUnlockedEventArgs e)
    {
        switch (e.skillType)
        {
            // Turbine
            case PlayerSkills.SkillType.LowTierTurbineStage1:
                TurbineUpgrade = turbineUpgrades[0];
                break;
            case PlayerSkills.SkillType.LowTierTurbineStage2:
                TurbineUpgrade = turbineUpgrades[1];
                break;
            case PlayerSkills.SkillType.HighTierTurbineStage1:
                TurbineUpgrade = turbineUpgrades[2];
                break;
            case PlayerSkills.SkillType.HighTierTurbineStage2:
                TurbineUpgrade = turbineUpgrades[3];
                break;

            // Weapon
            case PlayerSkills.SkillType.LowTierWeaponStage1:
                WeaponUpgrade = weaponUpgrades[0];
                break;
            case PlayerSkills.SkillType.LowTierWeaponStage2:
                WeaponUpgrade = weaponUpgrades[1];
                break;
            case PlayerSkills.SkillType.HighTierWeaponStage1:
                WeaponUpgrade = weaponUpgrades[2];
                break;
            case PlayerSkills.SkillType.HighTierWeaponStage2:
                WeaponUpgrade = weaponUpgrades[3];
                break;

            // Hull
            case PlayerSkills.SkillType.LowTierHullStage1:
                HullUpgrade = hullUpgrades[0];
                break;
            case PlayerSkills.SkillType.LowTierHullStage2:
                HullUpgrade = hullUpgrades[1];
                break;
            case PlayerSkills.SkillType.LowTierHullStage3:
                HullUpgrade = hullUpgrades[2];
                break;
            case PlayerSkills.SkillType.HighTierHullStage1:
                HullUpgrade = hullUpgrades[3];
                break;
            case PlayerSkills.SkillType.HighTierHullStage2:
                HullUpgrade = hullUpgrades[4];
                break;
            case PlayerSkills.SkillType.HighTierHullStage3:
                HullUpgrade = hullUpgrades[5];
                break;

            // Self Repair
            case PlayerSkills.SkillType.LowTierSelfRepairStage1:
                SelfRepairUpgrade = selfRepairUpgrades[0];
                break;
            case PlayerSkills.SkillType.LowTierSelfRepairStage2:
                SelfRepairUpgrade = selfRepairUpgrades[1];
                break;
            case PlayerSkills.SkillType.HighTierSelfRepairStage1:
                SelfRepairUpgrade = selfRepairUpgrades[2];
                break;
            case PlayerSkills.SkillType.HighTierSelfRepairStage2:
                SelfRepairUpgrade = selfRepairUpgrades[3];
                break;

            // Oxygen Tank
            case PlayerSkills.SkillType.LowTierOxygenTankStage1:
                OxygenTankUpgrade = oxygenTankUpgrades[0];
                break;
            case PlayerSkills.SkillType.LowTierOxygenTankStage2:
                OxygenTankUpgrade = oxygenTankUpgrades[1];
                break;
            case PlayerSkills.SkillType.HighTierOxygenTankStage1:
                OxygenTankUpgrade = oxygenTankUpgrades[2];
                break;
            case PlayerSkills.SkillType.HighTierOxygenTankStage2:
                OxygenTankUpgrade = oxygenTankUpgrades[3];
                break;

            // Inventory
            case PlayerSkills.SkillType.LowTierInventoryStage1:
                InventoryUpgrade = inventoryUpgrades[0];
                break;
            case PlayerSkills.SkillType.LowTierInventoryStage2:
                InventoryUpgrade = inventoryUpgrades[1];
                break;
            case PlayerSkills.SkillType.HighTierInventoryStage1:
                InventoryUpgrade = inventoryUpgrades[2];
                break;
            case PlayerSkills.SkillType.HighTierInventoryStage2:
                InventoryUpgrade = inventoryUpgrades[3];
                break;
        }
    }

    private void Start()
    {
        //storage = FindObjectOfType<storage>();
    }

    #region Setters and Getters
    public Turbine TurbineUpgrade
    {
        get
        {
            return currentTurbineUpgrade;
        }
        set
        {
            this.currentTurbineUpgrade = value;
        }
    }
    public Weapon WeaponUpgrade
    {
        get
        {
            return currentWeaponUpgrade;
        }
        set
        {
            this.currentWeaponUpgrade = value;
        }
    }
    public Hull HullUpgrade
    {
        get
        {
            return currentHullUpgrade;
        }
        set
        {
            this.currentHullUpgrade = value;
        }
    }
    public SelfRepair SelfRepairUpgrade
    {
        get
        {
            return currentSelfRepairUpgrade;
        }
        set
        {
            this.currentSelfRepairUpgrade = value;
        }
    }
    public OxygenTank OxygenTankUpgrade
    {
        get
        {
            return currentOxygenTankUpgrade;
        }
        set
        {
            this.currentOxygenTankUpgrade = value;
        }
    }
    public Inventory InventoryUpgrade
    {
        get
        {
            return currentInventoryUpgrade;
        }
        set
        {
            this.currentInventoryUpgrade = value;
        }
    }

    //Resource ResourceProperty { get; set; }
    public PlayerSkills GetPlayerSkills()
    {
        return this.playerSkills;
    }

    public storage GetStorage()
    {
        return storage;
    }

    public Turbine[] GetTurbineUpgrades()
    {
        return turbineUpgrades;
    }
    public Weapon[] GetWeaponUpgrades()
    {
        return weaponUpgrades;
    }
    public Hull[] GetHullUpgrades()
    {
        return hullUpgrades;
    }
    public SelfRepair[] GetSelfRepairUpgrades()
    {
        return selfRepairUpgrades;
    }
    public OxygenTank[] GetOxygenTankUpgrades()
    {
        return oxygenTankUpgrades;
    }
    public Inventory[] GetInventoryUpgrades()
    {
        return inventoryUpgrades;
    }

    #endregion
}
