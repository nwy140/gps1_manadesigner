using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class itemSlots : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{

    [SerializeField] Image image;
    public Image slotBorder;
    [SerializeField] Text itemAmountText;

    public event Action<itemSlots> OnPointerEnterEvent;
    public event Action<itemSlots> OnPointerExitEvent;
    public event Action<itemSlots> OnLeftClickEvent;
    public event Action<itemSlots> OnBeginDragEvent;
    public event Action<itemSlots> OnEndDragEvent;
    public event Action<itemSlots> OnDragEvent;
    public event Action<itemSlots> OnDropEvent;

    private Color normalColor = Color.white;
    private Color disabledColor = new Color(1, 1, 1, 0);

    public item _item;
    public item Item
    {
        get { return _item; }
        set
        {
            _item = value;

            if (_item == null)
            {
                image.color = disabledColor;
            }
            else
            {
                image.sprite = _item.Icon;
                image.color = normalColor;
            }
        }
    }

    private int _amount;

    public int Amount
    {
        get
        { 
            return _amount; 
        }
        set
        {
            _amount = value;
            itemAmountText.enabled = _item != null && _item.maxStack > 1 && _amount > 1;
            if (itemAmountText.enabled)
            {
                itemAmountText.text = _amount.ToString();
            }
        }
    }

    private void OnValidate()
    {
        if (image == null)
            image = GetComponent<Image>();
    }

    public virtual bool CanReceiveItem(item item)
    {
        return true;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData != null && eventData.button == PointerEventData.InputButton.Left)
        {
            if (OnLeftClickEvent != null)
                OnLeftClickEvent(this);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(OnPointerEnterEvent != null)
            OnPointerEnterEvent(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (OnPointerExitEvent != null)
            OnPointerExitEvent(this);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (OnBeginDragEvent != null)
            OnBeginDragEvent(this);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (OnEndDragEvent != null)
            OnEndDragEvent(this);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (OnDragEvent != null)
            OnDragEvent(this);
    }

    public void OnDrop(PointerEventData eventData)
    {
        if(OnDropEvent != null)
            OnDropEvent(this);
    }
}
