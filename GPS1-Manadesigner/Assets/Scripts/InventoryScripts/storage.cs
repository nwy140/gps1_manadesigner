using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

public class storage : ItemContainer
{
    [FormerlySerializedAs("items")]
    [SerializeField] List<item> startingItems;
    //[SerializeField] List<itemSlots> usedItemSlots;
    public GameObject itemsParentGameObject;
    [SerializeField] Sprite lockSprite;
    [SerializeField] int startingItemsAmount = 1;


    public Inventory inventoryUpgrade;
    [SerializeField] itemSlots itemSlotPrefab;
    [SerializeField] UpgradeHolder upgradeHolder;

    public override void Awake()
    {
        base.Awake();
        SetStartingItems();

        SetInventoryUpgrade(upgradeHolder.InventoryUpgrade, upgradeHolder.InventoryUpgrade.GetStackAmount());
    }

    //private void Start()
    //{
    //    SetInventoryUpgrade(upgradeHolder.InventoryUpgrade, upgradeHolder.InventoryUpgrade.GetStackAmount());
    //}

    private void OnValidate()
    {
        if (itemsParentGameObject.transform != null)
            itemsParentGameObject.transform.GetComponentsInChildren(includeInactive: true, result: ItemSlots);

        if (!Application.isPlaying)
        {
            SetStartingItems();
        }
    }

    private void SetStartingItems()
    {

        int i = 0;

        for (; i < startingItems.Count && i < ItemSlots.Count; i++)
        {

            ItemSlots[i].Item = startingItems[i];
            ItemSlots[i].Amount = startingItemsAmount;

        }

        for (; i < ItemSlots.Count; i++)
        {
            ItemSlots[i].Item = null;
            ItemSlots[i].Amount = 0;
        }

    }

    public override bool addItem(item item)
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if(ItemSlots[i].Item != null)
            {
                if (ItemSlots[i].Item.ID == item.ID && ItemSlots[i].Amount < item.maxStack)
                {
                    ItemSlots[i].Item = item;
                    ItemSlots[i].Item.maxStack = upgradeHolder.InventoryUpgrade.GetStackAmount();
                    ItemSlots[i].Amount++;
                    return true;
                }
            }
        }

        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item == null || (ItemSlots[i].Item.ID == item.ID && ItemSlots[i].Amount < item.maxStack))
            {
                ItemSlots[i].Item = item;
                ItemSlots[i].Item.maxStack = upgradeHolder.InventoryUpgrade.GetStackAmount();
                ItemSlots[i].Amount++;
                return true;
            }
        }
        return false;
    }

    public void SetInventoryUpgrade(Inventory inventoryUpgrade, int amount)
    {
        int i = 0;

        this.inventoryUpgrade = inventoryUpgrade;

        for (; i < inventoryUpgrade.GetNumberOfSlots(); i++)
        {
            //itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<itemSlots>().Item.maxStack = amount;
            //Debug.Log($"Max Stack {itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<itemSlots>().Item.maxStack}");

            if (itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<Image>().sprite == lockSprite)
            {
                itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = null;
                itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<Image>().color = Color.clear;
            }
        }

        for (; i < itemsParentGameObject.transform.childCount; i++)
        {
            itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = lockSprite;
            itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<Image>().color = Color.white;
            ItemSlots.Remove(itemsParentGameObject.transform.GetChild(i).gameObject.GetComponent<itemSlots>());
        }
    }

    public List<itemSlots> GetListOfItemsInInventory()
    {
        List<itemSlots> usedItemSlots = new List<itemSlots>();
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item != null)
            {
                usedItemSlots.Add(ItemSlots[i]);
            }
        }
        return usedItemSlots;
    }
    
}
