using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Items/Item")]
public class item : ScriptableObject
{
    [SerializeField] string id;
    public string ID { get { return id; } }
    public string resourceName;
    public Sprite Icon;
    public string itemDescription;
    [Range(1, 999)]
    public int maxStack = 2;

    public virtual item getCopy()
    {
        return this;
    }    

    public virtual void Destroy()
    {

    }
}
