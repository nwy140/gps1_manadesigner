//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class InventoryTest : MonoBehaviour
//{
//    private Dictionary<FishMaterial.FishMaterialTypes, int> playerFishMaterialInventory = new Dictionary<FishMaterial.FishMaterialTypes, int>();
//    private Dictionary<RawMaterial.RawMaterialTypes, int> playerRawMaterialInventory = new Dictionary<RawMaterial.RawMaterialTypes, int>();

//    // Start is called before the first frame update
//    void Start()
//    {
//    }

//    // Implement using scriptable objects. Look up Wei Yuen's video.

//    private void OnTriggerEnter2D(Collider2D collision)
//    {
//        if(collision.gameObject.GetComponent<FishMaterial>())
//        {
//            FishMaterial.FishMaterialTypes fishMaterial = collision.GetComponent<FishMaterial.FishMaterialTypes>();

//            switch (fishMaterial)
//            {
//                case FishMaterial.FishMaterialTypes.ClownFish:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.ClownFish, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.Octopus:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.Octopus, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.PufferFish:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.PufferFish, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.SpermWhale:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.SpermWhale, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.Shark:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.Shark, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.SnailFish:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.SnailFish, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.LanternFish:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.SwordFish, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.SwordFish:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.SwordFish, 1);
//                    break;
//                case FishMaterial.FishMaterialTypes.Liyvyatan:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.Liyvyatan, 1);
//                    break;
//            }
//        }
//        else if(collision.gameObject.GetComponent<RawMaterial>())
//        {
//            RawMaterial.RawMaterialTypes rawMaterial = collision.GetComponent<RawMaterial.RawMaterialTypes>();

//            switch (rawMaterial)
//            {
//                case RawMaterial.RawMaterialTypes.SeaAnemone:
//                    playerRawMaterialInventory.Add(RawMaterial.RawMaterialTypes.SeaAnemone, 1);
//                    break;
//                case RawMaterial.RawMaterialTypes.Starfish:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.Octopus, 1);
//                    break;
//                case RawMaterial.RawMaterialTypes.IronOre:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.PufferFish, 1);
//                    break;
//                case RawMaterial.RawMaterialTypes.Staghorn:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.SpermWhale, 1);
//                    break;
//                case RawMaterial.RawMaterialTypes.SeaWhipCoral:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.Shark, 1);
//                    break;
//                case RawMaterial.RawMaterialTypes.MeteoriteOre:
//                    playerFishMaterialInventory.Add(FishMaterial.FishMaterialTypes.SnailFish, 1);
//                    break;
//            }
//        }
//    }
//}
