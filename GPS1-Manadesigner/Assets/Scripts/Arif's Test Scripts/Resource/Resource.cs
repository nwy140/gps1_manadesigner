using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    [SerializeField] public item itemType;

    public item GetItemType()
    {
        return itemType;
    }
}
