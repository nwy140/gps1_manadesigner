using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class ItemContainer : MonoBehaviour
{
    [FormerlySerializedAs("items")]
    public List<itemSlots> ItemSlots;

    public event Action<itemSlots> OnPointerEnterEvent;
    public event Action<itemSlots> OnPointerExitEvent;
    public event Action<itemSlots> OnLeftClickEvent;
    public event Action<itemSlots> OnBeginDragEvent;
    public event Action<itemSlots> OnEndDragEvent;
    public event Action<itemSlots> OnDragEvent;
    public event Action<itemSlots> OnDropEvent;

    public virtual void Awake()
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            ItemSlots[i].OnPointerEnterEvent += slot => EventHelper(slot, OnPointerEnterEvent);
            ItemSlots[i].OnPointerExitEvent += slot => EventHelper(slot, OnPointerExitEvent);
            ItemSlots[i].OnLeftClickEvent += slot => EventHelper(slot, OnLeftClickEvent);
            ItemSlots[i].OnBeginDragEvent += slot => EventHelper(slot, OnBeginDragEvent);
            ItemSlots[i].OnEndDragEvent += slot => EventHelper(slot, OnEndDragEvent);
            ItemSlots[i].OnDragEvent += slot => EventHelper(slot, OnDragEvent);
            ItemSlots[i].OnDropEvent += slot => EventHelper(slot, OnDropEvent);
        }
    }

    private void EventHelper(itemSlots itemSlot, Action<itemSlots> action)
    {
        if(action != null)
        {
            action(itemSlot);
        }
    }

    public bool CheckIfSlotIsEmpty(item item)
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item == null || (ItemSlots[i].Item.ID == item.ID && ItemSlots[i].Amount < item.maxStack))
            {
                return true;
            }
        }
        //Debug.Log("Cannot Add");
        return false;
    }

    public virtual bool addItem(item item)
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item == null || (ItemSlots[i].Item.ID == item.ID && ItemSlots[i].Amount < item.maxStack))
            {
                ItemSlots[i].Item = item;
                ItemSlots[i].Amount++;
                return true;
            }
        }
        //Debug.Log("Cannot Add");
        return false;
    }

    public bool removeItem(item item)
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item == item)
            {
                ItemSlots[i].Amount--;
                if (ItemSlots[i].Amount == 0)
                {
                    ItemSlots[i].Item = null;
                }
                return true;
            }
        }
        return false;
    }
    public bool discardItem(itemSlots item)
    {
        if(item.Item != null)
        {
            item.Amount = 0;
            item.Item = null;
            return true;
        }
        else
        {
            return false;
        }
    }

    int balance = 0;

    //public bool addItemWithAmount(item item, int amount)
    //{
    //    for (int i = 0; i < ItemSlots.Count; i++)
    //    {
    //        if (ItemSlots[i].Item == null || ItemSlots[i].Item.ID == item.ID)
    //        {
    //            //if(ItemSlots[i].Item.maxStack < amount)
    //            //{
    //            //    ItemSlots[i].Item = item;
    //            //    ItemSlots[i].Amount = ItemSlots[i].Item.maxStack;
    //            //    balance = amount - ItemSlots[i].Item.maxStack;
    //            //}

    //            ItemSlots[i].Item = item;
    //            ItemSlots[i].Amount = amount;

    //            return true;
    //        }
    //    }
    //    return false;
    //}

    int passRemaining = 0;

    public bool removeItemWithAmount(item item, int amount)
    {

        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item == item)
            {
                ItemSlots[i].Amount += passRemaining;

                if (passRemaining > 0)
                {
                    passRemaining = 0;
                }

                if (ItemSlots[i].Amount < amount)
                {
                    passRemaining += ItemSlots[i].Amount;
                }

                ItemSlots[i].Amount -= amount;

                if (ItemSlots[i].Amount <= 0)
                {
                    ItemSlots[i].Item = null;
                }
                //Debug.Log($"Item = {ItemSlots[i].Item}, Amount Remaining = {ItemSlots[i].Amount}");
                return true;
            }
        }
        return false;
    }

    public bool IsFull()
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item == null)
            {
                return false;
            }
        }
        return true;
    }

    public void Clear()
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].Item != null && Application.isPlaying)
            {
                ItemSlots[i].Item.Destroy();
            }
            ItemSlots[i].Item = null;
            ItemSlots[i].Amount = 0;
        }
    }
}
