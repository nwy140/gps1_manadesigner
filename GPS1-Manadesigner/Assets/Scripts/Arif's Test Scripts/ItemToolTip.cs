using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemToolTip : MonoBehaviour
{
    [SerializeField] Text itemNameText;
    [SerializeField] Text itemSlotText;

    public void ShowToolTip(item itemInInventory)
    {
        itemNameText.text = itemInInventory.resourceName;
        itemSlotText.text = itemInInventory.itemDescription;

        //gameObject.SetActive(true);
        OpenText(true);
    }

    public void HideToolTip()
    {
        //gameObject.SetActive(false);
        OpenText(false);
    }

    void OpenText(bool isOpen)
    {
        itemNameText.enabled = isOpen;
        itemSlotText.enabled = isOpen;
    }
}
