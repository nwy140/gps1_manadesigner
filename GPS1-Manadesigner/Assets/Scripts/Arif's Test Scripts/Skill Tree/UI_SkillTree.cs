using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SkillTree : MonoBehaviour
{
    [SerializeField] Button[] turbineButtonArray, weaponButtonArray, hullButtonArray, selfRepairButtonArray, oxygenTankButtonArray, inventoryButtonArray;
    private PlayerSkills playerSkills;
    [SerializeField] private UpgradeHolder upgradeHolder;
    [SerializeField] UpgradeChecker upgradeChecker;
    [SerializeField] storage inventory;
    [SerializeField] ItemSaveManager itemSaveManager;
    [SerializeField] Character character;

    private void Start()
    {
        if (upgradeHolder == null)
        {
            upgradeHolder = FindObjectOfType<UpgradeHolder>();
        }

        if (upgradeChecker == null)
        {
            upgradeChecker = FindObjectOfType<UpgradeChecker>();
        }

        if (inventory == null)
        {
            inventory = FindObjectOfType<storage>();
        }
    }

    #region Delegates

    public delegate void BeginPurchase();
    public BeginPurchase beginPurchase;

    public void StartBuyingUpgrade()
    {
        beginPurchase?.Invoke();

        if (beginPurchase != null)
        {
            upgradeChecker.CheckAllUpgrades();
            inventory.SetInventoryUpgrade(upgradeHolder.InventoryUpgrade, upgradeHolder.InventoryUpgrade.GetStackAmount());
            //itemSaveManager.LoadInventory(character);
        }

        beginPurchase = null;
    }

    //void ReorganiseInventory()
    //{
    //    List<itemSlots> allItemTypes = new List<itemSlots>();

    //    for (int i = 0; i < inventory.GetListOfItemsInInventory().Count; i++)
    //    {
    //        if(allItemTypes.Count <= 0)
    //        {
    //            if(inventory.GetListOfItemsInInventory()[i].Item != null)
    //            {
    //                allItemTypes.Add(inventory.GetListOfItemsInInventory()[i]);
    //            }
    //        }
    //        else
    //        {
    //            for(int j = 0; j < allItemTypes.Count; j++)
    //            {
    //                if(inventory.GetListOfItemsInInventory()[i].Item == allItemTypes[j].Item)
    //                {
    //                    allItemTypes[j].Amount += inventory.GetListOfItemsInInventory()[i].Amount;
    //                }
    //                else
    //                {
    //                    allItemTypes.Add(inventory.GetListOfItemsInInventory()[i]);
    //                }
    //            }
    //        }
    //    }

    //    int balance = 0;

    //    for(int i = 0; i < allItemTypes.Count; i++)
    //    {
    //        if(balance <= allItemTypes[i].Item.maxStack)
    //        {
    //            inventory.addItemWithAmount(allItemTypes[i].Item, balance);
    //            balance = 0;
    //        }
    //        else if (balance > allItemTypes[i].Item.maxStack)
    //        {
    //            inventory.addItemWithAmount(allItemTypes[i].Item, allItemTypes[i].Item.maxStack);
    //            balance -= allItemTypes[i].Item.maxStack;
    //        }

    //        if(balance <= 0)
    //        {
    //            inventory.addItemWithAmount(allItemTypes[i].Item, allItemTypes[i].Amount);
    //        }
    //    }
    //}

    #region Turbine
    public void ChosenLowTierTurbineStage1Button()
    {
        beginPurchase = LowTierTurbineStage1Button;
    }

    public void ChosenLowTierTurbineStage2Button()
    {
        beginPurchase = LowTierTurbineStage2Button;
    }

    public void ChosenHighTierTurbineStage1Button()
    {
        beginPurchase = HighTierTurbineStage1Button;
    }

    public void ChosenHighTierTurbineStage2Button()
    {
        beginPurchase = HighTierTurbineStage2Button;
    }
    #endregion

    #region Weapon
    public void ChosenLowTierWeaponStage1Button()
    {
        beginPurchase = LowTierWeaponStage1Button;
    }

    public void ChosenLowTierWeaponStage2Button()
    {
        beginPurchase = LowTierWeaponStage2Button;
    }

    public void ChosenHighTierWeaponStage1Button()
    {
        beginPurchase = HighTierWeaponStage1Button;
    }

    public void ChosenHighTierWeaponStage2Button()
    {
        beginPurchase = HighTierWeaponStage2Button;
    }
    #endregion

    #region Hull
    public void ChosenLowTierHullStage1Button()
    {
        beginPurchase = LowTierHullStage1Button;
    }

    public void ChosenLowTierHullStage2Button()
    {
        beginPurchase = LowTierHullStage2Button;
    }

    public void ChosenLowTierHullStage3Button()
    {
        beginPurchase = LowTierHullStage3Button;
    }

    public void ChosenHighTierHullStage1Button()
    {
        beginPurchase = HighTierHullStage1Button;
    }

    public void ChosenHighTierHullStage2Button()
    {
        beginPurchase = HighTierHullStage2Button;
    }

    public void ChosenHighTierHullStage3Button()
    {
        beginPurchase = HighTierHullStage3Button;
    }
    #endregion

    #region Self Repair
    public void ChosenLowTierSelfRepairStage1Button()
    {
        beginPurchase = LowTierSelfRepairStage1Button;
    }

    public void ChosenLowTierSelfRepairStage2Button()
    {
        beginPurchase = LowTierSelfRepairStage2Button;
    }

    public void ChosenHighTierSelfRepairStage1Button()
    {
        beginPurchase = HighTierSelfRepairStage1Button;
    }

    public void ChosenHighTierSelfRepairStage2Button()
    {
        beginPurchase = HighTierSelfRepairStage2Button;
    }
    #endregion

    #region Oxygen Tank
    public void ChosenLowTierOxygenTankStage1Button()
    {
        beginPurchase = LowTierOxygenTankStage1Button;
    }

    public void ChosenLowTierOxygenTankStage2Button()
    {
        beginPurchase = LowTierOxygenTankStage2Button;
    }

    public void ChosenHighTierOxygenTankStage1Button()
    {
        beginPurchase = HighTierOxygenTankStage1Button;
    }

    public void ChosenHighTierOxygenTankStage2Button()
    {
        beginPurchase = HighTierOxygenTankStage2Button;
    }
    #endregion

    #region Inventory
    public void ChosenLowTierInventoryStage1Button()
    {
        beginPurchase = LowTierInventoryStage1Button;
    }

    public void ChosenLowTierInventoryStage2Button()
    {
        beginPurchase = LowTierInventoryStage2Button;
    }

    public void ChosenHighTierInventoryStage1Button()
    {
        beginPurchase = HighTierInventoryStage1Button;
    }

    public void ChosenHighTierInventoryStage2Button()
    {
        beginPurchase = HighTierInventoryStage2Button;
    }
    #endregion

    #endregion

    public void SetPlayerSkills(PlayerSkills playerSkills)
    {
        this.playerSkills = playerSkills;
    }

    #region Functions

    #region Turbine
    private void LowTierTurbineStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetTurbineUpgrades()[0]);
        if(!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierTurbineStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            turbineButtonArray[0].interactable = false;
        }
    }

    private void LowTierTurbineStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetTurbineUpgrades()[1]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierTurbineStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            turbineButtonArray[1].interactable = false;
        }
    }

    private void HighTierTurbineStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetTurbineUpgrades()[2]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierTurbineStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            turbineButtonArray[2].interactable = false;
        }
    }

    private void HighTierTurbineStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetTurbineUpgrades()[3]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierTurbineStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            turbineButtonArray[3].interactable = false;
        }
    }
    #endregion

    #region Weapon
    private void LowTierWeaponStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetWeaponUpgrades()[0]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierWeaponStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            weaponButtonArray[0].interactable = false;
        }
    }

    private void LowTierWeaponStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetWeaponUpgrades()[1]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierWeaponStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            weaponButtonArray[1].interactable = false;
        }
    }

    private void HighTierWeaponStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetWeaponUpgrades()[2]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierWeaponStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            weaponButtonArray[2].interactable = false;
        }
    }

    private void HighTierWeaponStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetWeaponUpgrades()[3]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierWeaponStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            weaponButtonArray[3].interactable = false;
        }
    }
    #endregion

    #region Hull
    private void LowTierHullStage1Button()
    {
        if (playerSkills == null) { Debug.Log("there is no player skill"); }

        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetHullUpgrades()[0]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierHullStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            hullButtonArray[0].interactable = false;
        }
    }

    private void LowTierHullStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetHullUpgrades()[1]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierHullStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            hullButtonArray[1].interactable = false;
        }
    }

    private void LowTierHullStage3Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetHullUpgrades()[2]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierHullStage3))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            hullButtonArray[2].interactable = false;
        }
    }

    private void HighTierHullStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetHullUpgrades()[3]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierHullStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            hullButtonArray[3].interactable = false;
        }
    }

    private void HighTierHullStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetHullUpgrades()[4]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierHullStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            hullButtonArray[4].interactable = false;
        }
    }

    private void HighTierHullStage3Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetHullUpgrades()[5]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierHullStage3))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            hullButtonArray[5].interactable = false;
        }
    }
    #endregion

    #region Self Repair
    private void LowTierSelfRepairStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetSelfRepairUpgrades()[0]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierSelfRepairStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            selfRepairButtonArray[0].interactable = false;
        }
    }

    private void LowTierSelfRepairStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetSelfRepairUpgrades()[1]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierSelfRepairStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            selfRepairButtonArray[1].interactable = false;
        }
    }

    private void HighTierSelfRepairStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetSelfRepairUpgrades()[2]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierSelfRepairStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            selfRepairButtonArray[2].interactable = false;
        }
    }

    private void HighTierSelfRepairStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetSelfRepairUpgrades()[3]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierSelfRepairStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            selfRepairButtonArray[3].interactable = false;
        }
    }
    #endregion

    #region Oxygen Tank
    private void LowTierOxygenTankStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetOxygenTankUpgrades()[0]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierOxygenTankStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            oxygenTankButtonArray[0].interactable = false;
        }
    }

    private void LowTierOxygenTankStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetOxygenTankUpgrades()[1]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierOxygenTankStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            oxygenTankButtonArray[1].interactable = false;
        }
    }

    private void HighTierOxygenTankStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetOxygenTankUpgrades()[2]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierOxygenTankStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            oxygenTankButtonArray[2].interactable = false;
        }
    }

    private void HighTierOxygenTankStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetOxygenTankUpgrades()[3]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierOxygenTankStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            oxygenTankButtonArray[3].interactable = false;
        }
    }
    #endregion

    #region Inventory
    private void LowTierInventoryStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetInventoryUpgrades()[0]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierInventoryStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            inventoryButtonArray[0].interactable = false;
        }
    }

    private void LowTierInventoryStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetInventoryUpgrades()[1]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.LowTierInventoryStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            inventoryButtonArray[1].interactable = false;
        }
    }

    private void HighTierInventoryStage1Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetInventoryUpgrades()[2]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierInventoryStage1))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            inventoryButtonArray[2].interactable = false;
        }
    }

    private void HighTierInventoryStage2Button()
    {
        playerSkills.SetRequirements(upgradeHolder.GetStorage(), upgradeHolder.GetInventoryUpgrades()[3]);
        if (!playerSkills.TryUnlockSkill(PlayerSkills.SkillType.HighTierInventoryStage2))
        {
            Debug.Log("Cannot Unlock");
        }
        else
        {
            inventoryButtonArray[3].interactable = false;
        }
    }
    #endregion

    #endregion
}
