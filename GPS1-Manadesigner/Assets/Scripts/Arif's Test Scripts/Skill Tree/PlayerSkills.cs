using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkills
{
    public event EventHandler<OnSkillUnlockedEventArgs> OnSkillUnlocked;
    public class OnSkillUnlockedEventArgs : EventArgs
    {
        public SkillType skillType;
    }

    public enum SkillType
    {
        None,

        // Turbine
        LowTierTurbineStage1,
        LowTierTurbineStage2,
        HighTierTurbineStage1,
        HighTierTurbineStage2,

        // Weapon
        LowTierWeaponStage1,
        LowTierWeaponStage2,
        HighTierWeaponStage1,
        HighTierWeaponStage2,

        // Hull
        LowTierHullStage1,
        LowTierHullStage2,
        LowTierHullStage3,
        HighTierHullStage1,
        HighTierHullStage2,
        HighTierHullStage3,

        // Self Repair
        LowTierSelfRepairStage1,
        LowTierSelfRepairStage2,
        HighTierSelfRepairStage1,
        HighTierSelfRepairStage2,

        // Oxygen Tank
        LowTierOxygenTankStage1,
        LowTierOxygenTankStage2,
        HighTierOxygenTankStage1,
        HighTierOxygenTankStage2,

        // Inventory
        LowTierInventoryStage1,
        LowTierInventoryStage2,
        HighTierInventoryStage1,
        HighTierInventoryStage2
    }

    private List<SkillType> unlockedSkillTypeList;

    private int skillPoints;
    item[] fishR; item[] rawR; int[] fishNumR = { 1, 1 }; int[] rawNumR = { 1 }; storage inv; PlayerUpgrades upg;
    readonly List<item> itemsToRemove = new List<item>();

    public static Action OnSetRequirementForPopup;

    public PlayerSkills()
    {
        unlockedSkillTypeList = new List<SkillType>();
    }

    public void GetCurrentUpgrades(UpgradeHolder upgradeHolder)
    {
        #region Turbine
        if (upgradeHolder.TurbineUpgrade == upgradeHolder.GetTurbineUpgrades()[0])
        {
            unlockedSkillTypeList.Add(SkillType.LowTierTurbineStage1);
        }
        else if (upgradeHolder.TurbineUpgrade == upgradeHolder.GetTurbineUpgrades()[2])
        {
            unlockedSkillTypeList.Add(SkillType.HighTierTurbineStage1);
        }
        #endregion

        #region Hull
        if (upgradeHolder.HullUpgrade == upgradeHolder.GetHullUpgrades()[0])
        {
            unlockedSkillTypeList.Add(SkillType.LowTierHullStage1);
        }
        else if (upgradeHolder.HullUpgrade == upgradeHolder.GetHullUpgrades()[1])
        {
            unlockedSkillTypeList.Add(SkillType.LowTierHullStage2);
        }
        else if (upgradeHolder.HullUpgrade == upgradeHolder.GetHullUpgrades()[2])
        {
            unlockedSkillTypeList.Add(SkillType.HighTierHullStage1);
        }
        else if (upgradeHolder.HullUpgrade == upgradeHolder.GetHullUpgrades()[3])
        {
            unlockedSkillTypeList.Add(SkillType.HighTierHullStage2);
        }
        #endregion

        #region Oxygen Tank
        if (upgradeHolder.OxygenTankUpgrade == upgradeHolder.GetOxygenTankUpgrades()[0])
        {
            unlockedSkillTypeList.Add(SkillType.LowTierOxygenTankStage1);
        }
        else if (upgradeHolder.OxygenTankUpgrade == upgradeHolder.GetOxygenTankUpgrades()[2])
        {
            unlockedSkillTypeList.Add(SkillType.HighTierOxygenTankStage1);
        }
        #endregion

        #region Self Repair
        if (upgradeHolder.SelfRepairUpgrade == upgradeHolder.GetSelfRepairUpgrades()[0])
        {
            unlockedSkillTypeList.Add(SkillType.LowTierSelfRepairStage1);
        }
        else if (upgradeHolder.SelfRepairUpgrade == upgradeHolder.GetSelfRepairUpgrades()[2])
        {
            unlockedSkillTypeList.Add(SkillType.HighTierSelfRepairStage1);
        }
        #endregion

        #region Inventory
        if (upgradeHolder.InventoryUpgrade == upgradeHolder.GetInventoryUpgrades()[0])
        {
            unlockedSkillTypeList.Add(SkillType.LowTierInventoryStage1);
        }
        else if (upgradeHolder.InventoryUpgrade == upgradeHolder.GetInventoryUpgrades()[2])
        {
            unlockedSkillTypeList.Add(SkillType.HighTierInventoryStage1);
        }
        #endregion

        #region Weapon
        if (upgradeHolder.WeaponUpgrade == upgradeHolder.GetWeaponUpgrades()[0])
        {
            unlockedSkillTypeList.Add(SkillType.LowTierWeaponStage1);
        }
        else if (upgradeHolder.WeaponUpgrade == upgradeHolder.GetWeaponUpgrades()[2])
        {
            unlockedSkillTypeList.Add(SkillType.HighTierWeaponStage1);
        }
        #endregion
    }

    public void SetRequirements(storage inv, PlayerUpgrades upg)
    {
        if(inv == null)
        {
            Debug.Log("there is no inventory");
        }

        if (upg == null)
        {
            Debug.Log("there is no upgrade");
        }

        this.inv = inv;
        this.upg = upg;

        this.fishR = upg.GetFishMaterialTypeRequirement();
        this.rawR = upg.GetRawMaterialTypeRequirement();
        this.fishNumR = upg.GetFishMaterialAmountRequirement();
        this.rawNumR = upg.GetRawMaterialAmountRequirement();
    }

    public bool IsRequirementMet()
    {
        List<int> fishMaterialIndex = new List<int>();
        List<int> rawMaterialIndex = new List<int>();
        bool fishCheck = true;
        bool rawCheck = true;

        for(int i = 0; i < fishNumR.Length; i++)
        {
            fishMaterialIndex.Add(0);
        }

        for (int i = 0; i < rawNumR.Length; i++)
        {
            rawMaterialIndex.Add(0);
        }

        foreach (itemSlots usedItemSlots in inv.GetListOfItemsInInventory())
        {
            //Debug.Log($"items in inventory length: {inv.GetListOfItemsInInventory().Count}");

            for (int i = 0; i < fishNumR.Length; i++)
            {
                //Debug.Log($"fish amount requirement: {fishNumR.Length}");
                if (fishMaterialIndex[i] < fishNumR[i])
                {
                    if (usedItemSlots.Item == fishR[i])
                    {
                        fishMaterialIndex[i] += usedItemSlots.Amount;
                        itemsToRemove.Add(usedItemSlots.Item);
                    }
                }

                //if (usedItemSlots.Item == fishR[i])
                //{
                //    if (fishMaterialIndex[i] < fishNumR[i])
                //    {
                //        itemsToRemove.Add(usedItemSlots.Item);
                //    }
                //    Debug.Log($"found item slot with required item: {usedItemSlots.Item}");
                //    fishMaterialIndex[i] += usedItemSlots.Amount;
                //    Debug.Log($"fish material index {fishMaterialIndex[i]}");
                //}
            }

            for (int i = 0; i < rawNumR.Length; i++)
            {
                if (rawMaterialIndex[i] < rawNumR[i])
                {
                    if (usedItemSlots.Item == rawR[i])
                    {
                        rawMaterialIndex[i] += usedItemSlots.Amount;
                        //if(usedItemSlots.Amount < rawNumR[i])
                        //{
                        //    rawNumR[i] -= usedItemSlots.Amount;
                        //}
                        Debug.Log($"Used Item Slots: {usedItemSlots.Item}, {usedItemSlots.Amount}\tItem Requirement: {rawR[i]}, {rawNumR[i]}");
                        itemsToRemove.Add(usedItemSlots.Item);
                    }
                }

                //if (usedItemSlots.Item == rawR[i])
                //{
                //    if (rawMaterialIndex[i] < rawNumR[i])
                //    {
                //        itemsToRemove.Add(usedItemSlots.Item);
                //    }
                //    rawMaterialIndex[i] += usedItemSlots.Amount;
                //    //Debug.Log($"raw material index {rawMaterialIndex[i]}");
                //    Debug.Log($"Used Item Slots: {usedItemSlots.Item}, {usedItemSlots.Amount}");
                //}
            }
        }

        //for (int i = 0; i < fishNumR.Length; i++)
        //{
        //    Debug.Log($"fish material index {fishMaterialIndex[i]}");
        //}
        //for (int i = 0; i < rawNumR.Length; i++)
        //{
        //    Debug.Log($"raw material index {rawMaterialIndex[i]}");
        //}

        for (int i = 0; i < fishMaterialIndex.Count; i++)
        {
            if (fishMaterialIndex[i] >= fishNumR[i])
            {
                fishMaterialIndex[i] = 0;
            }
            else
            {
                fishCheck = false;
            }
        }

        for (int i = 0; i < rawMaterialIndex.Count; i++)
        {
            if (rawMaterialIndex[i] >= rawNumR[i])
            {
                rawMaterialIndex[i] = 0;
            }
            else
            {
                rawCheck = false;
            }
        }

        if (rawCheck && fishCheck)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RemoveAllUsedResources()
    {
        //Debug.Log("Items to remove count = " + itemsToRemove.Count);
        //Debug.Log("Fish number requirement = " + fishNumR.Length);
        //Debug.Log("Raw number requirement = " + rawNumR.Length);

        for (int i = 0; i < itemsToRemove.Count; i++)
        {
            for (int j = 0; j < fishR.Length; j++)
            {
                if (itemsToRemove[i].name == fishR[j].name)
                {
                    //Debug.Log("Fish mount to deduct = " + fishNumR[j]);
                    inv.removeItemWithAmount(itemsToRemove[i], fishNumR[j]);
                }
            }
        }

        for (int i = 0; i < itemsToRemove.Count; i++)
        {
            for (int j = 0; j < rawR.Length; j++)
            {
                if (itemsToRemove[i].name == rawR[j].name)
                {
                    //Debug.Log("Raw mount to deduct = " + rawNumR[j]);
                    inv.removeItemWithAmount(itemsToRemove[i], rawNumR[j]);
                }
            }
        }

        itemsToRemove.Clear();
    }

    public int GetSkillPoints()
    {
        return skillPoints;
    }

    private void UnlockSkill(SkillType skillType)
    {
        if(!IsSkillUnlocked(skillType))
        {
            unlockedSkillTypeList.Add(skillType);
            OnSkillUnlocked?.Invoke(this, new OnSkillUnlockedEventArgs { skillType = skillType });
        }
    }

    public bool IsSkillUnlocked(SkillType skillType)
    {
        return unlockedSkillTypeList.Contains(skillType);
    }

    public bool CanUnlock(SkillType skillType)
    {
        SkillType skillRequirement = GetSkillRequirement(skillType);

        if (skillRequirement != SkillType.None)
        {
            if (IsSkillUnlocked(skillRequirement))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    public SkillType GetSkillRequirement(SkillType skillType)
    {
        switch(skillType)
        {
            // Turbine
            case SkillType.LowTierTurbineStage2: return SkillType.LowTierTurbineStage1;
            case SkillType.HighTierTurbineStage2: return SkillType.HighTierTurbineStage1;

            // Weapon
            case SkillType.LowTierWeaponStage2: return SkillType.LowTierWeaponStage1;
            case SkillType.HighTierWeaponStage2: return SkillType.HighTierWeaponStage1;

            // Hull
            case SkillType.LowTierHullStage2: return SkillType.LowTierHullStage1;
            case SkillType.HighTierHullStage2: return SkillType.HighTierHullStage1;
            case SkillType.LowTierHullStage3: return SkillType.LowTierHullStage2;
            case SkillType.HighTierHullStage3: return SkillType.HighTierHullStage2;

            // Self Repair
            case SkillType.LowTierSelfRepairStage2: return SkillType.LowTierSelfRepairStage1;
            case SkillType.HighTierSelfRepairStage2: return SkillType.HighTierSelfRepairStage1;

            // Oxygen Tank
            case SkillType.LowTierOxygenTankStage2: return SkillType.LowTierOxygenTankStage1;
            case SkillType.HighTierOxygenTankStage2: return SkillType.HighTierOxygenTankStage1;

            // Inventory
            case SkillType.LowTierInventoryStage2: return SkillType.LowTierInventoryStage1;
            case SkillType.HighTierInventoryStage2: return SkillType.HighTierInventoryStage1;
        }
        return SkillType.None;
    }

    public bool TryUnlockSkill(SkillType skillType)
    {
        if (CanUnlock(skillType))
        {
            if (IsRequirementMet())
            {
                RemoveAllUsedResources();
                UnlockSkill(skillType);
                return true;
            }
            else
            {
                //Debug.Log("Requirement not met");
                return false;
            }
        }
        else
        {
            //Debug.Log("Cannot unlock at all");
            return false;
        }
    }
}
