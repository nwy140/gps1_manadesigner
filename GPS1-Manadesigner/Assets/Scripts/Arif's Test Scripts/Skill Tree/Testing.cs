using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    //[SerializeField] private Player player;
    [SerializeField] private UpgradeHolder upgradeHolder;
    [SerializeField] private UI_SkillTree uiSkillTree;

    // Start is called before the first frame update
    void Start()
    {
        uiSkillTree.SetPlayerSkills(upgradeHolder.GetPlayerSkills());
    }
}
