using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "Upgrades/Inventory", order = 4)]
public class Inventory : PlayerUpgrades
{
    [SerializeField] int numberOfSlots;
    [Range(1, 999)]
    [SerializeField] int maxStacking;

    public int GetNumberOfSlots()
    {
        return numberOfSlots;
    }

    public int GetStackAmount()
    {
        return maxStacking;
    }
}
