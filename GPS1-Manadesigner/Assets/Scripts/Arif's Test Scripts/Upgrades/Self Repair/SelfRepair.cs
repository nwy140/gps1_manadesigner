using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Self Repair", menuName = "Upgrades/Self Repair", order = 3)]
public class SelfRepair : PlayerUpgrades
{
    [Header("Base")]
    [SerializeField] float healthRegenerationAmount;
    [SerializeField] bool canRegenerate = false;

    [Space]

    [Header("Regeneration")]
    [SerializeField] float healthRegenerationRate;

    [Space]

    [Header("Kits")]
    [SerializeField] int availableHealthKits;
    [SerializeField] float healthReplenishAmount;

    public float GetHealthRegenerationAmount()
    {
        return healthRegenerationAmount;
    }

    public float GetHealthRegenerationRate()
    {
        return healthRegenerationRate;
    }

    public int GetAvailableHealthKits()
    {
        return availableHealthKits;
    }

    public float GetHealthReplenishPercentage()
    {
        return healthReplenishAmount;
    }
}
