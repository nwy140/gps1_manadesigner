using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Turbine", menuName = "Upgrades/Turbine", order = 2)]
public class Turbine : PlayerUpgrades
{
    [SerializeField] float turbineSpeed;

    public float GetTurbineSpeed()
    {
        return turbineSpeed;
    }
}