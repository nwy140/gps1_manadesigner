using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Oxygen Tank", menuName = "Upgrades/Oxygen Tank", order = 5)]
public class OxygenTank : PlayerUpgrades
{
    [Header("Standard")]
    [SerializeField] float oxygenSupplyAmount;
    [SerializeField] readonly float[] oxygenDepletionRate = { 3, 2, 1 };

    [Header("Unique Feature")]
    [SerializeField] float defaultOxygenDepletionRate = 3;
    [SerializeField] bool isUsingLowTechUniqueFeature = false;

    public float GetOxygenSupplyAmount()
    {
        return oxygenSupplyAmount;
    }

    public float GetDefaultOxygenDepletionRate()
    {
        return defaultOxygenDepletionRate;
    }

    public float GetOxygenDepletionRate(int stage)
    {
        return oxygenDepletionRate[stage];
    }
}