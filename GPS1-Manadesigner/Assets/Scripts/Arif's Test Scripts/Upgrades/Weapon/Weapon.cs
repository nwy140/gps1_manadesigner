using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Upgrades/Weapon", order = 5)]
public class Weapon : PlayerUpgrades
{
    [SerializeField] PlayerBullet playerBullet;
    [SerializeField] float attackRate;
    [SerializeField] float attackDamage;
    [SerializeField] float attackSpeed;

    public float GetAttackRate()
    {
        return attackRate;
    }

    public float GetAttackDamage()
    {
        return attackDamage;
    }

    public float GetAttackSpeed()
    {
        return attackSpeed;
    }

    public PlayerBullet GetPlayerBullet()
    {
        return playerBullet;
    }    
}
