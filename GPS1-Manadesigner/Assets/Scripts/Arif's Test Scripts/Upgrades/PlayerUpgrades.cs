using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUpgrades : ScriptableObject
{
    [SerializeField] item[] fishMaterialTypeRequirement;
    [SerializeField] item[] rawMaterialTypeRequirement;
    [SerializeField] int[] fishMaterialAmountRequirement;
    [SerializeField] int[] rawMaterialAmountRequirement;

    [SerializeField] protected bool isUniqueFeatureUsed;
    [SerializeField] protected bool isLowTierUpgrade;

    public string ID;

    public item[] GetFishMaterialTypeRequirement()
    {
        return fishMaterialTypeRequirement;
    }
    public item[] GetRawMaterialTypeRequirement()
    {
        return rawMaterialTypeRequirement;
    }

    public int[] GetFishMaterialAmountRequirement()
    {
        return fishMaterialAmountRequirement;
    }

    public int[] GetRawMaterialAmountRequirement()
    {
        return rawMaterialAmountRequirement;
    }

    public bool IsUniqueFeatureUsed()
    {
        return isUniqueFeatureUsed;
    }

    public bool IsLowTierUpgrade()
    {
        return isLowTierUpgrade;
    }

    public virtual PlayerUpgrades GetCopy()
    {
        return this;
    }
}