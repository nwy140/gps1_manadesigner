using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hull", menuName = "Upgrades/Hull", order = 1)]
public class Hull : PlayerUpgrades
{
    [SerializeField] float health;
    [SerializeField] float pressureStrength = 1;

    public float GetHealth()
    {
        return health;
    }

    public float GetPressureStrength()
    {
        return pressureStrength;
    }
}
