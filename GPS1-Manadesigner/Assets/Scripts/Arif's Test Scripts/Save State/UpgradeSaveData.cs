using System;
using System.Collections.Generic;

[Serializable]
public class UpgradeSaveData
{
    public string UpgradeID;

    public UpgradeSaveData(string id)
    {
        UpgradeID = id;
    }
}

[Serializable]
public class InstalledUpgradeSaveData
{
    public List<UpgradeSaveData> installedUpgrade;

    public InstalledUpgradeSaveData()
    {
        installedUpgrade = new List<UpgradeSaveData>();
    }
}


