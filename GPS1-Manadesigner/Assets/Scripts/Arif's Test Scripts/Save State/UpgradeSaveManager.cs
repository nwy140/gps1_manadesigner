using System.Collections.Generic;
using UnityEngine;

public class UpgradeSaveManager : MonoBehaviour
{
    [SerializeField] UpgradeDatabase upgradeDatabase;

    private const string UpgradeFileName = "Upgrade";

    public void LoadUpgrade(UpgradeHolder upgradeHolder)
    {
        InstalledUpgradeSaveData savedInstalledUpgrade = UpgradeSaveIO.LoadUpgrades(UpgradeFileName);

        if (savedInstalledUpgrade == null || savedInstalledUpgrade.installedUpgrade.Count <= 0) { return; }

        UpgradeSaveData savedTurbineUpgrade = savedInstalledUpgrade.installedUpgrade[0];
        UpgradeSaveData savedWeaponUpgrade = savedInstalledUpgrade.installedUpgrade[1];
        UpgradeSaveData savedHullUpgrade = savedInstalledUpgrade.installedUpgrade[2];
        UpgradeSaveData savedSelfRepairUpgrade = savedInstalledUpgrade.installedUpgrade[3];
        UpgradeSaveData savedOxygenTankUpgrade = savedInstalledUpgrade.installedUpgrade[4];
        UpgradeSaveData savedInventoryUpgrade = savedInstalledUpgrade.installedUpgrade[5];

        upgradeHolder.TurbineUpgrade = upgradeDatabase.GetUpgradeCopy(savedTurbineUpgrade.UpgradeID) as Turbine;
        upgradeHolder.WeaponUpgrade = upgradeDatabase.GetUpgradeCopy(savedWeaponUpgrade.UpgradeID) as Weapon;
        upgradeHolder.HullUpgrade = upgradeDatabase.GetUpgradeCopy(savedHullUpgrade.UpgradeID) as Hull;
        upgradeHolder.SelfRepairUpgrade = upgradeDatabase.GetUpgradeCopy(savedSelfRepairUpgrade.UpgradeID) as SelfRepair;
        upgradeHolder.OxygenTankUpgrade = upgradeDatabase.GetUpgradeCopy(savedOxygenTankUpgrade.UpgradeID) as OxygenTank;
        upgradeHolder.InventoryUpgrade = upgradeDatabase.GetUpgradeCopy(savedInventoryUpgrade.UpgradeID) as Inventory;
    }

    public void SaveUpgrade(UpgradeHolder upgradeHolder)
    {
        SaveUpgrades(upgradeHolder.TurbineUpgrade, upgradeHolder.WeaponUpgrade, upgradeHolder.HullUpgrade, upgradeHolder.SelfRepairUpgrade, upgradeHolder.OxygenTankUpgrade, upgradeHolder.InventoryUpgrade, UpgradeFileName); ;
    }

    private void SaveUpgrades(Turbine turbine, Weapon weapon, Hull hull, SelfRepair selfRepair, OxygenTank oxygenTank, Inventory inventory, string filename)
    {
        var saveData = new InstalledUpgradeSaveData();

        saveData.installedUpgrade.Add(new UpgradeSaveData(turbine.ID));
        saveData.installedUpgrade.Add(new UpgradeSaveData(weapon.ID));
        saveData.installedUpgrade.Add(new UpgradeSaveData(hull.ID));
        saveData.installedUpgrade.Add(new UpgradeSaveData(selfRepair.ID));
        saveData.installedUpgrade.Add(new UpgradeSaveData(oxygenTank.ID));
        saveData.installedUpgrade.Add(new UpgradeSaveData(inventory.ID));

        UpgradeSaveIO.SaveUpgrades(saveData, filename);
    }
}
