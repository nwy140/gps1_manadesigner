using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveHandler : MonoBehaviour
{
    //Player player;
    storage storage;
    itemSlots itemSlots;

    private void Start()
    {
        //player = FindObjectOfType<Player>();
        //storage = player.GetStorage();

        Load();
    }

    public void Save()
    {
        SaveObject saveObject = new SaveObject
        {
            //turbineUpgrade = player.GetTurbineUpgrade(),
    };

        string json = JsonUtility.ToJson(saveObject);

        File.WriteAllText(Application.dataPath + "/save.txt", json);
    }

    public void Load()
    {
        if(File.Exists(Application.dataPath + "/save.txt"))
        {
            string saveString = File.ReadAllText(Application.dataPath + "/save.txt");

            SaveObject saveObject = JsonUtility.FromJson<SaveObject>(saveString);

            //player.SetTurbineUpgrade(saveObject.turbineUpgrade);
        }
    }

    private class SaveObject
    {
        public Turbine turbineUpgrade;
    }
}
