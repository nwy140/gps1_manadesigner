using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu]
public class UpgradeDatabase : ScriptableObject
{
    [SerializeField] PlayerUpgrades[] playerUpgrades;

    public PlayerUpgrades GetUpgradeReference(string upgradeID)
    {
        foreach (PlayerUpgrades upgrade in playerUpgrades)
        {
            if (upgrade.ID == upgradeID)
            {
                return upgrade;
            }
        }
        return null;
    }

    public PlayerUpgrades GetUpgradeCopy(string upgradeID)
    {
        PlayerUpgrades upgrade = GetUpgradeReference(upgradeID);
        if (upgrade == null) return null;
        return upgrade.GetCopy();
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        LoadUpgrades();
    }

    private void OnEnable()
    {
        EditorApplication.projectChanged += LoadUpgrades;
    }

    private void OnDisable()
    {
        EditorApplication.projectChanged -= LoadUpgrades;
    }

    private void LoadUpgrades()
    {
        playerUpgrades = FindAssetByType<PlayerUpgrades>("Assets/Upgrade SO");
    }

    public static T[] FindAssetByType<T>(params string[] folders) where T : UnityEngine.Object
    {
        string type = typeof(T).ToString().Replace("UnityEngine.", "");

        string[] guids;
        if (folders == null || folders.Length == 0)
        {
            guids = AssetDatabase.FindAssets("t:" + type);
        }
        else
        {
            guids = AssetDatabase.FindAssets("t:" + type, folders);
        }

        T[] assets = new T[guids.Length];

        for(int i = 0; i < guids.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            assets[i] = AssetDatabase.LoadAssetAtPath<T>(assetPath);
        }

        return assets;
    }
#endif
}
