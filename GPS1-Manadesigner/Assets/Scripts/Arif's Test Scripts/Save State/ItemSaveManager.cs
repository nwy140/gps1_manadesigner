using System.Collections.Generic;
using UnityEngine;

public class ItemSaveManager : MonoBehaviour
{
    [SerializeField] ItemDatabase itemDatabase;

    private const string InventoryFileName = "Inventory";
    //private const string EquipmentFileName = "Equimpent";

    public void LoadInventory(Character character)
    {
        ItemContainerSaveData savedSlots = ItemSaveIO.LoadItems(InventoryFileName);
        if (savedSlots == null || character.Storage.ItemSlots.Count < savedSlots.SavedSlots.Length) return;

        character.Storage.Clear(); // may cause

        for(int i = 0; i < savedSlots.SavedSlots.Length; i++)
        {
            itemSlots itemSlot = character.Storage.ItemSlots[i];
            ItemSlotSaveData savedSlot = savedSlots.SavedSlots[i];

            if (itemSlot != null)
            {
                if (savedSlot == null)
                {
                    itemSlot.Item = null;
                    itemSlot.Amount = 0;
                }
                else
                {
                    itemSlot.Item = itemDatabase.GetItemCopy(savedSlot.ItemID);
                    itemSlot.Amount = savedSlot.Amount;
                    Debug.Log("Added " + savedSlots.SavedSlots[i].ItemID);
                }
            }
            else
            {
                break;
            }
        }
    }

    public void SaveInventory(Character character)
    {
        Debug.Log("Saved");
        SaveItems(character.Storage.ItemSlots, InventoryFileName);
    }

    private void SaveItems(IList<itemSlots> itemSlots, string fileName)
    {
        var saveData = new ItemContainerSaveData(itemSlots.Count);

        for(int i = 0; i < saveData.SavedSlots.Length; i++)
        {
            itemSlots itemSlot = itemSlots[i];

            if(itemSlot.Item == null)
            {
                saveData.SavedSlots[i] = null;
            }
            else
            {
                saveData.SavedSlots[i] = new ItemSlotSaveData(itemSlot.Item.ID, itemSlot.Amount);
                Debug.Log($"Saved  {saveData.SavedSlots[i].ItemID}, Amount {saveData.SavedSlots[i].ItemID}");
            }
        }

        ItemSaveIO.SaveItems(saveData, fileName);
    }
}
