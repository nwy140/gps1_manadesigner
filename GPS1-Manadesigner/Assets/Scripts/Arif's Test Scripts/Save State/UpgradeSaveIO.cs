using UnityEngine;

public static class UpgradeSaveIO
{
    private static readonly string baseSavePath;

    static UpgradeSaveIO()
    {
        baseSavePath = Application.persistentDataPath;
    }

    public static void SaveUpgrades(InstalledUpgradeSaveData upgrade, string filename)
    {
        FileReadWrite.WriteToBinaryFile(baseSavePath + "/" + filename + ".dat", upgrade);
    }

    public static InstalledUpgradeSaveData LoadUpgrades(string filename)
    {
        string filePath = baseSavePath + "/" + filename + ".dat";

        if(System.IO.File.Exists(filePath))
        {
            return FileReadWrite.ReadFromBinaryFile<InstalledUpgradeSaveData>(filePath);
        }

        return null;
    }
}
