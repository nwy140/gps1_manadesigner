using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public MeterTank oxygen;
    public Slider health;

    void Update()
    {
        if (oxygen.tankFill.value == 0)
        {
            InvokeRepeating("HealthDeplate", 3.0f, 0f);

            //if (health.value == 0)
            //{
            //    Debug.Log("Game Over...");
            //}
        }  
    }

    public void HealthDeplate()
    {
        health.value -= 0.01f;
    }
}
