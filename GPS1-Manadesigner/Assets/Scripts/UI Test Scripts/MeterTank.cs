using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeterTank : MonoBehaviour
{
    public Slider tankFill;

    private void Start()
    {
        InvokeRepeating("TankFall", 3.0f, 3.0f);
    }

    public void TankFall()
    {
        tankFill.value -= 1f;
    }
}
