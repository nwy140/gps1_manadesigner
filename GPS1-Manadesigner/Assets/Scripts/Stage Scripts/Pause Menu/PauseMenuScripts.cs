using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PauseMenuScripts : MonoBehaviour
{
    public GameObject pauseMenu;
    public bool isPaused;
    public bool isPauseMenuOn;
    public Character character;

    public GameObject[] objectsToDisable;

    private void Awake()
    {
        if (character == null)
        {
            character = FindObjectOfType<Character>();
        }
    }
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    isPaused = !isPaused;
        //}

        //if(pauseMenu.activeInHierarchy)
        //{
        //    animator.SetBool("IsPause", true);
        //}

        //if (isInventoryPause)
        //{
        //    Pause();
        //    Time.timeScale = 0;
        //}
        //else
        //{
        //    Unpause();
        //    Time.timeScale = 1;
        //}


        if (isPauseMenuOn && isPaused)
        {
            pauseMenu.SetActive(true);
        }
        else
        {
            pauseMenu.SetActive(false);
        }

        if (isPaused)
        {
            OpenPauseMenu();
        }
        else
        {
            ClosePauseMenu();
            //reset animator Ref : https://answers.unity.com/questions/1490688/how-to-restart-animator-controller.html  
            //animator.Rebind();
        }
        if (DayCounter.instance != null)
        {
            if (DayCounter.instance.days > 14)
            {
                var eventScreenObj = GameObject.FindGameObjectWithTag("EventScreen");
                if (eventScreenObj != null)
                {
                    eventScreenObj.SetActive(true);
                }
            }
        }
    }

    public void DisablePause()
    {
        pauseMenu.SetActive(false);
    }

    public void OpenPauseMenu()
    {
        Time.timeScale = 0;
        //pauseMenu.SetActive(true);
        isPaused = true;
    }

    public UnityEvent onUnPause;

    public void ClosePauseMenu()
    {
        Time.timeScale = 1;
        foreach (GameObject chosenObject in objectsToDisable)
        {
            chosenObject.SetActive(false);
        }
        isPauseMenuOn = false;
        isPaused = false;
        onUnPause.Invoke();
    }

    public void QuitGame()
    {
        ClosePauseMenu();
        Debug.Log("Returning to main...");
        SceneManager.LoadScene("MainMenu");
    }

    public void OnInput(InputAction.CallbackContext context)
    {
        if (context.action.triggered)
        {
            if (character.gameObject.activeInHierarchy) { return; }
            isPauseMenuOn = !isPauseMenuOn;
            isPaused = !isPaused;
        }
    }

    //IEnumerator PauseInvert()
    //{
    //    yield return new WaitForSecondsRealtime(1f);
    //    isPaused = !isPaused;
    //}

}
