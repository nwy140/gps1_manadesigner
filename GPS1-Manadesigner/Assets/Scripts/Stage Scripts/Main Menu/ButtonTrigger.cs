using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class ButtonTrigger : MonoBehaviour
{
    public void StartTutorial()
    {
        PlayerPrefs.DeleteKey("Day");
        DayCounter.instance.days = 0;
        PlayerPrefs.SetFloat("health", -1);
        PlayerPrefs.SetFloat("oxygenSupply", -1);
        PlayerPrefs.SetInt("healthKits", -1);
        SceneManager.LoadScene("Intro");
    }

    public void SkipTutorial()
    {
        PlayerPrefs.DeleteKey("Day");
        DayCounter.instance.days = 0;
        PlayerPrefs.SetFloat("health", -1);
        PlayerPrefs.SetFloat("oxygenSupply", -1);
        PlayerPrefs.SetInt("healthKits", -1);
        SceneManager.LoadScene("Stage Prototype");
    }
    
    public void ContinueGame()
    {
        DayCounter.instance.days = 0;
        PlayerPrefs.SetFloat("health", -1);
        PlayerPrefs.SetFloat("oxygenSupply", -1);
        PlayerPrefs.SetInt("healthKits", -1);
        SceneManager.LoadScene("Stage Prototype");
    }

    public void ExitButton()
    {
        Debug.Log("Game quit...");
        Application.Quit();
    }

    public void DeletePersistentPathDirAndPlayerPrefs()
    {
        // Ref: https://answers.unity.com/questions/1619142/delete-all-save-files-in-persistentdatapth.html
        string[] filePaths = Directory.GetFiles(Application.persistentDataPath, "*.dat*");
        foreach (string filePath in filePaths)
        {
            File.Delete(filePath);
        }

        PlayerPrefs.SetFloat("health", -1);
        PlayerPrefs.SetFloat("oxygenSupply", -1);
        PlayerPrefs.SetInt("healthKits", -1);
    }
}
