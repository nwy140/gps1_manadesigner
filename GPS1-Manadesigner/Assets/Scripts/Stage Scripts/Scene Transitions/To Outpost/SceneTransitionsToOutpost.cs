using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionsToOutpost : MonoBehaviour
{
    public string sceneName;
    public bool tutorialPhase;
    SaveHandler saveHandler;
    UpgradeSaveManager upgradeSaveManager;
    ItemSaveManager itemSaveManager;
    [SerializeField] Character character;
    UpgradeHolder upgradeHolder;
    PauseMenuScripts pauseMenuScripts;

    private void Awake()
    {
        //saveHandler = FindObjectOfType<SaveHandler>();
        character = FindObjectOfType<Character>();
        upgradeHolder = FindObjectOfType<UpgradeHolder>();
        itemSaveManager = FindObjectOfType<ItemSaveManager>();
        upgradeSaveManager = FindObjectOfType<UpgradeSaveManager>();
        pauseMenuScripts = FindObjectOfType<PauseMenuScripts>();
    }

    public void Confirm()
    {
        Save();
        pauseMenuScripts.ClosePauseMenu();
        SceneManager.LoadScene(sceneName);
    }
    public void Save()
    {
        itemSaveManager.SaveInventory(character);
        upgradeSaveManager.SaveUpgrade(this.upgradeHolder);

        if(!tutorialPhase)
        {
            PlayerPrefs.SetInt("Day", DayCounter.instance.days);
        }
    }
    //private void OnTriggerEnter2D(Collider2D player)
    //{
    //    if(player.GetComponent<Player>())
    //    {
    //        itemSaveManager.SaveInventory(character);
    //        upgradeSaveManager.SaveUpgrade(this.upgradeHolder);
    //        SceneManager.LoadScene("Outpost Stage");
    //    }
    //}
}
