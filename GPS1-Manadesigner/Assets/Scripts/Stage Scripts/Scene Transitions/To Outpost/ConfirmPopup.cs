using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmPopup : MonoBehaviour
{
    public GameObject Panel;
    PauseMenuScripts pauseMenuScripts;

    void Awake()
    {
        Panel.SetActive(false);
        Time.timeScale = 1;
    }
    private void Start()
    {
        pauseMenuScripts = FindObjectOfType<PauseMenuScripts>();
    }

    private void OnTriggerEnter2D(Collider2D player)
    {
        if (player.CompareTag("Player"))
        {
            pauseMenuScripts.OpenPauseMenu();
            Panel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void Cancel()
    {
        Panel.SetActive(false);
        Time.timeScale = 1;
    }
}
