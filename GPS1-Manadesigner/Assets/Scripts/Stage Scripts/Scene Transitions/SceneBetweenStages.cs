using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneBetweenStages : MonoBehaviour
{
    UpgradeSaveManager upgradeSaveManager;
    ItemSaveManager itemSaveManager;
    [SerializeField] Character character;
    UpgradeHolder upgradeHolder;
    public string nextSceneName = "Stage 2 Prototype";

    public bool isWinScene = true;
    private void Awake()
    {
        //saveHandler = FindObjectOfType<SaveHandler>();
        if(character == null)
        {
            character = FindObjectOfType<Character>();
        }
        upgradeHolder = FindObjectOfType<UpgradeHolder>();
        itemSaveManager = FindObjectOfType<ItemSaveManager>();
        upgradeSaveManager = FindObjectOfType<UpgradeSaveManager>();
    }

    private void OnTriggerEnter2D(Collider2D player)
    {
        if (player.CompareTag("Player"))
        {
            itemSaveManager.SaveInventory(character);
            upgradeSaveManager.SaveUpgrade(this.upgradeHolder);
            FindObjectOfType<PlayerHealth>().saveHealth();
            //SceneManager.LoadScene(nextSceneName);


            // Temporary only, bad practice hard code
            if (SceneManager.GetActiveScene().name == "Stage 3 Prototype" && gameObject.name == "Next Scene")
            {
                FindObjectOfType<SceneTransitioning>().SceneTransition("USAWins");
            }
            else
            {
                FindObjectOfType<SceneTransitioning>().SceneTransition(nextSceneName);
            }
        }
    }
}
