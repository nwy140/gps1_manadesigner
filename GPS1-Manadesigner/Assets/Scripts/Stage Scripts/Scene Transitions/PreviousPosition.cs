using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviousPosition : SceneTransitioning
{
    public Transform player;
    public float posX, posY;
    public string previousSceneName;

    public override void Start()
    {
        base.Start();

        if (prevScene == previousSceneName)
        {
            player.position = new Vector2(posX, posY);
        }
    }
}
