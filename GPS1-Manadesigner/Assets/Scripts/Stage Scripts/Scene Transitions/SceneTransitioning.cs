using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitioning : MonoBehaviour
{
    public static string prevScene;
    public static string currScene;

    public virtual void Start()
    {
        currScene = SceneManager.GetActiveScene().name;
    }

    public void SceneTransition(string name)
    {
        prevScene = currScene;
        SceneManager.LoadScene(name);
    }
}
