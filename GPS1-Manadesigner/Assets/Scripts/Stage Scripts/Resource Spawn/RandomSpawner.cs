using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public int numberToSpawn;
    public List<GameObject> spawnPool;
    public GameObject bg;
    public float spawnRad = 5;
    public float spawnCollisionCheckRadius;
    void Start()
    {
        spawnRes();
    }

    public void spawnRes()
    {
        int randomItem = 0;
        GameObject spawn;
        MeshCollider m = bg.GetComponent<MeshCollider>();
        float x, y;
        Vector2 pos;

        for(int i = 0; i<numberToSpawn; i++)
        {
            randomItem = Random.Range(0, spawnPool.Count);
            spawn = spawnPool[randomItem];

            x = Random.Range(m.bounds.min.x, m.bounds.max.x);
            y = Random.Range(m.bounds.min.y, m.bounds.max.y);
            pos = new Vector2(x, y);

            if(!Physics.CheckSphere(pos, spawnCollisionCheckRadius))
            {
                Instantiate(spawn, pos, spawn.transform.rotation);
            }
            
        }
    }
}
