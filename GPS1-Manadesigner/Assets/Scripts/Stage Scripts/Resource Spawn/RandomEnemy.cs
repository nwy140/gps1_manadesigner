using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEnemy : MonoBehaviour
{
    public Collider2D[] colliders;
    public float radius;
    float rando;
    public Vector2 pos = new Vector2(0,0);
    
    public GameObject quad;
    public List<GameObject> enemyPool;
    public int numberToSpawn;
    public LayerMask mask;
    public float spawnRad = 5;
    public float spawnCollisionCheckRadius;


    void Start()
    {
        spawnEnemy();
    }
    

    public void spawnEnemy()
    {
        int randomEnem = 0;
        GameObject spawn;
        float x, y;
        MeshCollider c = quad.GetComponent<MeshCollider>();
        int safetyNet = 0;
        bool canSpawnHere = false;

        Debug.Log("Function is in use.");

        for (int i = 0; i < numberToSpawn; i++)
        {
            randomEnem = Random.Range(0, enemyPool.Count);
            spawn = enemyPool[randomEnem];

            x = Random.Range(c.bounds.min.x, c.bounds.max.x);
            y = Random.Range(c.bounds.min.y, c.bounds.max.y);
            pos = new Vector2(x, y);

            while (!canSpawnHere)
            {
                x = Random.Range(c.bounds.min.x, c.bounds.max.x);
                y = Random.Range(c.bounds.min.y, c.bounds.max.y);
                pos = new Vector2(x, y);
                canSpawnHere = PreventSpawnOverLap(pos);

                if (canSpawnHere)
                {
                    break;
                }
                safetyNet++;
                if (safetyNet > 50)
                {
                    break;
                }
            }

            if(!Physics.CheckSphere(pos, spawnCollisionCheckRadius, mask))
            {
                Instantiate(spawn, pos, spawn.transform.rotation);
                Debug.Log("Fishes spawned.");
            }

            else
            {
                Debug.Log("Is colliding with something. Cannot spawn");
            }
            
        }
    }

    bool PreventSpawnOverLap(Vector2 pos)
    {
        colliders = Physics2D.OverlapCircleAll(transform.position, radius, mask);

        for (int i = 0; i < colliders.Length; i++)
        {
            Vector2 centerPoint = colliders[i].bounds.center;
            float width = colliders[i].bounds.extents.x;
            float height = colliders[i].bounds.extents.y;

            float leftExtent = centerPoint.x - width;
            float rightExtent = centerPoint.x + width;
            float lowerExtent = centerPoint.y - height;
            float upperExtent = centerPoint.y + height;

            if (pos.x >= leftExtent && pos.y <= rightExtent)
            {
                if (pos.y >= lowerExtent && pos.y <= upperExtent)
                {
                    return false;
                }
            }
        }
        return true;
    }

}
