using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastInHeirarchy : MonoBehaviour
{
    void Start()
    {
        this.transform.SetAsLastSibling();
    }
}
