using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayCounter : MonoBehaviour
{
    public static DayCounter instance;
    public int days = 0;
    public Text dayCounter;

    private void Awake()
    {
        instance = this;
        days = PlayerPrefs.GetInt("Day", days);
        CountDays();
    }

    public void CountDays()
    {
        days++;
        if (dayCounter != null)
        {
            dayCounter.text = "DAY " + days;
        }
    }
}
