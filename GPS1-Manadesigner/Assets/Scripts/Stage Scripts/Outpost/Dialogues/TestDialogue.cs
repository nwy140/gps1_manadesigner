using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDialogue : MonoBehaviour
{
    public static TestDialogue instance;
    DialogueSystem dialogue;

    [Header("Choices Per Day")]
    public GameObject dayFiveChoice;
    public bool dayFivePicked = false;
    public GameObject dayEightChoice;
    public bool goodNews = false;
    public bool badNews = false;
    public GameObject dayElevenChoice;
    public bool yes = false;
    public bool no = false;

    [Header("Index")]
    public int index = 0;

    void Start()
    {
        dialogue = DialogueSystem.instance;
        dayFiveChoice.SetActive(false);
        dayEightChoice.SetActive(false);
        dayElevenChoice.SetActive(false);
    }

    private void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (OutpostChoices.instance.isActive == true)
        {
            StartUpdating();
        }
    }

    public void StartUpdating()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
            {
                //AudioManager.instance.PlaySFX("TalkScientist");
                switch (DayCounter.instance.days)
                {
                    case 2:
                    case 3:
                    case 4:
                        {
                            if (index >= dayTwo.Length)
                            {
                                return;
                            }

                            Talk(dayTwo[index]);
                            index++;

                            if (index == dayTwo.Length)
                            {
                                OutpostChoices.instance.EnablePanels();
                            }

                            break;
                        }

                    case 5:
                    case 6:
                    case 7:
                        {
                            if (index >= dayFive.Length)
                            {
                                return;
                            }

                            Talk(dayFive[index]);
                            index++;

                            if (index == dayFive.Length)
                            {
                                OutpostChoices.instance.newsPanel.SetActive(false);
                                dayFiveChoice.SetActive(true);
                            }

                            break;
                        }

                    case 8:
                    case 9:
                    case 10:
                        {
                            if (index >= dayEight.Length)
                            {
                                return;
                            }

                            Talk(dayEight[index]);
                            index++;

                            if (index == dayEight.Length)
                            {
                                OutpostChoices.instance.newsPanel.SetActive(false);
                                dayEightChoice.SetActive(true);
                            }

                            break;
                        }

                    case 11:
                    case 12:
                    case 13:
                        {
                            if (index >= dayEleven.Length)
                            {
                                return;
                            }

                            Talk(dayEleven[index]);
                            index++;

                            if (index == dayEleven.Length)
                            {
                                OutpostChoices.instance.newsPanel.SetActive(false);
                                dayElevenChoice.SetActive(true);
                            }

                            break;
                        }


                    case 14:
                        {
                            if (index >= dayFourteen.Length)
                            {
                                return;
                            }

                            Talk(dayFourteen[index]);
                            index++;

                            if (index == dayFourteen.Length)
                            {
                                OutpostChoices.instance.EnablePanels();
                            }

                            break;
                        }

                    case 15:
                        {
                            break;
                        }

                    default:
                        {
                            Debug.Log("No dialogues included! 14 days are maxed.");
                            break;
                        }
                }
            }
        }
    }

    void Talk(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string scientist = (parts.Length >= 2) ? parts[1] : "";

        dialogue.StartTalking(speech, scientist);
    }

    public void DayFive()
    {
        dayFivePicked = true;
        OutpostChoices.instance.newsPanel.SetActive(true);
        dayFiveChoice.SetActive(false);
    }

    public void GoodNews()
    {
        goodNews = true;
        DialogChoices.instance.index = 0;
        OutpostChoices.instance.newsPanel.SetActive(true);
        dayEightChoice.SetActive(false);
    }

    public void BadNews()
    {
        badNews = true;
        DialogChoices.instance.index = 0;
        OutpostChoices.instance.newsPanel.SetActive(true);
        dayEightChoice.SetActive(false);
    }

    public void ConfirmYes()
    {
        yes = true;
        DialogChoices.instance.index = 0;
        OutpostChoices.instance.newsPanel.SetActive(true);
        dayElevenChoice.SetActive(false);
    }

    public void NotSure()
    {
        no = true;
        DialogChoices.instance.index = 0;
        OutpostChoices.instance.newsPanel.SetActive(true);
        dayElevenChoice.SetActive(false);
    }


    [Header("Dialogue Lines")]

    [TextArea]
    public string noticeToAllTechs = "Leave the last element in every dialogue boxes empty or there'll be an index boundary error.";

    public string[] dayTwo = new string[]
    {
        "Well, not much to update, really. The USSR is making steady progress. :Liam",
        "With the current projections and the speed they�re going, they�ll reach there in 2 weeks, as we�ve speculated.",
        "Maybe even less.",
        "Liam lets out an exhausted sigh. The news has taken a toll on him. : ",
        "There�s no point worrying about this, in any case. We�ll do what we have to do. :Liam",
        "He�s extremely bad at hiding his worry, even with his arms folded. A lone finger taps against his arm in apprehensive energy. : ",
        "Just keep at it with what you�re doing, and we�ll be fine. :Liam",
        "Liam looks at you, a subtle expression of concern mingled with a hint of hope. : ",
        " "
    };

    public string[] dayFive = new string[]
    {
        "Oh, hmm. Things have been� hectic around here. :Liam",
        "Liam looks tired, as if he hasn�t had a good night�s sleep lately. : ",
        "Research has been coming along well at least. Good progress. We�re catching up with them. :Liam",
        "A long breath of relief escapes from him. : ",
        "Liam takes out his clipboard and looks through a few papers.",
        "They�re almost at the end of the Continental Region. :Liam",
        " "
    };

    public string[] dayEight = new string[]
    {
        "Update� oh, yes right, of course. :Liam",
        "You can tell his mind is elsewhere. And also probably sleep deprived. : ",
        "Liam is still trying to keep the same professionalism you saw him with during your first meeting, but it�s getting harder.",
        "You feel a little bad for him. He must have a lot of expectations riding on his shoulders.",
        "Do you want to hear the good news, or the bad news? :Liam",
        " "
    };

    public string[] dayEleven = new string[]
    {
        "Liam groans tiredly when you ask, no longer bothering with restraint. : ",
        "This is a man that has not slept in a while.",
        "You see that his glasses are a little crooked. He corrects them, but it doesn�t make him look any less disheveled.",
        "Things aren't... going as planned. :Liam",
        "They�re really close. Already halfway through the Mariana Trench. We�re scrambling to get things in order.",
        "Liam sighs, running a hand through his hair. : ",
        "You take a closer look at him, and you notice that his current demeanor is a departure from the well-kept Head Scientist you met when you first came here.",
        "This program has taken so much out of him.",
        "We�ve done our best to keep up with the research. It isn�t easy. :Liam",
        "You know that � It isn�t easy� isn�t even half of it. : ",
        "The submarine seems to be holding up decently, especially with all the new upgrades. :Liam",
        "He goes silent for a moment. When he speaks again, it�s barely above a whisper. : ",
        "Tell me, do you think we�ll� be able to get there before them? :Liam",
        "Be honest.",
        "Through the lens of his glasses, you can see warry hope shifting in his eyes. : ",
        " "
    };

    public string[] dayFourteen = new string[]
    {
        "Well, it�s the final day. :Liam McCoy",
        "Liam�s voice is stoic despite the exhaustion weighing him down. :",
        "He takes out his clipboard, but he doesn�t even look at it to know what to say next.",
        "It�s approximately 20 hours before they reach the bottom. Maybe even less. :Liam McCoy",
        "His eyes stare intensely into yours. : ",
        "It�s now or never. This is your last shot. :Liam McCoy",
        "Liam claps a hand on your shoulder, firmly. : ",
        "Go out there and prove that you�re really the Navy�s finest. : Liam McCoy",
        " "
    };
}
