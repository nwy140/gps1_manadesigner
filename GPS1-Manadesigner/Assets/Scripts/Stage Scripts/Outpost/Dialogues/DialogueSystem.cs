using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour
{
    public static DialogueSystem instance;
    public DIALOGUE_ELEMENTS dialogueElements;
    public bool isTalking { get { return talking != null; } }
    public bool isWaitingForUserInput = false;
    public Text pressContinue;
    Coroutine talking = null;
    string targetSpeech = "";

    [System.Serializable]
    public class DIALOGUE_ELEMENTS
    {
        public GameObject textBox;
        public Text scientistName;
        public Text dialogues;
    }

    private void Awake()
    {
        instance = this;
    }

    public void StartTalking(string speech, string speaker = "")
    {
        StopTalking();
        dialogueElements.dialogues.text = targetSpeech;
        StartCoroutine(Talking(speech, speaker));
    }

    public void StopTalking()
    {
        if (isTalking)
        {
            StopCoroutine(talking);
        }

        talking = null;
    }

    IEnumerator Talking(string speech, string speaker = "")
    {
        dialogueElements.textBox.SetActive(true);
        dialogueElements.dialogues.text = "";
        dialogueElements.scientistName.text = FindCharacterName(speaker);
        isWaitingForUserInput = false;
        targetSpeech = speech;

        while (dialogueElements.dialogues.text != targetSpeech)
        {
            dialogueElements.dialogues.text += targetSpeech[dialogueElements.dialogues.text.Length];
            yield return new WaitForEndOfFrame();
        }

        isWaitingForUserInput = true;

        while (isWaitingForUserInput)
        {
            yield return new WaitForEndOfFrame();
        }

        StopTalking();
        pressContinue.text = "Press enter to continue";
    }

    string FindCharacterName(string name)
    {
        string retVal = dialogueElements.scientistName.text;

        if (name != dialogueElements.scientistName.text && name != "")
        {
            retVal = (name.ToLower().Contains("Narrator")) ? "" : name;
        }

        return retVal;
    }
}
