using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class OutpostChoices : MonoBehaviour
{
    //[SerializeField] Player player;
    public static OutpostChoices instance;

    [Header("Dialogue Essentials")]
    public GameObject defaultPanel;
    public GameObject newsPanel;
    public GameObject scientistIdleAnim;
    public GameObject scientistTalkAnim;
    public GameObject coverPanel;
    public Text nameText, textBox;
    public bool isActive = false;

    UpgradeHolder upgradeHolder;
    storage storage;

    [Header("Save State")]
    [SerializeField] ItemSaveManager itemSaveManager;
    [SerializeField] UpgradeSaveManager upgradeSaveManager;
    [SerializeField] Character character;

    private void Start()
    {
        upgradeHolder = FindObjectOfType<UpgradeHolder>();
        //character = FindObjectOfType<Character>();
        //itemSaveManager = FindObjectOfType<ItemSaveManager>();
    }

    private void Awake()
    {
        newsPanel.SetActive(false);
        scientistTalkAnim.SetActive(false);
        instance = this;
        nameText.text = " ";
        textBox.text = "DAY " + DayCounter.instance.days + " (Press enter)";
    }

    public void TalkWithScientist()
    {
        isActive = true;
        
        newsPanel.SetActive(true);
        scientistTalkAnim.SetActive(true);
        //upgradePanel.SetActive(false);
        //shelfImage.SetActive(false);
        defaultPanel.SetActive(false);
        scientistIdleAnim.SetActive(false);
    }

    public void EnablePanels()
    {
        isActive = false;
        
        newsPanel.SetActive(false);
        scientistTalkAnim.SetActive(false);
        defaultPanel.SetActive(true);
        scientistIdleAnim.SetActive(true);
        coverPanel.SetActive(false);
        //upgradePanel.SetActive(true);
        //shelfImage.SetActive(true);

        TestDialogue.instance.index = 0;
        nameText.text = " ";
        textBox.text = "DAY " + DayCounter.instance.days;
    }

    public void ReturnSea()
    {
        itemSaveManager.SaveInventory(character);
        upgradeSaveManager.SaveUpgrade(upgradeHolder);

        PlayerPrefs.SetFloat("health", -1);
        PlayerPrefs.SetFloat("oxygenSupply", -1);
        PlayerPrefs.SetInt("healthKits", -1);

        //player = FindObjectOfType<Player>();
        //storage = player.GetStorage();

        //SaveObject saveObject = new SaveObject
        //{
        //    turbineUpgrade = player.GetTurbineUpgrade(),
        //    items = storage.GetListOfItemsInInventory()
        //};

        //string json = JsonUtility.ToJson(saveObject);

        //File.WriteAllText(Application.dataPath + "/Save Files/Autosave.txt", json);

        FindObjectOfType<SceneTransitioning>().SceneTransition("Stage Prototype");
        //SceneManager.LoadScene("Stage Prototype");

    }

    private class SaveObject
    {
        public Turbine turbineUpgrade;
        public List<item> items;
    }
}
