using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogChoices : MonoBehaviour
{
    public static DialogChoices instance;

    [Header("Panels")]
    public GameObject choicePanel;

    [Header("Scientist Sprites")]
    public GameObject scientistTalk;
    public GameObject scientistIdle;
    public GameObject glasses;

    [Header("Index")]
    public int index = 0;

    DialogueSystem dialogue;

    private void Start()
    {
        dialogue = DialogueSystem.instance;
        choicePanel.SetActive(false);
    }

    private void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (TestDialogue.instance.dayFivePicked)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
                {
                    if (index >= continentalRegion.Length)
                    {
                        return;
                    }

                    Talk(continentalRegion[index]);
                    index++;

                    if (index == continentalRegion.Length)
                    {
                        TestDialogue.instance.dayFivePicked = false;
                        OutpostChoices.instance.EnablePanels();
                    }
                }
            }
        }
        
        else if (TestDialogue.instance.goodNews)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
                {
                    if (index >= goodNewsDialogs.Length)
                    {
                        return;
                    }

                    Talk(goodNewsDialogs[index]);
                    index++;

                    if (index == goodNewsDialogs.Length)
                    {
                        TestDialogue.instance.goodNews = false;
                        OutpostChoices.instance.EnablePanels();
                    }
                }
            }
        }

        else if (TestDialogue.instance.badNews)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
                {
                    if (index >= badNewsDialog.Length)
                    {
                        return;
                    }

                    Talk(badNewsDialog[index]);
                    index++;

                    if (index == badNewsDialog.Length)
                    {
                        TestDialogue.instance.badNews = false;
                        OutpostChoices.instance.EnablePanels();
                    }
                }
            }
        }

        else if (TestDialogue.instance.yes)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
                {
                    if (index >= confidenceDialog.Length)
                    {
                        return;
                    }

                    Talk(confidenceDialog[index]);
                    index++;

                    if (index == confidenceDialog.Length)
                    {
                        TestDialogue.instance.yes = false;
                        OutpostChoices.instance.EnablePanels();
                    }
                }
            }
        }

        else if (TestDialogue.instance.no)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (!dialogue.isTalking || dialogue.isWaitingForUserInput)
                {
                    if (index >= notSureDialog.Length)
                    {
                        return;
                    }

                    Talk(notSureDialog[index]);
                    index++;

                    if (index == notSureDialog.Length)
                    {
                        TestDialogue.instance.no = false;
                        OutpostChoices.instance.EnablePanels();
                    }
                }
            }
        }

    }
    void Talk(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string scientist = (parts.Length >= 2) ? parts[1] : "";

        dialogue.StartTalking(speech, scientist);
    }

    [TextArea]
    public string noticeToAllTechs = "Leave the last element in every dialogue boxes empty or there'll be an index boundary error.";

    public string[] continentalRegion = new string[]
    {
        "Ah, right. Divers only do meters, I forgot-- sorry. :Liam",
        "Liam almost looks sheepish, but you can�t really tell with his glasses in the way. : ",
        "It�s around 3,000 meters deep. Rough Estimate, more or less :Liam",
        "Abyssal plain starts when you hit the pressure threshold. That�s the divide.",
        "Same thing goes for when you descend deeper too. Remember you�ll need a better hullshell to pass it.� He states matter-of-factly.",
        "Briefly, Liam steals a glimpse at you and you hear the sound of pen against paper. : ",
        "Midway, you hear the clattering of what you presume is lab equipment coming from the other room.",
        "�Nothing�s wrong Dr McCoy! Everything�s fine! � The voice of a young scientist is what follows the racket.",
        "Liam takes a deep breath, to hide his exasperated groan.",
        "Just� do what you do best. We�re working as hard as we can too. :Liam",
        " "
    };

    public string[] goodNewsDialogs = new string[]
    {
        "Good news is, NaMSA just got additional funding from the government. Which, I don�t know if it�s gonna do much with so little time left. :Liam",
        "At least the staff will be less overworked now.",
        "You can tell he�s hoping that this cuts him a break too. : ",
        "However, you found yourself dumbfounded from the mention of NaMSA.",
        "You asked Liam what the name stands for.",
        "Wait, you don't know? :Liam",
        "All you can muster is a long �uh�. : ",
        "I thought they�d tell you what you�re getting yourself into. :Liam",
        "You tell him your superiors did, but you kind of tuned half of it out because it was� boring. : ",
        "For the first time, think you see a smile? It�s gone before you can point it out or process it.",
        "The NaMSA briefing was very long, yes. :Liam",
        "'And boring', were the words he chose to leave unsaid. : ",
        "Well, in any case, NaMSA stands for National Marine and Subnautics Administration. :Liam",
        "You make a noise of acknowledgement as you nod. : ",
        " "
    };

    public string[] badNewsDialog = new string[]
    {
        "The bad news is that the USSR is already near the end of the Abyssal Plain. :Liam",
        "They were literally only at the end of the Continental Region 3 days ago!",
        "Irritation begins to rise in Liam, but he quickly quells it. : ",
        " "
    };

    public string[] confidenceDialog = new string[]
    {
        "Liam nods. : ",
        "Good, good. I�m glad. :Liam",
        "A smile flickers briefly across his exhausted face. : ",
        "We�re almost at the end. The last stretch. :Liam",
        "Liam looks to the submarine docking station, then back at you. : ",
        "I better get a pay raise at the end of this. :Liam",
        "You let out a laugh at this, and Liam can�t help but chuckle alongside you. : ",
        " "
    };

    public string[] notSureDialog = new string[]
    {
        "....... :Liam",
        "He doesn't say anything. : ",
        "You can hear his breathing becoming more labored as seconds pass by.",
        "You barely see it, but for a moment, it looked like he was on the verge of breaking down.",
        "And in the next moment, it�s gone.",
        "Liam adjusts his glasses until all you can see are the fluorescent lights shimmering in the lenses.",
        "You�re reminded of when you first met him, but� something is different.",
        "I see. :Liam",
        "This doesn�t mean you can stop trying.",
        "Liam directs you a pointed look. : ",
        "You better keep diving deeper. For all our sakes. :Liam",
        "Yelling in the distance catches your attention. : ",
        "The outpost hasn�t slowed down since you got back.",
        "Everyone is busy, and things are frantic.",
        "A sigh escapes from Liam when he hears an argument brewing the room over.",
        "Looks like it�s going to be another all-nighter. :Liam",
        " "
    };

}
