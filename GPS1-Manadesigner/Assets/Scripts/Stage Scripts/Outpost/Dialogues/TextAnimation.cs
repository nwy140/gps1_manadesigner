using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAnimation : MonoBehaviour
{
    public GameObject choices, text, coverHover, pressEnter;
    public float textDelay = 0.1f;
    public string fullText;
    public bool finishText = false;
    string currentText = "";

    void Start()
    {
        choices.SetActive(false);
        text.SetActive(true);
    }

    private void Awake()
    {
        StartCoroutine(ShowText());
    }

    void Update()
    {
        if(finishText)
        {
            coverHover.SetActive(true);
        }
        
        if (Input.GetKeyDown(KeyCode.Return))
        {
            choices.SetActive(true);
            text.SetActive(false);
            coverHover.SetActive(false);
            pressEnter.SetActive(false);
        }
    }

    IEnumerator ShowText()
    {
        for (int i = 0; i <= fullText.Length; i++)
        {
            currentText = fullText.Substring(0, i);
            this.GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(textDelay);
        }

        finishText = true;
    }
}
