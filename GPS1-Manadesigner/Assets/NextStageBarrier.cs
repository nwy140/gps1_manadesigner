using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStageBarrier : MonoBehaviour
{
    [SerializeField] int pressureStrenghtRequirement;

    private void Start()
    {
        PlayerHealth playerHealth = FindObjectOfType<PlayerHealth>();

        if (playerHealth.GetHull().GetPressureStrength() >= pressureStrenghtRequirement)
        {
            //Temporarily Disabled
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
//    private void OnCollisionEnter2D(Collision2D collision)
//    {
//        if(collision.gameObject.GetComponent<PlayerHealth>())
//        {
//            Debug.Log("Collide");
//            PlayerHealth playerHealth = collision.gameObject.GetComponent<PlayerHealth>();
//            if(playerHealth.GetHull().GetPressureStrength() >= pressureStrenghtRequirement)
//            {
//                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
//                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
//            }
//        }
//    }
//}
