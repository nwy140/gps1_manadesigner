using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaOfEffect : MonoBehaviour
{
    public ParticleSystem explosion;
    private float AOEDamage = 100f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<EnemyHealth>())
        {
            EnemyHealth enemy = collision.gameObject.GetComponent<EnemyHealth>();
            enemy.GetDamage(AOEDamage);
        }
    }

    private void Start()
    {
        explosion.Play();
        Destroy(this.gameObject, 1f);
    }
}
