using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// This script checks if a certain upgrade has been purchased. If a specific upgrade has been purchased, the button that is assigned to call the method to purchase that specific upgrade 
// will be set to uninteractable

public class UpgradeChecker : MonoBehaviour
{
    [SerializeField] PlayerUpgrades[] standardUpgrades;

    [SerializeField] Sprite disabledImage;

    [Space]

    [SerializeField] Button[] turbineButtons;
    [SerializeField] Turbine[] turbineUpgrades;

    [Space]

    [SerializeField] Button[] oxygenTankButtons;
    [SerializeField] OxygenTank[] oxygenTankUpgrades;

    [Space]

    [SerializeField] Button[] inventoryButtons;
    [SerializeField] Inventory[] inventoryUpgrades;

    [Space]

    [SerializeField] Button[] hullButtons;
    [SerializeField] Hull[] hullUpgrades;

    [Space]

    [SerializeField] Button[] selfRepairButtons;
    [SerializeField] SelfRepair[] selfRepairUpgrades;

    [Space]

    [SerializeField] Button[] weaponButtons;
    [SerializeField] Weapon[] weaponUpgrades;

    [Space]

    [SerializeField] UpgradeHolder upgradeHolder;

    // Start is called before the first frame update
    void Start()
    {
        if (upgradeHolder == null)
        {
            upgradeHolder = FindObjectOfType<UpgradeHolder>();
        }

        CheckAllUpgrades();
    }

    public void CheckAllUpgrades()
    {
        CheckUpgradeButtons(turbineButtons, turbineUpgrades, upgradeHolder.TurbineUpgrade);
        CheckUpgradeButtons(oxygenTankButtons, oxygenTankUpgrades, upgradeHolder.OxygenTankUpgrade);
        CheckUpgradeButtons(inventoryButtons, inventoryUpgrades, upgradeHolder.InventoryUpgrade);
        CheckUpgradeButtons(hullButtons, hullUpgrades, upgradeHolder.HullUpgrade);
        CheckUpgradeButtons(selfRepairButtons, selfRepairUpgrades, upgradeHolder.SelfRepairUpgrade);
        CheckUpgradeButtons(weaponButtons, weaponUpgrades, upgradeHolder.WeaponUpgrade);
    }

    private void CheckUpgradeButtons(Button[] upgradeButton, PlayerUpgrades[] playerUpgrades, PlayerUpgrades currentUpgrade)
    {
        for(int i = 0; i < standardUpgrades.Length; i++)
        {
            if (currentUpgrade == standardUpgrades[i])
                return;
        }

        int upgradeIndex = 0;

        for (int i = 0; i < playerUpgrades.Length; i++)
        {
            upgradeButton[i].interactable = false;

            if (playerUpgrades[i].ID != currentUpgrade.ID)
            {
                upgradeIndex++;
            }
            else
            {
                break;
            }
        }

        //Debug.Log($"Upgrade Index = {upgradeIndex}, Half of player upgrades length = {playerUpgrades.Length / 2}");

        if (upgradeIndex >= playerUpgrades.Length / 2)
        {
            for (int i = 0; i < playerUpgrades.Length / 2; i++)
            {
                GameObject buttonObject = upgradeButton[i].gameObject;
                buttonObject.GetComponent<Image>().color = new Color32(120, 120, 120, 255);
                upgradeButton[i].enabled = false;
            }
        }

        if (upgradeIndex < playerUpgrades.Length / 2)
        {
            for (int i = playerUpgrades.Length / 2; i < playerUpgrades.Length; i++)
            {
                GameObject buttonObject = upgradeButton[i].gameObject;
                buttonObject.GetComponent<Image>().color = new Color32(120, 120, 120, 255);
                upgradeButton[i].enabled = false;
            }
        }
    }
}
