using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class QuickOpenInventory : MonoBehaviour
{

    public Character charInven;
    public PauseMenuScripts pauseMenu;
    private void Awake()
    {
        Init();
    }
    private void Init()
    {
        //https://answers.unity.com/questions/890636/find-an-inactive-game-object.html
        var charInvenComp = Resources.FindObjectsOfTypeAll<Character>();
        if (charInvenComp.Length > 0)
        {
            charInven = charInvenComp[0];
        }
        var temp = Resources.FindObjectsOfTypeAll<PauseMenuScripts>();
        if (temp.Length > 0)
        {
            pauseMenu = temp[0];
        }
    }

    public void OnInputSelect(InputAction.CallbackContext context)
    {
        Debug.Log(context.phase);
        if (context.action.phase == InputActionPhase.Started)
        {
            AttemptOpenInventory();
        }
    }
    public void AttemptOpenInventory()
    {
        if (charInven == null)
        {
            Init();
        }
        else if(charInven != null /*&& pauseMenu != null*/)
        {
            Debug.Log("Attempt Open inventory");
            charInven.gameObject.SetActive(!charInven.gameObject.activeInHierarchy);
            if (charInven.gameObject.activeInHierarchy == true)
            {
                pauseMenu.OpenPauseMenu();
            }
            else
            {
                pauseMenu.ClosePauseMenu();
            }
        }
    }
}
