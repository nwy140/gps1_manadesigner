using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RequirementChecker : MonoBehaviour
{
    public PlayerUpgrades playerUpgrade;

    List<item> fishMaterials = new List<item>();
    List<item> rawMaterials = new List<item>();

    public Text[] fishMaterialText;
    public Text[] rawMaterialText;

    public storage inv;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        for (int i = 0; i < playerUpgrade.GetFishMaterialAmountRequirement().Length; i++)
        {
            fishMaterialText[i].text = $"x{playerUpgrade.GetFishMaterialAmountRequirement()[i]}";
        }

        for (int i = 0; i < playerUpgrade.GetRawMaterialAmountRequirement().Length; i++)
        {
            rawMaterialText[i].text = $"x{playerUpgrade.GetRawMaterialAmountRequirement()[i]}";
        }
    }

    private void OnEnable()
    {
        CheckRequirement();
    }

    public void CheckRequirement()
    {
        List<int> fishAmountRequirements = new List<int>();
        List<int> rawAmountRequirements = new List<int>();

        //Debug.Log($"List of Items in Inventory = {inv.GetListOfItemsInInventory().Count}");

        for (int i = 0; i < playerUpgrade.GetFishMaterialAmountRequirement().Length; i++)
        {
            fishAmountRequirements.Add(0);
        }

        //Debug.Log($"Fish Amount Requirement = {fishAmountRequirements.Count}");
        //Debug.Log($"Fish Material Type Requirement Length = {playerUpgrade.GetFishMaterialAmountRequirement().Length}");

        for (int i = 0; i < playerUpgrade.GetRawMaterialAmountRequirement().Length; i++)
        {
            rawAmountRequirements.Add(0);
        }

        //Debug.Log($"Raw Amount Requirement = {rawAmountRequirements.Count}");
        //Debug.Log($"Raw Material Type Requirement Length = {playerUpgrade.GetRawMaterialAmountRequirement().Length}");


        for (int i = 0; i < playerUpgrade.GetFishMaterialTypeRequirement().Length; i++)
        {

            foreach(itemSlots itemSlot in inv.GetListOfItemsInInventory())
            {
                //Debug.Log("Go through each items in inventory for items similar to fish requirement");

                if (itemSlot.Item == playerUpgrade.GetFishMaterialTypeRequirement()[i])
                {
                    fishAmountRequirements[i] += itemSlot.Amount;
                }
            }

            if (fishAmountRequirements[i] >= playerUpgrade.GetFishMaterialAmountRequirement()[i])
            {
                fishMaterialText[i].color = Color.green;
            }
            else
            {
                fishMaterialText[i].color = Color.red;
            }
        }

        for (int i = 0; i < playerUpgrade.GetRawMaterialTypeRequirement().Length; i++)
        {
            foreach (itemSlots itemSlot in inv.GetListOfItemsInInventory())
            {
                //Debug.Log("Go through each items in inventory for items similar to fish requirement");

                if (itemSlot.Item == playerUpgrade.GetRawMaterialTypeRequirement()[i])
                {
                    rawAmountRequirements[i] += itemSlot.Amount;
                }
            }

            if (rawAmountRequirements[i] >= playerUpgrade.GetRawMaterialAmountRequirement()[i])
            {
                rawMaterialText[i].color = Color.green;
            }
            else
            {
                rawMaterialText[i].color = Color.red;
            }
        }
    }
}
