using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    //[SerializeField] Slider healthBarPrefab;
    [SerializeField] Resource fishMaterial;
    [SerializeField] Sprite hurtSprite;
    Sprite originalSprite;

    //Slider healthBar;
    Canvas gameCanvas;

    [SerializeField] float startingHealth = 10;
    float health;

    private void Awake()
    {
        //gameCanvas = FindObjectOfType<Canvas>();
        //gameCanvas = 
        GameObject.FindGameObjectWithTag("MainCanvas").TryGetComponent(out gameCanvas);
        health = startingHealth;

        //if (healthBar == null)
        //{
        //    healthBar = Instantiate(healthBarPrefab, transform.position, Quaternion.identity);
        //    healthBar.transform.SetParent(gameCanvas.transform);
        //}
    }

    private void Start()
    {
        originalSprite = GetComponent<SpriteRenderer>().sprite;
    }

    private void Update()
    {
        //if (healthBar == null) { return; }
        //Vector2 screenPos = Camera.main.WorldToScreenPoint(new Vector2(transform.position.x, transform.position.y + 1));
        //healthBar.transform.position = screenPos;
        //healthBar.value = this.health / this.startingHealth;
    }

    public void GetDamage(float damage)
    {
        this.health -= damage;
        this.GetComponent<SpriteRenderer>().sprite = hurtSprite;
        StartCoroutine(ReturnOriginalSprite());
        if(this.health <= 0)
        {
            KillEnemy();
            OnKO.Invoke();
        }
    }

    IEnumerator ReturnOriginalSprite()
    {
        yield return new WaitForSeconds(0.5f);
        if(gameObject.activeInHierarchy)
        {
            GetComponent<SpriteRenderer>().sprite = originalSprite;
        }

    }


    bool hasFishMaterialSpawned = false;
    private void KillEnemy()
    {
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<Collider2D>().enabled = false;
        //Destroy(this.healthBar.gameObject);
        //fishMaterial.gameObject.SetActive(true);
        //fishMaterial.transform.SetParent(null);

        if (hasFishMaterialSpawned == false)
        {
            Instantiate(fishMaterial, transform.position, Quaternion.identity);
            hasFishMaterialSpawned = true;
            Debug.Log("Instantiate");
        }

        Destroy(this);
    }

    public UnityEvent OnKO;
}
