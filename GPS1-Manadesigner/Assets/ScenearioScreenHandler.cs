using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;


public class ScenearioScreenHandler : MonoBehaviour
{
    public GameObject gameOverScreen;
    //public GameObject USAScreen;
    //public GameObject USSRScreen;
    public GameObject player;
    private void Awake()
    {
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (DayCounter.instance != null)
        {
            if (DayCounter.instance.days > 14)
            {
                //gameObject.SetActive(true);
                //USSRScreen.SetActive(true);
                player = (GameObject.FindGameObjectWithTag("Player"));
                //player.GetComponentInChildren<PlayerHealth>().DeletePersistentPathDirAndPlayerPrefs();
                SceneManager.LoadScene("USSRWins");
            }
            else
            {
                gameOverScreen.SetActive(true);
                player = (GameObject.FindGameObjectWithTag("Player"));
                var pHp = player.GetComponentInChildren<PlayerHealth>();
                if (pHp.health <= 0)
                {
                    //player.GetComponentInChildren<PlayerHealth>().DeletePersistentPathDirAndPlayerPrefs();
                    SceneManager.LoadScene("GameOverScreen");
                }
            }

        }
        else
        {
            gameOverScreen.SetActive(true);
            player = (GameObject.FindGameObjectWithTag("Player"));
            var pHp = player.GetComponentInChildren<PlayerHealth>();
            if (pHp.health <= 0)
            {
                //player.GetComponentInChildren<PlayerHealth>().DeletePersistentPathDirAndPlayerPrefs();
                SceneManager.LoadScene("GameOverScreen");
            }
        }
    }

    public void AttemptWin()
    {
        if (DayCounter.instance.days >14)
        {
            player = (GameObject.FindGameObjectWithTag("Player"));
            //player.GetComponentInChildren<PlayerHealth>().DeletePersistentPathDirAndPlayerPrefs();
            SceneManager.LoadScene("USSRWins");
        }
        else
        {
            SceneManager.LoadScene("USAWins");
        }
    }
}
