using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public void RestartGame()
    {
        PlayerPrefs.DeleteKey("Day");
        DayCounter.instance.days = 0;
        Debug.Log("All prefs are deleted.");
        PlayerPrefs.SetFloat("health", -1);
        PlayerPrefs.SetFloat("oxygenSupply", -1);
        PlayerPrefs.SetInt("healthKits", -1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void LoadSceneByName(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
