using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressureStageChecker : MonoBehaviour
{
    [SerializeField] int stage;

    public int GetStage()
    {
        return stage;
    }
}
