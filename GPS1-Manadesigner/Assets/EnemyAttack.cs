using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyAttack : MonoBehaviour
{
    PlayerHealth player;
    [SerializeField] float attackDamage = 10;
    [SerializeField] float attackRate = 1f;
    float attackTime;
    bool isAttacking = false;

    private void Start()
    {
        player = FindObjectOfType<PlayerHealth>();
    }

    private void Update()
    {
        if(isAttacking)
        {
            if (attackTime <= 0)
            {
                player.GetDamage(attackDamage);
                attackTime = attackRate;
                if (attackDamage > 0)
                { 
                    OnAtk.Invoke();
                }
                //Debug.Log("Attack!");
            }
            else
            {
                attackTime -= Time.deltaTime;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (player != null)
        {
            if (collision.gameObject == player.gameObject)
            {
                isAttacking = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == player.gameObject)
        {
            isAttacking = false;
            attackTime = 0;
        }
    }
    public UnityEvent OnAtk;


}
