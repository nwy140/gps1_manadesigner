using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToggleSaveToPlayerPrefs : MonoBehaviour
{
    public Toggle toggleComp;
    public string pPrefsName;
    int pPrefsVal;
    private void OnEnable()
    {
        pPrefsVal = PlayerPrefs.GetInt(pPrefsName);
        toggleComp.isOn = Convert.ToBoolean(pPrefsName);
    }

    // Update is called once per frame
    void Update()
    {
        PlayerPrefs.SetInt(pPrefsName, Convert.ToInt32(toggleComp.isOn));
    }
}
