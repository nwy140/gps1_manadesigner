using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    // [SerializeField] Turbine turbineUpgrade;
    Rigidbody2D playerRigidBody;
    public float speed = 5f;

    void Start()
    {
        playerRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Movement();
    }

    void Movement()
    {
        var deltaX = Input.GetAxis("Horizontal");
        var deltaY = Input.GetAxis("Vertical");

        playerRigidBody.velocity = new Vector2(deltaX * speed, deltaY * speed);
    }
}
