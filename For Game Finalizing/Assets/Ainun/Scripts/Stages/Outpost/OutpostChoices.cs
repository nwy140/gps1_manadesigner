using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OutpostChoices : MonoBehaviour
{
    public void ReturnSea()
    {
        SceneManager.LoadScene("Stage One");
    }
}
