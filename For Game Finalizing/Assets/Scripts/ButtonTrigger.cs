using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonTrigger : MonoBehaviour
{
    public void Startgame()
    {
        SceneManager.LoadScene("Stage One");
    }
    
    public void ExitButton()
    {
        Debug.Log("Game quit...");
        Application.Quit();
    }
    
}
